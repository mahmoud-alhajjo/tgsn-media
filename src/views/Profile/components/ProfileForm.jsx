import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Card, CardContent, CircularProgress, Button, CardActions, Typography, Grid } from '@material-ui/core';

import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator'

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 250,
        backgroundColor: theme.palette.background.paper,
    },
    headerForm: {
        marginLeft: theme.spacing(1),
    },
}));

const mapStateToProps = ({ }) => { return {}; };

const mapDispatchToProps = dispatch => ({});

const ProfileForm = ({
    user,
    submitUpdateUser,
    submitUpdateUserPassword,
    submitting,
}) => {

    const classes = useStyles();
    const [formData, setFormData] = useState(user)
    const [formDataPassword, setFormDataPassword] = useState({ id: user.id, old_password: '', password: '', password_confirmation: '' })
    const validationPassWord = () => {
        if ((8 <= formDataPassword.password.length && formDataPassword.password.length <= 16) && (8 <= formDataPassword.old_password.length && formDataPassword.old_password.length <= 16) && (formDataPassword.password === formDataPassword.password_confirmation) && !submitting) {
            return true
        }
        return false
    }
    useEffect(() => {
        ValidatorForm.addValidationRule('isPasswordComplex', value => {
            if (!value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W\_])[A-Za-z\d\W\_]{8,}$/)) {
                return false
            }
            return true
        },
            ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
                if (value !== formDataPassword.password) {
                    return false;
                }
                return true;
            })
        )
    })

    return (
        <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="flex-start"
        >
            <Grid item xs={4}>
                <Card style={{ marginBottom: 30 }}>
                    <CardContent>
                        <Typography component="h3" className={classes.headerForm}>Update your profile</Typography>
                        <ValidatorForm onSubmit={() => { }}>
                            <TextValidator
                                label='Username'
                                name='username'
                                value={formData.username}
                                onChange={event =>
                                    setFormData({ ...formData, username: event.target.value })}
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                disabled={submitting}
                            />
                            <TextValidator
                                label='Email Address'
                                name='email'
                                value={formData.email}
                                onChange={event =>
                                    setFormData({ ...formData, email: event.target.value })}
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                required
                                disabled={submitting}
                                validators={['isEmail']}
                                errorMessages={['Email is not valid']}
                            />
                        </ValidatorForm>
                    </CardContent>
                    <CardActions>
                        {submitting && <CircularProgress />}
                        <Button
                            size="medium"
                            color="primary"
                            style={{ marginTop: 10, textTransform: 'capitalize' }}
                            disabled={(user.username === formData.username && user.email === formData.email) || submitting ? true : false}
                            type='submit'
                            onClick={() => {
                                submitUpdateUser(formData)
                            }}>
                            Update
                    </Button>
                    </CardActions>
                </Card>
            </Grid>
            <Grid item xs={7}>
                <Card style={{ marginBottom: 30 }}>
                    <CardContent>
                        <Typography component="h3" className={classes.headerForm}>Write new password or keep it empty to keep your old password</Typography>
                        <ValidatorForm onSubmit={() => { }}>
                            <TextValidator
                                label='Old Password'
                                name='oldpassword'
                                value={formDataPassword.old_password}
                                onChange={event =>
                                    setFormDataPassword({ ...formDataPassword, old_password: event.target.value })}
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                disabled={submitting}
                                validators={['isPasswordComplex']}
                                errorMessages={['The password must be complex and at least 8 characters, use letters, numbers, and common punctuation characters.']}
                            />
                            <TextValidator
                                label='Password'
                                name='password'
                                value={formDataPassword.password}
                                onChange={event =>
                                    setFormDataPassword({ ...formDataPassword, password: event.target.value })}
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                disabled={submitting}
                                validators={['isPasswordComplex']}
                                errorMessages={['The password must be complex and at least 8 characters, use letters, numbers, and common punctuation characters.']}
                            />
                            <TextValidator
                                label='Confirm Password'
                                name='password'
                                value={formDataPassword.password_confirmation}
                                onChange={event =>
                                    setFormDataPassword({ ...formDataPassword, password_confirmation: event.target.value })}
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                disabled={submitting}
                                validators={['isPasswordComplex', 'isPasswordMatch']}
                                errorMessages={['The password must be complex and at least 8 characters, use letters, numbers, and common punctuation characters.', 'Passwords not Match']}
                            />

                        </ValidatorForm>
                        <CardActions>
                            {submitting && <CircularProgress />}
                            <Button
                                size="medium"
                                color="secondary"
                                style={{ marginTop: 10, textTransform: 'none' }}
                                disabled={validationPassWord() ? false : true}
                                type='submit' onClick={() => {
                                    submitUpdateUserPassword(formDataPassword)
                                }}>
                                Change password
                    </Button>
                        </CardActions>
                    </CardContent>
                </Card >
            </Grid>
        </Grid>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileForm);

import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Container } from '@material-ui/core';

import { updateUserByID, getUser, updatePassword } from '../../api/helper'
import ProfileForm from './components/ProfileForm';
import FullPageLoader from '../../components/FullPageLoader';


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 250,
        backgroundColor: theme.palette.background.paper,
    },
    dashboardContainer: {
        marginTop: theme.spacing(4),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        '& img': {
            margin: theme.spacing(3),
        },
    },
}));

const mapStateToProps = ({
    users: { submitting },
    userProfile: { userProfile, user, fetching }
}) => {
    return {
        submitting,
        userProfile,
        user,
        fetching
    };
};

const mapDispatchToProps = dispatch => ({
    dispatchGetUser: userId => {
        dispatch(getUser(userId))
    },
    submitUpdateUser: user => {
        dispatch(
            updateUserByID({
                id: user.id,
                username: user.username,
                email: user.email,
            }),
        )
    },
    submitUpdateUserPassword: userpassword => {
        dispatch(
            updatePassword({
                id: userpassword.id,
                old_password: userpassword.old_password,
                password: userpassword.password,
                password_confirmation: userpassword.password_confirmation
            })
        )
    }
});

const Profile = ({
    user,
    submitUpdateUser,
    submitUpdateUserPassword,
    dispatchGetUser,
    submitting,
    userProfile,
    fetching
}) => {

    const classes = useStyles();
    useEffect(() => {
        userProfile && dispatchGetUser(userProfile.id)
        // eslint-disable-next-line
    }, [dispatchGetUser]);

    return (
        <Container maxWidth="xl" className={classes.dashboardContainer}>
            {fetching ? (
                <FullPageLoader />
            ) : (
                    user && <ProfileForm
                        user={user}
                        submitUpdateUser={(user) => submitUpdateUser(user)}
                        submitUpdateUserPassword={(userpassword) => submitUpdateUserPassword(userpassword)}
                        submitting={submitting}
                    />
                )}
        </Container >
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

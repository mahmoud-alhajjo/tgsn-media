import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Table from '../../components/Table'
import { Container } from '@material-ui/core'
import { getAllUsers, createUser, updateUser, deleteUser } from '../../api/helper'
import CreateUser from './components/CreateUser'
import UpdateUser from './components/UpdateUser'

const useStyles = makeStyles(theme => ({
  usersContainer: {
    marginTop: theme.spacing(4)
  }
}))

const mapStateToProps = ({
  users: {
    users,
    totalData,
    fetching,
    error,
    submitting,
    showUpdateModal,
    showAddModal,
    showDeleteDialog
  },
  userProfile: { isAdmin }
}) => {
  return {
    users,
    totalData,
    fetching,
    error,
    submitting,
    showUpdateModal,
    showAddModal,
    showDeleteDialog,
    isAdmin
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchGetAllUsers: pageNumber => {
    dispatch(getAllUsers(pageNumber + 1))
  },
  submitCreateUser: user => {
    dispatch(createUser(user))
  },
  submitDeleteEvent: userId => {
    dispatch(deleteUser(userId))
  },
  submitUpdateUser: user => {
    dispatch(
      updateUser({
        id: user.id,
        username: user.username,
        email: user.email,
        activated: user.activated
      })
    )
  },
  toggleUpdateModal: showUpdateModal => {
    dispatch({
      type: 'TOGGLE_USER_UPDATE',
      payload: {
        showUpdateModal: showUpdateModal
      }
    })
  },
  toggleAddModal: showAddModal => {
    dispatch({
      type: 'TOGGLE_USER_ADD',
      payload: {
        showAddModal: showAddModal
      }
    })
  },
  toggleDeleteDialog: showDeleteDialog => {
    dispatch({
      type: 'TOGGLE_USER_DELETE',
      payload: {
        showDeleteDialog: showDeleteDialog
      }
    })
  }
})

const Users = ({
  users,
  totalData,
  fetching,
  dispatchGetAllUsers,
  submitCreateUser,
  submitUpdateUser,
  submitDeleteEvent,
  showUpdateModal,
  showAddModal,
  showDeleteDialog,
  toggleUpdateModal,
  toggleAddModal,
  toggleDeleteDialog,
  submitting,
  isAdmin
}) => {
  const classes = useStyles()
  const [pageNumber, setPageNumber] = useState(0)
  const [userToUpdate, setUserToUpdate] = useState({})
  const columns = [
    { title: 'Username', field: 'username' },
    { title: 'Email', field: 'email' },
    {
      title: 'Activated',
      field: 'activated',
      render: rowData => (rowData.activated ? 'Active' : 'Inactive')
    }
  ]
console.log('users', users)
console.log('fetching', fetching)

  useEffect(
    () => {
      dispatchGetAllUsers(pageNumber)
    },
    [dispatchGetAllUsers, pageNumber]
  )

  return (
    <Container maxWidth='xl' className={classes.usersContainer}>
      {showAddModal &&
        <CreateUser
          open={showAddModal}
          handleClose={() => toggleAddModal(false)}
          submitCreateUser={submitCreateUser}
          submitting={submitting}
        />}
      {showUpdateModal &&
        <UpdateUser
          open={showUpdateModal}
          handleClose={() => toggleUpdateModal(false)}
          submitUpdateUser={submitUpdateUser}
          submitting={submitting}
          userToUpdate={userToUpdate}
        />}
      <Container>
        <Table
          style={{ maxWidth: '100%' }}
          columns={columns}
          // allowDelete={isAdmin}
          allowEdit={isAdmin}
          allowAdd={isAdmin}
          rowsPerPage={10}
          data={users}
          isLoading={fetching}
          pageNumber={pageNumber}
          onChangePage={newPage => {
            setPageNumber(newPage)
            dispatchGetAllUsers(newPage)
          }}
          totalData={totalData}
          title='Users'
          onRowAdd={() => toggleAddModal(true)}
          onRowUpdate={user => {
            console.log('user',user)
            setUserToUpdate(user)
            toggleUpdateModal(true)
          }}
          addTooltip='Add User'
        />
      </Container>
    </Container>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)

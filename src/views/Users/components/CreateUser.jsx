import React, { useState, useEffect } from 'react'
import { FormControlLabel, Switch } from '@material-ui/core'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'

const CreateUser = ({ open, handleClose, submitCreateUser, submitting }) => {
  const [formData, setFormData] = useState({
    username: '',
    email: '',
    password: '',
    activated: true
  })
  useEffect(() => {
    ValidatorForm.addValidationRule('isPasswordComplex', value => {
      if (!value.match('^([a-zA-Z0-9@*#]{8,16})$')) {
        return false
      }
      return true
    })
  })
  return (
    <DialogForm
      open={open}
      handleClose={handleClose}
      title='Create User'
      onSubmit={() => submitCreateUser(formData)}
      submitting={submitting}
      submitLabel='Create'
    >
      <TextValidator
        label='Username'
        name='username'
        value={formData.username}
        onChange={event =>
          setFormData({ ...formData, username: event.target.value })}
        variant='outlined'
        margin='normal'
        fullWidth
        required
        disabled={submitting}
        validators={['required']}
        errorMessages={['Username is required']}
      />
      <TextValidator
        label='Email Address'
        name='email'
        value={formData.email}
        onChange={event =>
          setFormData({ ...formData, email: event.target.value })}
        variant='outlined'
        margin='normal'
        fullWidth
        required
        disabled={submitting}
        validators={['required', 'isEmail']}
        errorMessages={['Email address is required', 'Email is not valid']}
      />
      <TextValidator
        label='Password'
        name='password'
        value={formData.password}
        onChange={event =>
          setFormData({ ...formData, password: event.target.value })}
        variant='outlined'
        margin='normal'
        fullWidth
        required
        disabled={submitting}
        validators={['required', 'isPasswordComplex']}
        errorMessages={['Password is required', 'Password must be at least 8 characters and not more than 16 characters']}
      />
      <FormControlLabel
        control={
          <Switch
            checked={formData.activated}
            disabled={submitting}
            onChange={() =>
              setFormData({ ...formData, activated: !formData.activated })}
          />
        }
        label={formData.activated ? 'Activated' : 'Inactivated'}
      />
    </DialogForm>
  )
}

export default CreateUser

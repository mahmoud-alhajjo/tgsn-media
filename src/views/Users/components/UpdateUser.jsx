import React, { useState } from 'react'
import { FormControlLabel, Switch } from '@material-ui/core'
import { TextValidator } from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'

const UpdateUser = ({
  open,
  handleClose,
  submitUpdateUser,
  submitting,
  userToUpdate
}) => {
  const [formData, setFormData] = useState(userToUpdate)
  return (
    <DialogForm
      open={open}
      handleClose={handleClose}
      title='Edit User'
      onSubmit={() => submitUpdateUser(formData)}
      submitting={submitting}
      submitLabel='Save Changes'
    >
      <TextValidator
        label='Username'
        name='username'
        value={formData.username}
        onChange={event =>
          setFormData({ ...formData, username: event.target.value })}
        variant='outlined'
        margin='normal'
        fullWidth
        required
        disabled={submitting}
        validators={['required']}
        errorMessages={['Username is required']}
      />
      <TextValidator
        label='Email Address'
        name='email'
        value={formData.email}
        onChange={event =>
          setFormData({ ...formData, email: event.target.value })}
        variant='outlined'
        margin='normal'
        fullWidth
        required
        disabled={submitting}
        validators={['required', 'isEmail']}
        errorMessages={['Email address is required', 'Email is not valid']}
      />
      <FormControlLabel
        control={
          <Switch
            checked={formData.activated}
            disabled={submitting || userToUpdate.is_admin}
            onChange={() =>
              setFormData({ ...formData, activated: !formData.activated })}
          />
        }
        label={formData.activated ? 'Activated' : 'Inactivated'}
      />
    </DialogForm>
  )
}

export default UpdateUser

import React, { Fragment, useState } from 'react'
import { connect } from 'react-redux'
import { Layer, GeoJSONLayer, Popup } from 'react-mapbox-gl'
import { getEventsByCoordinate } from '../../../api/helper'
import MapPopup from '../components/MapPopup'
import { Typography } from '@material-ui/core'

const mapDispatchToProps = dispatch => ({
  dispatchGetEventsByCoordinate: (countryId, coordsId, pageNum) => {
    dispatch(getEventsByCoordinate(countryId, coordsId, pageNum))
  },
  zoomCluster: event => {
    const map = event.target
    map.getCanvas().style.cursor = ''
    var features = map.queryRenderedFeatures(event.point, {
      layers: ['cluster_layer']
    })
    if (!features || features.length === 0) return
    var clusterId = features && features[0].properties.cluster_id
    var pointCount = features && features[0].properties.point_count
    var clusterSource = map.getSource('media_source')

    // Get all points under a cluster
    if (clusterSource) {
      clusterSource.getClusterLeaves(
        clusterId,
        pointCount,
        0,
        (err, cFeatures) => {
          if (cFeatures) {
            const clusterLeaves = cFeatures.map(child => {
              return {
                longitude: child.geometry.coordinates[0],
                latitude: child.geometry.coordinates[1]
              }
            })
            dispatch({
              type: 'MAP_CLUSTER_ZOOM_ACTION',
              payload: {
                clusterLeaves
              }
            })
          }
        }
      )
    }
  }
})

const ClusterLayer = ({
  mapCoordinates,
  dispatchGetEventsByCoordinate,
  zoomCluster,
  countryId,
  classification
}) => {
  const [showPopup, setShowPopup] = useState(false)
  const [popupCoordinates, setPopupCoordinates] = useState([])
  const [coordsId, setCoordsId] = useState(null)
  const [infoPopup, setInfoPopup] = useState('')

  const features =
    mapCoordinates &&
    mapCoordinates.map(coordinate => {
      return {
        type: 'Feature',
        properties: {
          id: coordinate.id,
          count: coordinate.count,
          city_name: coordinate.city_name,
          region_name: coordinate.region_name,
          kills_sum: coordinate.kills_sum,
          kills_mean: coordinate.kills_mean,
          longitude: coordinate.longitude,
          latitude: coordinate.latitude
        },
        geometry: {
          type: 'Point',
          coordinates: [coordinate.longitude, coordinate.latitude]
        }
      }
    })

  let typeClassification = classification === 'kills' ? 'kills_sum' : 'count'

  return (
    <Fragment>
      <GeoJSONLayer
        sourceOptions={{
          cluster: true,
          clusterMaxZoom: 8,
          clusterRadius: 50,
          clusterProperties: {
            cluster_count: ['+', ['get', 'count']],
            cluster_kills: ['+', ['get', 'kills_sum']]
          }
        }}
        id='media_source'
        data={{
          type: 'FeatureCollection',
          features
        }}
      />
      <Layer
        id='cluster_layer'
        sourceId='media_source'
        filter={['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']}
        paint={{
          'circle-color': '#6977C3',
          'circle-radius': {
            property: typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count',
            type: 'interval',
            stops: [[3, 20], [50, 25], [100, 30]]
          },
          'circle-stroke-width': 5,
          'circle-stroke-color': '#aaa',
          'circle-stroke-opacity': 0.5
        }}
        type='circle'
        onClick={event => {
          zoomCluster(event)
          setShowPopup(false)
        }}
        onMouseEnter={event => {
          (event.target.getCanvas().style.cursor = 'pointer')
        }}
        onMouseLeave={event => {
          event.target.getCanvas().style.cursor = ''
          setInfoPopup('')
        }}
      />
      <Layer
        id={typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count'}
        type='symbol'
        sourceId='media_source'
        filter={['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']}
        layout={{
          'text-field': typeClassification === 'kills_sum' ? '{cluster_kills}' : '{cluster_count}',
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 12
        }}
        paint={{
          'text-color': '#fff'
        }}
      />
      <Layer
        id='unclustered_layer'
        sourceId='media_source'
        filter={['!', ['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']]}
        paint={{
          'circle-color': '#6977C3',
          'circle-radius': {
            property: typeClassification,
            type: 'interval',
            stops: [[3, 20], [50, 25], [100, 30]]
          },
          'circle-stroke-width': 5,
          'circle-stroke-color': '#aaa',
          'circle-stroke-opacity': 0.5
        }}
        type='circle'
        onClick={event => {
          dispatchGetEventsByCoordinate(
            countryId,
            event.features[0].properties.id,
            1
          )
          setPopupCoordinates(event.features[0].geometry.coordinates)
          setCoordsId(event.features[0].properties.id)
          setShowPopup(true)
        }}
        onMouseEnter={(event) => {
          !infoPopup && setInfoPopup(event.features[0].properties)
          event.target.getCanvas().style.cursor = 'pointer'
        }}
        onMouseLeave={event => {
          (event.target.getCanvas().style.cursor = '')
          setInfoPopup('')
        }}
      />
      <Layer
        id='unclustered_layer_count'
        type='symbol'
        sourceId='media_source'
        filter={['!', ['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']]}
        layout={{
          'text-field': `{${typeClassification}}`,
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 12
        }}
        paint={{
          'text-color': '#fff'
        }}
      />
      {infoPopup &&
        <Popup
          key={infoPopup.id}
          coordinates={[infoPopup.longitude, infoPopup.latitude]}
          offset={{
            'bottom-left': [12, -38], 'bottom': [0, -38], 'bottom-right': [-12, -38]
          }}>
          <Typography>City: {infoPopup.city_name}</Typography>
          <Typography>Region: {infoPopup.region_name}</Typography>
          <Typography>Kills sum: {infoPopup.kills_sum}</Typography>
          <Typography>Kills mean: {infoPopup.kills_mean}</Typography>
        </Popup>
      }
      {showPopup &&
        <MapPopup
          coordinates={popupCoordinates}
          handleClosePopup={() => setShowPopup(false)}
          countryId={countryId}
          coordsId={coordsId}
        />}
    </Fragment>
  )
}

export default connect(null, mapDispatchToProps)(ClusterLayer)

import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { InputLabel, IconButton } from '@material-ui/core'
import { Close as CloseIcon, Translate } from '@material-ui/icons'
import moment from 'moment'
export const googleTranslate = require("google-translate")("AIzaSyDxGiqSYhE_goREwZunphHueYbFK6WAvxQ");

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    width: 350,
    top: 0,
    bottom: 0,
    right: 0,
    zIndex: 99,
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    borderRadius: theme.spacing(0.5),
    paddingTop: 0
  },
  gridList: {
    width: '100%',
    padding: theme.spacing(1)
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    '& label': {
      fontWeight: 'bold'
    }
  },
  row: {
    padding: theme.spacing(1),
    display: 'flex',
    alignItems: 'center'
  },
  fieldName: {
    flex: 1,
    minHeight: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
    '& label': {
      fontWeight: 'bold',
      fontSize: '14px'
    }
  },
  field: {
    flex: 3,
    paddingLeft: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px',
    width: '66%'
  }
}))

const getArrayText = array => {
  return array && array.map(e => e.name).join(' ,')
}
const GridRow = ({ fieldName, children }) => {
  const classes = useStyles()
  return (
    <div className={classes.row}>
      <div className={classes.fieldName}>
        <InputLabel>
          {fieldName}
        </InputLabel>
      </div>
      <div className={classes.field}>
        {children}
      </div>
    </div>
  )
}

const MapEventDetails = ({ event, handCloseDetails }) => {
  const classes = useStyles()
  const [translateText, setTranslateText] = useState('')

  return (
    <div className={classes.root}>
      <div className={classes.gridList}>
        <div className={classes.header}>
          <IconButton
            onClick={() => {
              googleTranslate.translate(event.report_heading, 'ar', 'en', function (err, translations) {
                translations && setTranslateText(translations.translatedText)
              })
            }}
            style={{ marginLeft: 9 }}
          >
            <Translate />
          </IconButton>
          <IconButton
            onClick={handCloseDetails}
          >
            <CloseIcon />
          </IconButton>
        </div>
        <GridRow fieldName='Date'>
          <InputLabel>
            {moment(event.date, 'DD/MM/YYYY').format('DD/MM/YYYY')}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Report Heading'>
          <InputLabel>
            {event.report_heading}
          </InputLabel>
        </GridRow>

        {translateText && <GridRow fieldName='Translate'>
          <InputLabel>{translateText}</InputLabel>
        </GridRow>}

        <GridRow fieldName='Location'>
          <InputLabel>
            {event.location}
          </InputLabel>
        </GridRow>
        <GridRow fieldName='Region'>
          <InputLabel>
            {event.region}
          </InputLabel>
        </GridRow>
        {/* 
        <GridRow fieldName='City'>
          <InputLabel>
            {getArrayText([event.city])}
          </InputLabel>
        </GridRow> */}

        <GridRow fieldName='Weapons'>
          <InputLabel>
            {getArrayText(event.weapons)}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Attack types'>
          <InputLabel>
            {getArrayText(event.attack_types)}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Targets'>
          <InputLabel>
            {getArrayText(event.targets)}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Targets types'>
          <InputLabel>
            {getArrayText(event.targets_types)}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Kills'>
          <InputLabel>
            {event.kills}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Other'>
          <InputLabel>
            {event.other}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Evidences'>
          <InputLabel>
            {getArrayText(event.evidences)}
          </InputLabel>
        </GridRow>

        <GridRow fieldName='Created by'>
          <InputLabel>
            {event.user.username}
          </InputLabel>
        </GridRow>
      </div>
    </div>
  )
}

export default MapEventDetails

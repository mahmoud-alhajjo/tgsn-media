import React, { useState, useReducer } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import moment from 'moment'
import AutoComplete from '../../../components/AutoComplete'
import { Card, CardContent, Grid, Button, CardActions, Typography, TextField, InputLabel } from '@material-ui/core';
import { DateRangeInput } from '@datepicker-react/styled'

const useStyles = makeStyles(theme => ({
    card: {
        position: 'absolute',
        left: 208,
        top: 52,
        margin: '20px',
        zIndex: 12
    },
    card2: {
        maxWidth: 380,
        position: 'absolute',
        left: 255,
        top: 149,
        maxHeight: '100vh',
        overflow: 'auto',
        zIndex: 11,
        marginBottom: 20,
        paddingBottom: 0
    },
    cardForPAgeEvent: {
        position: 'absolute',
        left: 271,
        top: 56,
        margin: '20px',
        zIndex: 12
    },
    cardForPAgeStatistics: {
        position: 'absolute',
        left: 352,
        top: 56,
        margin: '20px',
        zIndex: 12
    },
    cardForPAgeEvent2: {
        minWidth: 300,
        maxWidth: 380,
        marginRight: 24,
        maxHeight: '100vh',
        overflow: 'auto',
    },
})
)

const mapStateToProps = ({ categories: { allItems } }) => {
    return {
        ...allItems
    }
}

const mapDispatchToProps = dispatch => ({
})

const initialState = {
    startDate: null,
    endDate: null,
    focusedInput: null,
}

function reducer(state, action) {
    switch (action.type) {
        case 'focusChange':
            return { ...state, focusedInput: action.payload }
        case 'dateChange': {
            return action.payload
        }
        default:
            throw new Error()
    }
}


const Search = ({
    open,
    handleClose,
    submitting,
    countryId,
    callbackFun,
    submitMapSearchEvents,
    submitMapSearchChoroplethEvents,
    levelMap,
    weapon,
    attack_type,
    target_type,
    target,
    evidence,
    showfilterDate,
    setShowfilterDate,
    showfilterItems,
    setShowfilterItems,
    submitSearchEvents,
    styleForPAgeEvent,
    lengthcountryName,
    submitStatisticsSearchEvents,
    styleForPAgeStatistics
}) => {

    const classes = useStyles()
    const [state, dispatch] = useReducer(reducer, initialState)
    let maxBookingDate = new Date()
    let styleCard1 = styleForPAgeEvent ? classes.cardForPAgeEvent : styleForPAgeStatistics ? classes.cardForPAgeStatistics : classes.card
    let styleCard2 = styleForPAgeEvent || styleForPAgeStatistics ? classes.cardForPAgeEvent2 : classes.card2
    let headertext = styleForPAgeEvent ? 'data' : styleForPAgeStatistics ? 'statistics' : 'map'
    let sizeInput = styleForPAgeEvent || styleForPAgeStatistics ? '' : 'Small'
    let sizeChip = styleForPAgeEvent || styleForPAgeStatistics ? 'medium' : 'small'
    const inputProps = {
        margin: 'normal',
        variant: 'outlined',
        fullWidth: true,
        disabled: submitting
    }

    const [formData, setFormData] = useState({
        countryId: parseInt(countryId),
        sinceDate: null,
        toDate: null,
        gteKills: '',
        lteKills: '',
        region: '',
        reportHeading: '',
        weapon: '',
        attack_type: '',
        target_type: '',
        target: '',
        evidence: '',
        other: '',
    })

    const onSubmit = () => {
        if (submitSearchEvents) {
            submitSearchEvents({
                ...formData,
                sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
                toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
                weapon: formData.weapon,
                attack_type: formData.attack_type,
                target_type: formData.target_type,
                target: formData.target,
                evidence: formData.evidence,
            })
        } if (submitStatisticsSearchEvents) {
            submitStatisticsSearchEvents(countryId.length > 0 ? countryId : parseInt(countryId), {
                ...formData,
                sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
                toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
                weapon: formData.weapon,
                attack_type: formData.attack_type,
                target_type: formData.target_type,
                target: formData.target,
                evidence: formData.evidence,
            })
        } else {
            callbackFun({
                ...formData,
                sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
                toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
                weapon: formData.weapon,
                attack_type: formData.attack_type,
                target_type: formData.target_type,
                target: formData.target,
                evidence: formData.evidence,
            })
            submitMapSearchEvents(countryId.length > 0 ? countryId : parseInt(countryId), {
                ...formData,
                sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
                toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
                weapon: formData.weapon,
                attack_type: formData.attack_type,
                target_type: formData.target_type,
                target: formData.target,
                evidence: formData.evidence,
            })
            submitMapSearchChoroplethEvents(levelMap, countryId.length > 0 ? countryId : parseInt(countryId), {
                ...formData,
                sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
                toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
                weapon: formData.weapon,
                attack_type: formData.attack_type,
                target_type: formData.target_type,
                target: formData.target,
                evidence: formData.evidence,
            })
        }
    }
    return (
        <div>
            {showfilterDate && <div className={styleCard1} style={{ left: styleForPAgeStatistics ? '' : 225 + (lengthcountryName * 9 + 15) }}>
                <Grid container justify="space-around">
                    <DateRangeInput
                        onDatesChange={data => {
                            dispatch({
                                type: 'dateChange', payload: data
                            })
                        }}
                        onFocusChange={focusedInput => {
                            dispatch({ type: 'focusChange', payload: focusedInput })
                        }}
                        startDate={state.startDate} // Date or null
                        endDate={state.endDate} // Date or null
                        focusedInput={state.focusedInput} // START_DATE, END_DATE or null
                        displayFormat='dd/MM/yyyy'
                        maxBookingDate={maxBookingDate}
                    />
                    <Button
                        size="small"
                        variant="contained"
                        color="primary"
                        onClick={() => onSubmit()}
                        style={{ height: 35, marginLeft: 5, marginTop: 5, textTransform: 'capitalize' }}
                    >
                        Apply
                    </Button>
                </Grid>
            </div>
            }
            {showfilterItems && <Card className={styleCard2}>
                <CardContent>
                    <Typography component="h4">Filter your {headertext}</Typography>
                    <InputLabel htmlFor="input-with-icon-adornment" style={{ marginTop: 12 }}>Number of kills :</InputLabel>
                    <Grid container justify="flex-start">
                        <TextField
                            placeholder={styleForPAgeEvent ? "" : 'Greater'}
                            label={styleForPAgeEvent ? "Greater" : ''}
                            id="filled-size-small"
                            name='gteKills'
                            value={formData.gteKills}
                            onChange={event =>
                                setFormData({ ...formData, gteKills: parseInt(event.target.value) })}
                            type='number'
                            InputProps={{ inputProps: { min: 0 } }}
                            {...inputProps}
                            style={{ width: 125, marginRight: 10, marginTop: 7 }}
                            variant='standard'
                            size="small"
                        />
                        <TextField
                            placeholder={styleForPAgeEvent ? "" : 'Less'}
                            label={styleForPAgeEvent ? "Less" : ''}
                            id="filled-size-small"
                            name='lteKills'
                            value={formData.lteKills}
                            onChange={event =>
                                setFormData({ ...formData, lteKills: parseInt(event.target.value) })}
                            type='number'
                            InputProps={{ inputProps: { min: 0 } }}
                            {...inputProps}
                            style={{ width: 125, marginTop: 7 }}
                            variant='standard'
                            size="small"
                        />
                    </Grid>
                    <AutoComplete
                        label='Weapon'
                        options={weapon}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                weapon: options ? options.map(opt => opt.value) : []
                            })}
                        value={weapon.filter(e => formData.weapon.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        style={{ width: 175, marginRight: 10 }}
                        sizeChip={sizeChip}
                        sizeInput={sizeInput}
                    />
                    <AutoComplete
                        label='Attack types'
                        options={attack_type}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                attack_type: options ? options.map(opt => opt.value) : []
                            })}
                        value={attack_type.filter(e => formData.attack_type.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        sizeChip={sizeChip}
                        sizeInput={sizeInput}
                    />
                    <AutoComplete
                        label='Targets'
                        options={target}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                target: options ? options.map(opt => opt.value) : []
                            })}
                        value={target.filter(e => formData.target.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        sizeChip={sizeChip}
                        sizeInput={sizeInput}
                    />
                    <AutoComplete
                        label='Targets types'
                        options={target_type}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                target_type: options ? options.map(opt => opt.value) : []
                            })}
                        value={target_type.filter(e => formData.target_type.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        sizeChip={sizeChip}
                        sizeInput={sizeInput}
                    />
                    <AutoComplete
                        label='Evidences'
                        options={evidence}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                evidence: options ? options.map(opt => opt.value) : []
                            })}
                        value={evidence.filter(e => formData.evidence.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        sizeChip={sizeChip}
                        sizeInput={sizeInput}
                    />
                    <CardActions style={{ marginBottom: 0, paddingBottom: 0 }}>
                        <Button size="small" color="primary" style={{ textTransform: 'capitalize' }} onClick={() => onSubmit()}>
                            Apply
                        </Button>
                        <Button size="small" color="primary" style={{ textTransform: 'capitalize' }} onClick={() => setShowfilterItems(false)}>
                            cancel
                         </Button>
                    </CardActions>
                </CardContent>
            </Card>}
        </div>

    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)

import React, { Fragment, useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import FullPageLoader from '../../../components/FullPageLoader'
import HeatmapLayer from './HeatmapLayer'
import ClusterLayer from './ClusterLayer'
import { Layers as LayersIcon, Camera } from '@material-ui/icons'
import { Box, Radio, FormControlLabel, Fab } from '@material-ui/core'
import Tooltip from '../../../components/Tooltip'
import ChoroplethLayer from './ChoroplethLayer';
import html2canvas from "html2canvas";

const useStyles = makeStyles(theme => ({
  layerSwitcherContainer: {
    position: 'absolute',
    bottom: '90px',
    right: '10px',
    zIndex: '10',
  },
  layerSwitcher: {
    width: 26,
    height: 26,
    boxShadow: 'rgba(0, 0, 0, 0.3) 0px 1px 4px',
    border: '1px solid rgba(0, 0, 0, 0.1)',
    backgroundColor: 'rgba(249, 249, 249, 0.95)',
    borderRadius: '3px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  layerSwitcherPopup: {
    position: 'absolute',
    right: '36px',
    bottom: '0',
    background: 'white',
    padding: '8px',
  }
}))

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = ({ map: { mapCoordinates, fetching } }) => {
  return {
    mapCoordinates,
    fetching
  }
}


const MapLayer = ({ mapCoordinates, fetching, countryId, levelMap, setChoroplethLayer, clusterInChoropleth, setClusterInChoropleth, colorchoropleth, classification, callbackFunfromMap }) => {
  const classes = useStyles()
  const [currentLayer, setCurrentLayer] = useState('heatmap')
  const [showSwitcherPopup, toggleSwitcherPopup] = useState(false)
  const [displayScreenShot, setDisplayScreenShot] = useState('')
  currentLayer === 'choropleth' ? setChoroplethLayer(true) : setChoroplethLayer(false)
  const screenShot = () => {
    let legend = document.getElementById("legend")
    if (legend) {
      callbackFunfromMap('none')
      setDisplayScreenShot('none')
      setTimeout(() => html2canvas(document.querySelector("#mapbox")).then(canvas => {
        let link = document.createElement('a')
        link.download = currentLayer ? `${currentLayer} Screenshot.png` : `screenshot.png`
        link.href = canvas.toDataURL()
        link.click()
      }).then(() => {
        callbackFunfromMap('')
        setDisplayScreenShot('')
      }).catch(() => alert(`can't take a screenshot please try again`)), 50)
    } else {
      html2canvas(document.querySelector(".mapboxgl-canvas")).then(canvas => {
        let link = document.createElement('a')
        link.download = currentLayer ? `${currentLayer} Screenshot.png` : `screenshot.png`
        link.href = canvas.toDataURL()
        link.click()
      }).catch(() => alert(`can't take a screenshot please try again`))
    }
  }

  if (fetching)
    return <FullPageLoader />

  return mapCoordinates && !fetching
    && <Fragment>
      <Box className={classes.layerSwitcherContainer} style={{ display: `${displayScreenShot}` }} >
        <Tooltip title='Switch layer' placement='left'>
          <Box
            className={classes.layerSwitcher}
            onClick={() => toggleSwitcherPopup(!showSwitcherPopup)}
          >
            <LayersIcon fontSize='small' />
          </Box>
        </Tooltip>
        {showSwitcherPopup &&
          <Box className={classes.layerSwitcherPopup}>
            <FormControlLabel control={<Radio checked={currentLayer === 'heatmap'}
              onChange={() => {
                setClusterInChoropleth(false)
                setCurrentLayer('heatmap')
                toggleSwitcherPopup(false)
              }} />} label="Heatmap" />
            <FormControlLabel control={<Radio checked={currentLayer === 'cluster'}
              onChange={() => {
                setClusterInChoropleth(false)
                toggleSwitcherPopup(false)
                setTimeout(() => setCurrentLayer('cluster'), 100)
              }} />} label="Cluster" />
            <FormControlLabel control={<Radio checked={currentLayer === 'choropleth'}
              onChange={() => {
                setCurrentLayer('choropleth')
                toggleSwitcherPopup(false)
              }} />} label="Choropleth" />
          </Box>}
      </Box>
      {currentLayer === 'cluster' &&
        <ClusterLayer
          countryId={countryId}
          mapCoordinates={mapCoordinates}
          classification={classification}
        />}
      {currentLayer === 'heatmap' &&
        <HeatmapLayer mapCoordinates={mapCoordinates} classification={classification} />}
      {currentLayer === 'choropleth' &&
        <ChoroplethLayer
          countryId={countryId}
          levelMap={levelMap}
          mapCoordinates={mapCoordinates}
          clusterInChoropleth={clusterInChoropleth}
          colorchoropleth={colorchoropleth}
          classification={classification}
        />}
      <Fab color="primary" aria-label="camera" style={{ position: 'absolute', bottom: 35, left: 30, display: `${displayScreenShot}` }} onClick={screenShot}>
        <Camera />
      </Fab>
    </Fragment>
}

export default connect(mapStateToProps, mapDispatchToProps)(MapLayer)

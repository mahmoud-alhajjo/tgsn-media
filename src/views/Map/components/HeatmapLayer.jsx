import React from 'react'
import { Layer, Feature } from 'react-mapbox-gl'

const HeatmapLayer = ({ mapCoordinates, classification }) => {
  return (
    <Layer
      type='heatmap'
      paint={{
        'heatmap-weight': ['interpolate', ['linear'], ['zoom'], 0, 0.4, 22, 1],
        'heatmap-radius': [
          'interpolate',
          ['linear'],
          ['zoom'],
          0,
          8,
          4,
          10,
          6,
          30
        ],
        'heatmap-intensity': ['interpolate', ['linear'], ['zoom'], 0, 1, 40, 5],
        'heatmap-color': [
          'interpolate',
          ['linear'],
          ['heatmap-density'],
          0,
          'rgba(0,0,255,0)',
          0.1,
          'royalblue',
          0.3,
          'cyan',
          0.5,
          'lime',
          0.7,
          'yellow',
          1,
          'red'
        ],
        'heatmap-opacity': 0.7
      }}
    >
      {// eslint-disable-next-line
      mapCoordinates.map((coord) => {
      let classificationType = classification === "kills" ? coord.kills_sum : coord.count
        for (let i = 0; i <= classificationType; i++) {
          return (
            <Feature
              key={`${coord.id}-${i}`}
              coordinates={[coord.longitude, coord.latitude]}
            />
          )
        }
      })}
    </Layer>
  )
}

export default HeatmapLayer

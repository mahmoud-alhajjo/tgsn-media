import React, { useState, useReducer } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { TextValidator } from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import AutoComplete from '../../../components/AutoComplete'
import { Card, CardContent, Grid, Button, CardActions, Typography } from '@material-ui/core';
import { DateRangeInput } from '@datepicker-react/styled'

const useStyles = makeStyles(theme => ({
    card: {
        position: 'absolute',
        left: 0,
        top: 0,
        margin: '20px',
    },
    card2: {
        maxWidth: 450,
        position: 'absolute',
        left: 255,
        top: 75,
        margin: '20px',
        maxHeight: '100vh',
        overflow: 'auto'
    },
})
)

const mapStateToProps = ({ categories: { allItems } }) => {
    return {
        ...allItems
    }
}

const mapDispatchToProps = dispatch => ({
})

const initialState = {
    startDate: null,
    endDate: null,
    focusedInput: null,
}

function reducer(state, action) {
    switch (action.type) {
        case 'focusChange':
            return { ...state, focusedInput: action.payload }
        case 'dateChange': {
            return action.payload
        }
        default:
            throw new Error()
    }
}


const SearchMap = ({
    open,
    handleClose,
    submitting,
    countryId,
    callbackFun,
    submitMapSearchEvents,
    submitMapSearchChoroplethEvents,
    levelMap,
    weapon,
    attack_type,
    target_type,
    target,
    evidence,
    showfilterDate,
    setShowfilterDate,
    showfilterItems,
    setShowfilterItems
}) => {

    const classes = useStyles()
    const [state, dispatch] = useReducer(reducer, initialState)
    var maxBookingDate = new Date()

    const inputProps = {
        margin: 'normal',
        variant: 'outlined',
        fullWidth: true,
        disabled: submitting
    }

    const [formData, setFormData] = useState({
        countryId: parseInt(countryId),
        sinceDate: null,
        toDate: null,
        gteKills: '',
        lteKills: '',
        region: '',
        reportHeading: '',
        weapon: '',
        attack_type: '',
        target_type: '',
        target: '',
        evidence: '',
        other: '',
    })

    const onSubmit = () => {
        callbackFun({
            ...formData,
            sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
            toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
            weapon: formData.weapon,
            attack_type: formData.attack_type,
            target_type: formData.target_type,
            target: formData.target,
            evidence: formData.evidence,
        })
        submitMapSearchEvents(parseInt(countryId), {
            ...formData,
            sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
            toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
            weapon: formData.weapon,
            attack_type: formData.attack_type,
            target_type: formData.target_type,
            target: formData.target,
            evidence: formData.evidence,
        })
        submitMapSearchChoroplethEvents(levelMap, parseInt(countryId), {
            ...formData,
            sinceDate: moment(state.startDate).local().format('DD/MM/YYYY'),
            toDate: moment(state.endDate).local().format('DD/MM/YYYY'),
            weapon: formData.weapon,
            attack_type: formData.attack_type,
            target_type: formData.target_type,
            target: formData.target,
            evidence: formData.evidence,
        })
    }
    return (
        <div>
            {open && <DialogForm
                open={open}
                handleClose={handleClose}
                title='Search Event'
                onSubmit={onSubmit}
                submitting={submitting}
                submitLabel='Search'
            >
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <KeyboardDatePicker
                        disableToolbar
                        variant='inline'
                        margin='normal'
                        inputVariant='outlined'
                        format='DD/MM/YYYY'
                        label='since Date'
                        fullWidth
                        value={formData.sinceDate}
                        onChange={sinceDate => setFormData({ ...formData, sinceDate })}
                        KeyboardButtonProps={{
                            'aria-label': 'change date'
                        }}
                        maxDate={moment()}
                    />
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <KeyboardDatePicker
                        disableToolbar
                        variant='inline'
                        margin='normal'
                        inputVariant='outlined'
                        format='DD/MM/YYYY'
                        label='to Date'
                        fullWidth
                        value={formData.toDate}
                        onChange={toDate => setFormData({ ...formData, toDate })}
                        KeyboardButtonProps={{
                            'aria-label': 'change date'
                        }}
                        maxDate={moment()}
                    />
                </MuiPickersUtilsProvider>
                <TextValidator
                    label='Greater or Equal number of kills'
                    name='gteKills'
                    value={formData.gteKills}
                    onChange={event =>
                        setFormData({ ...formData, gteKills: parseInt(event.target.value) })}
                    type='number'
                    InputProps={{ inputProps: { min: 0 } }}
                    {...inputProps}
                />
                <TextValidator
                    label='Less or Equal number of kills'
                    name='lteKills'
                    value={formData.lteKills}
                    onChange={event =>
                        setFormData({ ...formData, lteKills: parseInt(event.target.value) })}
                    type='number'
                    InputProps={{ inputProps: { min: 0 } }}
                    {...inputProps}
                />
                <TextValidator
                    label='Region'
                    name='region'
                    value={formData.region}
                    onChange={event =>
                        setFormData({ ...formData, region: event.target.value })}
                    {...inputProps}
                />
                <TextValidator
                    label='Report Heading'
                    name='reportHeading'
                    value={formData.reportHeading}
                    onChange={event =>
                        setFormData({
                            ...formData,
                            reportHeading: event.target.value
                        })}
                    multiline
                    rows='2'
                    {...inputProps}
                />
                <AutoComplete
                    label='weapon'
                    options={weapon}
                    onChange={options =>
                        setFormData({
                            ...formData,
                            weapon: options ? options.map(opt => opt.value) : []
                        })}
                    value={weapon.filter(e => formData.weapon.includes(e.id))}
                    isMulti
                    variant='outlined'
                    onCreateOption={() => { }}
                />
                <AutoComplete
                    label='Attack types'
                    options={attack_type}
                    onChange={options =>
                        setFormData({
                            ...formData,
                            attack_type: options ? options.map(opt => opt.value) : []
                        })}
                    value={attack_type.filter(e => formData.attack_type.includes(e.id))}
                    isMulti
                    variant='outlined'
                    onCreateOption={() => { }}
                />
                <AutoComplete
                    label='Targets'
                    options={target}
                    onChange={options =>
                        setFormData({
                            ...formData,
                            target: options ? options.map(opt => opt.value) : []
                        })}
                    value={target.filter(e => formData.target.includes(e.id))}
                    isMulti
                    variant='outlined'
                    onCreateOption={() => { }}
                />
                <AutoComplete
                    label='Targets types'
                    options={target_type}
                    onChange={options =>
                        setFormData({
                            ...formData,
                            target_type: options ? options.map(opt => opt.value) : []
                        })}
                    value={target_type.filter(e => formData.target_type.includes(e.id))}
                    isMulti
                    variant='outlined'
                    onCreateOption={() => { }}
                />
                <AutoComplete
                    label='Evidences'
                    options={evidence}
                    onChange={options =>
                        setFormData({
                            ...formData,
                            evidence: options ? options.map(opt => opt.value) : []
                        })}
                    value={evidence.filter(e => formData.evidence.includes(e.id))}
                    isMulti
                    variant='outlined'
                    onCreateOption={() => { }}
                />
                <TextValidator
                    label='Other'
                    name='other'
                    value={formData.other}
                    onChange={event =>
                        setFormData({
                            ...formData,
                            other: event.target.value
                        })}
                    multiline
                    rows='2'
                    {...inputProps}
                />
            </DialogForm>}
            {showfilterDate && <div className={classes.card}>
                <Grid container justify="space-around">
                    <DateRangeInput
                        onDatesChange={data => {
                            dispatch({
                                type: 'dateChange', payload: data
                            })
                        }}
                        onFocusChange={focusedInput => {
                            dispatch({ type: 'focusChange', payload: focusedInput })
                        }}
                        startDate={state.startDate} // Date or null
                        endDate={state.endDate} // Date or null
                        focusedInput={state.focusedInput} // START_DATE, END_DATE or null
                        displayFormat='dd/MM/yyyy'
                        maxBookingDate={maxBookingDate}
                    />
                    <Button
                        size="small"
                        variant="contained"
                        color="primary"
                        onClick={() => onSubmit()}
                        style={{ height: 35, marginLeft: 5, marginTop: 5 }}
                    >
                        Apply
                    </Button>
                </Grid>
            </div>
            }
            {showfilterItems && <Card className={classes.card2}>
                <CardContent>
                    <Typography component="span" style={{ paddingLeft: 12 }}>Filter your map</Typography>
                    <AutoComplete
                        label='weapon'
                        options={weapon}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                weapon: options ? options.map(opt => opt.value) : []
                            })}
                        value={weapon.filter(e => formData.weapon.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                        style={{ width: 175, marginRight: 10 }}
                    />
                    <AutoComplete
                        label='Attack types'
                        options={attack_type}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                attack_type: options ? options.map(opt => opt.value) : []
                            })}
                        value={attack_type.filter(e => formData.attack_type.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                    />
                    <AutoComplete
                        label='Targets'
                        options={target}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                target: options ? options.map(opt => opt.value) : []
                            })}
                        value={target.filter(e => formData.target.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                    />
                    <AutoComplete
                        label='Targets types'
                        options={target_type}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                target_type: options ? options.map(opt => opt.value) : []
                            })}
                        value={target_type.filter(e => formData.target_type.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                    />
                    <AutoComplete
                        label='Evidences'
                        options={evidence}
                        onChange={options =>
                            setFormData({
                                ...formData,
                                evidence: options ? options.map(opt => opt.value) : []
                            })}
                        value={evidence.filter(e => formData.evidence.includes(e.id))}
                        isMulti
                        variant='outlined'
                        onCreateOption={() => { }}
                    />
                    <CardActions>
                        <Button size="small" color="primary" onClick={() => onSubmit()}>
                            Apply
                        </Button>
                        <Button size="small" color="primary" onClick={() => setShowfilterItems(false)}>
                            Cancel
                         </Button>
                    </CardActions>
                </CardContent>
            </Card>}
        </div>

    )
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchMap)

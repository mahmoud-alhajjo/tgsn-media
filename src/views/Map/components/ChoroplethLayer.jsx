import React, { Fragment, useState } from 'react'
import { connect } from 'react-redux'
import { Layer, GeoJSONLayer, MapContext, Popup } from 'react-mapbox-gl'
import mapboxgl from 'mapbox-gl'

import { getEventsByCoordinate } from '../../../api/helper'
import MapPopup from '../components/MapPopup'
import FullPageLoader from '../../../components/FullPageLoader'
import { makeStyles } from '@material-ui/core/styles'
import { Card, CardContent, Box, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: theme.palette.background.paper,
    maxHeight: '496px',
    width: '175',
    position: 'absolute',
    right: 0,
    top: 0,
    margin: '20px'
  },
  legendBox: {
    width: 25,
    height: 25,
    borderRadius: 6,
    opacity: 0.7,
  }
})
)

const getFirstPart = (str) => {
  let i = str.indexOf("-");
  if (i > 0)
    return parseInt(str.slice(0, i));
  else
    return "";
}
const getSecondPart = (str) => {
  // str like this "0 - 73"
  return parseInt(str.split('-')[1]);
}


const mapStateToProps = ({ map: { mapBoundaries, mapChoropleth, fetchingBoundaries, fetchingChoropleth, legend } }) => {
  return {
    mapBoundaries, mapChoropleth, fetchingBoundaries, fetchingChoropleth, legend
  }
}
// 1 = city, 2 = region, 3 = sub-region
const mapDispatchToProps = dispatch => ({
  dispatchGetEventsByCoordinate: (countryId, coordsId, pageNum) => {
    dispatch(getEventsByCoordinate(countryId, coordsId, pageNum))
  },
  dispatchClearBoundaries: () => {
    dispatch({
      type: 'Cleare_MAP_BOUNDARIES',
    })
  },
  zoomCluster: event => {
    const map = event.target
    map.getCanvas().style.cursor = ''
    var features = map.queryRenderedFeatures(event.point, {
      layers: ['cluster_layer']
    })
    if (!features || features.length === 0) return
    var clusterId = features && features[0].properties.cluster_id
    var pointCount = features && features[0].properties.point_count
    var clusterSource = map.getSource('media_source')

    // Get all points under a cluster
    if (clusterSource) {
      clusterSource.getClusterLeaves(
        clusterId,
        pointCount,
        0,
        (err, cFeatures) => {
          if (cFeatures) {
            const clusterLeaves = cFeatures.map(child => {
              return {
                longitude: child.geometry.coordinates[0],
                latitude: child.geometry.coordinates[1]
              }
            })
            dispatch({
              type: 'MAP_CLUSTER_ZOOM_ACTION',
              payload: {
                clusterLeaves
              }
            })
          }
        }
      )
    }
  }
})

const ChoroplethLayer = ({ countryId, mapChoropleth, levelMap, dispatchGetEventsByCoordinate,
  zoomCluster, mapCoordinates, clusterInChoropleth, colorchoropleth,
  mapBoundaries, fetchingBoundaries, fetchingChoropleth, legend, classification, dispatchClearBoundaries }) => {

  React.useEffect(() => {
    return () => {
      dispatchClearBoundaries()
    };
  }, []);

  const classes = useStyles()
  const [showPopup, setShowPopup] = useState(false)
  const [popupCoordinates, setPopupCoordinates] = useState([])
  const [coordsId, setCoordsId] = useState(null)
  const [infoPopup, setInfoPopup] = useState('')

  let typeClassification = classification === 'kills' ? 'kills_sum' : 'count'
  let legendType = classification === 'kills' ? legend && legend.kills_sum.legend[0] : legend && legend.count.legend[0]

  let arrayOfLegend = []
  if (legendType) {
    if (legendType.length > 0) {
      for (let i = 0; i < legendType.length; i++) {
        let color = colorchoropleth === 'YOR' ? ['#FED976', '#FEB24C', '#FD8D3C', '#FC4E2A', '#E31A1C'] :
          colorchoropleth === 'YGB' ? ['#c7e9b4', '#7fcdbb', '#41b6c4', '#1d91c0', '#225ea8'] :
            ['#d4b9da', '#c994c7', '#df65b0', '#e7298a', '#ce1256']
        if (legendType[i].count > 0) {
          arrayOfLegend.push(getFirstPart(legendType[i].bin))
          arrayOfLegend.push(color[i])
        }
      }
    }
  }

  if (fetchingBoundaries || fetchingChoropleth)
    return <FullPageLoader />;

  const clusterFeatures =
    mapCoordinates &&
    mapCoordinates.map(coordinate => {
      return {
        type: 'Feature',
        properties: {
          id: coordinate.id,
          count: coordinate.count,
          city_name: coordinate.city_name,
          region_name: coordinate.region_name,
          kills_sum: coordinate.kills_sum,
          kills_mean: coordinate.kills_mean,
          longitude: coordinate.longitude,
          latitude: coordinate.latitude
        },
        geometry: {
          type: 'Point',
          coordinates: [coordinate.longitude, coordinate.latitude]
        }
      }
    })
  let mappedFeatures;
  if (mapBoundaries.length > 0) {
    if (levelMap === 1)
      mappedFeatures = mapBoundaries.map((e, i) => {
        let temp = mapChoropleth && mapChoropleth.find(element => element.city_name === e.properties.NAME_1)
        if (temp) {
          typeClassification === 'kills_sum' ? e.properties.kills_sum = temp.kills_sum : e.properties.count = temp.count
          e.properties.id = parseInt(e.id)
          e.properties.details = temp
        }
        return e;
      })
    else if (levelMap === 2)
      mappedFeatures = mapBoundaries.map((e, i) => {
        let temp = mapChoropleth && mapChoropleth.find(element => element.region_name === e.properties.NAME_2)
        if (temp) {
          typeClassification === 'kills_sum' ? e.properties.kills_sum = temp.kills_sum : e.properties.count = temp.count
          e.properties.id = parseInt(e.id)
          e.properties.details = temp
        }
        return e;
      })
    else if (levelMap === 3)
      mappedFeatures = mapBoundaries.map((e, i) => {
        let temp = mapChoropleth && mapChoropleth.find(element => element.subregion_name === e.properties.NAME_3)
        if (temp) {
          typeClassification === 'kills_sum' ? e.properties.kills_sum = temp.kills_sum : e.properties.count = temp.count
          e.properties.id = parseInt(e.id)
          e.properties.details = temp
        }
        return e;
      })
    else mappedFeatures = mapBoundaries;
  }
  
  let color = [0, '#FED976', 73, '#FEB24C', 146, '#FD8D3C', 218, '#FC4E2A', 291, '#E31A1C']
  if (arrayOfLegend.length > 0) { color = [...arrayOfLegend] }
  let hoveredStateId = ''

  return (
    <Fragment>
      <MapContext.Consumer>
        {(map) => {
          // use `map` here
          return <div>
            <GeoJSONLayer
              id='data_source'
              type="geojson"
              data={{
                type: 'FeatureCollection',
                features: mappedFeatures
              }}
            />
            <Layer
              id='choropleth_layer'
              sourceId='data_source'
              paint={{
                'fill-color': [
                  'interpolate',
                  ['linear'],
                  ['get', typeClassification],
                  ...color
                ],
                'fill-opacity': [
                  'case',
                  ['boolean', ['feature-state', 'hover'], false],
                  0.81,
                  0.6
                ]
              }}
              type='fill'
              before='waterway-label'
              onMouseMove={(event) => {
                if (levelMap > 0) {
                  event.target.getCanvas().style.cursor = 'pointer'
                  if (event.features.length > 0) {
                    if (hoveredStateId) {
                      map.setFeatureState(
                        { source: 'data_source', id: hoveredStateId },
                        { hover: false }
                      );
                    }
                    hoveredStateId = event.features[0].properties.id;
                    map.setFeatureState(
                      { source: 'data_source', id: hoveredStateId },
                      { hover: true }
                    );
                  }
                }
              }}
              onMouseLeave={(event) => {
                if (levelMap > 0) {
                  event.target.getCanvas().style.cursor = ''
                  if (hoveredStateId) {
                    map.setFeatureState(
                      { source: 'data_source', id: hoveredStateId },
                      { hover: false }
                    );
                  }
                  hoveredStateId = ''
                }
              }}
              onClick={(event) => {
                if (levelMap > 0) {
                  let details = JSON.parse(event.features[0].properties.details)
                  let area = details.city_name ? "city: " + details.city_name :
                    details.subregion_name ? "subregion: " + details.subregion_name :
                      details.region_name ? "region: " + details.region_name : ''
                  new mapboxgl.Popup()
                    .setLngLat(event.lngLat)
                    .setHTML(`
                    ${area} <br>
                    count: ${details.count} <br>
                    kills sum: ${details.kills_sum} <br>
                    kills mean:  ${details.kills_mean} <br>
                    `)
                    .addTo(map);
                }
              }}
            />
            <Layer
              id='choropleth_outline'
              sourceId='data_source'
              type='line'
              layout={{
                "line-join": "round",
                "line-cap": "round"
              }}
              paint={{
                'line-width': 1,
                'line-color': '#000',
                'line-opacity': 0.6
              }}
            />
          </div>
        }}
      </MapContext.Consumer>
      {clusterInChoropleth &&
        <div>
          <GeoJSONLayer
            sourceOptions={{
              cluster: true,
              clusterMaxZoom: 8,
              clusterRadius: 50,
              clusterProperties: {
                cluster_count: ['+', ['get', 'count']],
                cluster_kills: ['+', ['get', 'kills_sum']]
              }
            }}
            id='media_source'
            data={{
              type: 'FeatureCollection',
              features: clusterFeatures
            }}
          />
          <Layer
            id='cluster_layer'
            sourceId='media_source'
            filter={['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']}
            paint={{
              'circle-color': '#6977C3',
              'circle-radius': {
                property: typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count',
                type: 'interval',
                stops: [[3, 20], [50, 25], [100, 30]]
              },
              'circle-stroke-width': 5,
              'circle-stroke-color': '#aaa',
              'circle-stroke-opacity': 0.5
            }}
            type='circle'
            onClick={event => {
              zoomCluster(event)
              setShowPopup(false)
            }}
            onMouseEnter={event =>
              (event.target.getCanvas().style.cursor = 'pointer')}
            onMouseLeave={event => {
              event.target.getCanvas().style.cursor = ''
              setInfoPopup('')
            }}
          />
          <Layer
            id={typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count'}
            type='symbol'
            sourceId='media_source'
            filter={['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']}
            layout={{
              'text-field': typeClassification === 'kills_sum' ? '{cluster_kills}' : '{cluster_count}',
              'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
              'text-size': 12
            }}
            paint={{
              'text-color': '#fff'
            }}
          />
          <Layer
            id='unclustered_layer'
            sourceId='media_source'
            filter={['!', ['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']]}
            paint={{
              'circle-color': '#6977C3',
              'circle-radius': {
                property: typeClassification,
                type: 'interval',
                stops: [[3, 20], [50, 25], [100, 30]]
              },
              'circle-stroke-width': 5,
              'circle-stroke-color': '#aaa',
              'circle-stroke-opacity': 0.5
            }}
            type='circle'
            onClick={event => {
              dispatchGetEventsByCoordinate(
                countryId,
                event.features[0].properties.id,
                1
              )
              setPopupCoordinates(event.features[0].geometry.coordinates)
              setCoordsId(event.features[0].properties.id)
              setShowPopup(true)
            }}
            onMouseEnter={(event) => {
              !infoPopup && setInfoPopup(event.features[0].properties)
              event.target.getCanvas().style.cursor = 'pointer'
            }}
            onMouseLeave={event => {
              (event.target.getCanvas().style.cursor = '')
              setInfoPopup('')
            }}
          />
          <Layer
            id='unclustered_layer_count'
            type='symbol'
            sourceId='media_source'
            filter={['!', ['has', typeClassification === 'kills_sum' ? 'cluster_kills' : 'cluster_count']]}
            layout={{
              'text-field': `{${typeClassification}}`,
              'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
              'text-size': 12
            }}
            paint={{
              'text-color': '#fff'
            }}
          />
          {infoPopup &&
            <Popup
              key={infoPopup.id}
              coordinates={[infoPopup.longitude, infoPopup.latitude]}
              offset={{
                'bottom-left': [12, -38], 'bottom': [0, -38], 'bottom-right': [-12, -38]
              }}>
              <Typography>City: {infoPopup.city_name}</Typography>
              <Typography>Region: {infoPopup.region_name}</Typography>
              <Typography>Kills sum: {infoPopup.kills_sum}</Typography>
              <Typography>Kills mean: {infoPopup.kills_mean}</Typography>
            </Popup>
          }
          {showPopup &&
            <MapPopup
              coordinates={popupCoordinates}
              handleClosePopup={() => setShowPopup(false)}
              countryId={countryId}
              coordsId={coordsId}
            />}
        </div>
      }
      {(levelMap !== 0) && <div id='legend'><Card className={classes.card}>
        <CardContent>
          <Typography component="h6" variant="h6" style={{ marginBottom: 12 }}>By {classification}</Typography>
          {(legend && legendType.length > 0) && legendType.map((leg, index) => {
            let legendBackgroundColorYOR = index === 0 ? '#FED976' : index === 1 ? '#FEB24C' : index === 2 ? '#FD8D3C' : index === 3 ? '#FC4E2A' : '#E31A1C'
            let legendBackgroundColorYGB = index === 0 ? '#c7e9b4' : index === 1 ? '#7fcdbb' : index === 2 ? '#41b6c4' : index === 3 ? '#1d91c0' : '#225ea8'
            let legendBackgroundColorRP = index === 0 ? '#d4b9da' : index === 1 ? '#c994c7' : index === 2 ? '#df65b0' : index === 3 ? '#e7298a' : '#ce1256'
            let legendBackgroundColor = colorchoropleth === 'YOR' ? legendBackgroundColorYOR : colorchoropleth === 'YGB' ? legendBackgroundColorYGB : legendBackgroundColorRP
            return < Box key={index} style={{ marginBottom: 10 }}>
              {leg.count > 0 && <div><Box className={classes.legendBox} style={{ backgroundColor: legendBackgroundColor, display: 'inline-block', float: 'left' }} />
                <Typography component="span" style={{ paddingLeft: 12 }}>{leg.bin}</Typography>
                <div style={{ clear: 'both' }}></div></div>}
            </Box>
          })
          }
        </CardContent>
      </Card>
      </div>
      }
    </Fragment >
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ChoroplethLayer)

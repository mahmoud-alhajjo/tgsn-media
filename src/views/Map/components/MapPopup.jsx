import React, { Fragment, useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Popup } from 'react-mapbox-gl'
import { connect } from 'react-redux'
import {
  CircularProgress,
  Box,
  Typography,
  IconButton,
  Button
} from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import MapEventDetails from './MapEventDetails'
import { getTrimmedText } from '../../../components/helper'
import { getMoreEventsByCoordinate } from '../../../api/helper'

const useStyles = makeStyles(theme => ({
  root: { maxWidth: '400px', maxHeight: '400px' },
  loader: {
    margin: theme.spacing(5)
  },
  popupContent: {
    maxHeight: '320px',
    overflow: 'auto'
  },
  row: {
    padding: '8px',
    cursor: 'pointer',
    direction: 'rtl'
  }
}))

const mapStateToProps = ({
  map: { coordEvents, fetchingEvents, totalEvents, loadingMoreEvents }
}) => {
  return {
    coordEvents,
    fetchingEvents,
    loadingMoreEvents,
    totalEvents
  }
}
const mapDispatchToProps = dispatch => ({
  dispatchGetEventsByCoordinate: (countryId, coordsId, pageNum) => {
    dispatch(getMoreEventsByCoordinate(countryId, coordsId, pageNum))
  }
})

const MapPopup = ({
  coordinates,
  coordEvents,
  fetchingEvents,
  loadingMoreEvents,
  totalEvents,
  handleClosePopup,
  dispatchGetEventsByCoordinate,
  countryId,
  coordsId
}) => {
  const classes = useStyles()
  const [selectedEvent, setSelectedEvent] = useState(null)
  const [pageNumber, setPageNumber] = useState(1)
  useEffect(
    () => {
      setSelectedEvent(null)
    },
    [coordinates]
  )

  const loadMore = () => {
    let nextPage = pageNumber + 1
    setPageNumber(nextPage)
    dispatchGetEventsByCoordinate(countryId, coordsId, nextPage)
  }

  return fetchingEvents
    ? <Popup
      key='loading-popup'
      coordinates={coordinates}
      >
      <Box className={classes.loader}>
        <CircularProgress />
      </Box>
    </Popup>
    : <Fragment>
      <Popup
        key='details-popup'
        coordinates={coordinates}
        className={classes.root}
        >
        <IconButton
          size='small'
          onClick={() => {
            handleClosePopup()
            setSelectedEvent(null)
          }}
          >
          <CloseIcon fontSize='small' />
        </IconButton>
        <div className={classes.popupContent}>
          {coordEvents.map(event => {
            return (
              <Fragment>
                <div
                  key={event.id}
                  onClick={() => setSelectedEvent(event)}
                  className={classes.row}
                  >
                  <Typography variant='subtitle2' gutterBottom>
                    {getTrimmedText(event.report_heading)}
                  </Typography>
                  <Typography
                    variant='caption'
                    display='block'
                    gutterBottom
                    color='textSecondary'
                    >
                    {event.date}
                  </Typography>
                </div>
                <hr />
              </Fragment>
            )
          })}
          {coordEvents.length < totalEvents &&
          <Button
            onClick={loadMore}
            color='primary'
            style={{ textTransform: 'none' }}
              >
            {loadingMoreEvents ? 'Loading...' : 'Load more'}
          </Button>}
        </div>
      </Popup>
      {selectedEvent &&
      <MapEventDetails
        event={selectedEvent}
        handCloseDetails={() => setSelectedEvent(null)}
          />}
    </Fragment>
}

export default connect(mapStateToProps, mapDispatchToProps)(MapPopup)

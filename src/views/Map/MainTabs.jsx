import React, { useState, useEffect, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Tabs, Tab, Box, Typography, IconButton, Container, FormControl, TextField, MenuItem } from '@material-ui/core';
import { connect } from 'react-redux';
import { getMapCoordinates, getMapBoundaries, getAllCategoriesItems } from '../../api/helper';
import { Map as MapIcon, Equalizer as ChartIcon, DateRange, FilterList } from '@material-ui/icons';
import Map from './Map';
import FullPageLoader from '../../components/FullPageLoader';
import Tooltip from '../../components/Tooltip';
import Statistics from '../Statistics/Statistics';

const useStyles = makeStyles(theme => ({
	tabsTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		'& h6': {
			margin: `${theme.spacing(1)}px ${theme.spacing(5)}px`,
		},
	},
	filterButton: {
		marginLeft: theme.spacing(1),
	},
	selectEmpty: {
		marginTop: 6,
		marginLeft: 14,
	},
}));

const mapStateToProps = ({
	categories: { countriesList, allItems },
	map: { fetching: fetchingMap },
	statistics: { fetching: fetchingStatistics },
}) => {
	return {
		countriesList,
		allItems,
		fetchingMap,
		fetchingStatistics,
	};
};

const mapDispatchToProps = dispatch => ({
	dispatchGetMapCoordinates: countryId => {
		dispatch(getMapCoordinates(countryId));
	},
	dispatchGetMapBoundaries: (levelMap, countryId) => {
		dispatch(getMapBoundaries(levelMap, countryId, 0));
	},
	dispatchGetAllCategoriesItems: () => {
		dispatch(getAllCategoriesItems())
	},
});

function a11yProps(index) {
	return {
		id: `scrollable-force-tab-${index}`,
		'aria-controls': `scrollable-force-tabpanel-${index}`,
	};
}
function TabPanel(props) {
	const { children, value, index } = props;
	return value === index && <Fragment>{children}</Fragment>;
}

const getCountriesID = (str) => {
	if (str) {
		var txt = str;
		var numb = txt.match(/\d/g);
		numb = numb.join("");
		let countries = []
		for (let i = 0; i < numb.length; i++) {
			countries.push(parseInt(numb[i]))
		}
		return countries;
	}
}

const MainTabs = ({
	dispatchGetMapCoordinates,
	dispatchGetMapBoundaries,
	dispatchGetAllCategoriesItems,
	countriesList,
	allItems,
	fetchingMap,
	fetchingStatistics,
	handleSelectCountry,
	worldMap,
	...restProps
}) => {
	const classes = useStyles();
	const countryId = getCountriesID(restProps.match.params.countries);
	let countryName = (countryId && countryId.length === 1) ? 'Single country view' : 'Multiple countries view';
	const [value, setValue] = useState(0);
	const [showfilterDate, setShowfilterDate] = useState(false)
	const [showfilterItems, setShowfilterItems] = useState(false)
	const [iconfilter, setIconfilter] = useState(true)
	const [mapScreen, setMapScreen] = useState(true)
	const [nameClassification, setNameClassification] = useState('incidents')
	const callbackFunfromEvents = (data) => {
		setNameClassification(data)
	}

	useEffect(
		() => {
			dispatchGetMapCoordinates(countryId);
			dispatchGetMapBoundaries(0, countryId);
			dispatchGetAllCategoriesItems()
		},
		// eslint-disable-next-line
		[dispatchGetMapCoordinates, dispatchGetAllCategoriesItems]
	);

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};
	if (!countryId) {
		return (
			<Container className={classes.countryContainer}>
				<FormControl className={classes.formControl}>
					<TextField
						select
						value={''}
						variant="outlined"
						label="Select country"
						onChange={handleSelectCountry}
						InputLabelProps={{ shrink: false }}
						SelectProps={{
							MenuProps: {
								getContentAnchorEl: null,
								anchorOrigin: {
									vertical: 'bottom',
									horizontal: 'left',
								},
							},
						}}
					>
						{countriesList &&
							countriesList.map(c => {
								return (
									<MenuItem value={c.id} key={c.name}>
										{c.name}
									</MenuItem>
								);
							})}
					</TextField>
				</FormControl>
				<img src={worldMap} alt="logo" />
			</Container>
		);
	} else {
		return (
			<Fragment>
				<Box className={classes.tabsTitle}>
					<Typography variant="h6" m={2} gutterBottom>
						{mapScreen ? [countryName, nameClassification].filter(Boolean).join(' - ') : countryName}
						{iconfilter && <span>
							<IconButton aria-label="FilterList" className={classes.filterButton} onClick={() => setShowfilterItems(!showfilterItems)}>
								<FilterList fontSize="inherit" />
							</IconButton>
							<IconButton aria-label="DateRange" className={classes.filterButton} onClick={() => setShowfilterDate(!showfilterDate)}>
								<DateRange fontSize="inherit" />
							</IconButton>
						</span>
						}
					</Typography>
					<Tabs
						value={value}
						onChange={handleChange}
						variant="scrollable"
						scrollButtons="on"
						indicatorColor="primary"
						textColor="primary"
						aria-label="scrollable force tabs example"
					>
						<Tab
							icon={
								<Tooltip title="View incidents map">
									<MapIcon />
								</Tooltip>
							}
							{...a11yProps(0)}
							onClick={() => {
								setIconfilter(true)
								setShowfilterDate(false)
								setShowfilterItems(false)
								setMapScreen(true)
							}}
						/>
						<Tab
							icon={
								<Tooltip title="View incidents statistics">
									<ChartIcon />
								</Tooltip>
							}
							{...a11yProps(1)}
							onClick={() => {
								setIconfilter(true)
								setShowfilterDate(false)
								setShowfilterItems(false)
								setMapScreen(false)
							}}
						/>
					</Tabs>
				</Box>
				<TabPanel value={value} index={0}>
					<Map
						countryId={countryId}
						showfilterDate={showfilterDate}
						setShowfilterDate={setShowfilterDate}
						showfilterItems={showfilterItems}
						setShowfilterItems={setShowfilterItems}
						callbackFunfromEvents={callbackFunfromEvents}
						lengthcountryName={parseInt(countryName.length)}
					/>
					{fetchingMap && <FullPageLoader />}
				</TabPanel>
				<TabPanel value={value} index={1}>
					<Statistics
						countryId={countryId}
						showfilterDate={showfilterDate}
						setShowfilterDate={setShowfilterDate}
						showfilterItems={showfilterItems}
						setShowfilterItems={setShowfilterItems}
						lengthcountryName={parseInt(countryName.length)}
					/>
					{fetchingStatistics && <FullPageLoader />}
				</TabPanel>
			</Fragment>

		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainTabs)

import React, { useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors';
import ReactMapboxGl, { ZoomControl } from 'react-mapbox-gl'
import { setRTLTextPlugin } from 'mapbox-gl'
import MapLayer from './components/MapLayer'
import { Radio, Select, FormControl, MenuItem, Card, CardContent, InputLabel } from '@material-ui/core';
import { getMapChoropleth, getMapBoundaries, getMapCoordinates } from '../../api/helper'
import Search from './components/Search'


const REACT_APP_MAPBOX_ACCESS_TOKEN =
  'pk.eyJ1IjoiYWxhYWhhbWlkYWgiLCJhIjoiY2p6bDdrc3MzMHFkMDNtcG03b245ajdyeSJ9.l7-S0Y1taFlWQGaviyFbvg'

const MAPBOX_RTL_TEXT_PLUGIN_URL = `https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js`

const MapBox = ReactMapboxGl({
  accessToken: REACT_APP_MAPBOX_ACCESS_TOKEN,
  minZoom: 2,
  touchZoomRotate: false,
  preserveDrawingBuffer: true
})

setRTLTextPlugin(MAPBOX_RTL_TEXT_PLUGIN_URL)

const mapStateToProps = ({ map: { bounds, submitting } }) => {
  return {
    bounds,
    submitting,
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchGetMapBoundaries: (levelMap, countryId) => {
    dispatch(getMapBoundaries(levelMap, countryId, 0))
  },
  dispatchGetMapChoropleth: (levelMap, countryId, eventSearchForm) => {
    dispatch(getMapChoropleth(levelMap, countryId, eventSearchForm && eventSearchForm.eventSearchForm))
  },
  submitMapSearchEvents: (countryId, eventSearch) => {
    dispatch(getMapCoordinates(countryId, eventSearch))
  },
  submitMapSearchChoroplethEvents: (levelMap, countryId, eventSearch) => {
    dispatch(getMapChoropleth(levelMap, countryId, eventSearch))
  },
  dispatchClearBoundaries: () => {
    dispatch({
      type: 'Cleare_MAP_BOUNDARIES',
    })
  },
})

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: theme.palette.background.paper,
    maxHeight: '496px',
    width: '215px',
    position: 'absolute',
    left: 0,
    margin: '20px'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(3),
  },
})
)

const GreenRadio = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})(props => <Radio color="default" {...props} />);


const Map = ({
  countryId,
  bounds,
  dispatchGetMapBoundaries,
  dispatchGetMapChoropleth,
  submitting,
  submitMapSearchEvents,
  submitMapSearchChoroplethEvents,
  showfilterDate,
  setShowfilterDate,
  showfilterItems,
  setShowfilterItems,
  callbackFunfromEvents,
  lengthcountryName,
  dispatchClearBoundaries,
}) => {
  const classes = useStyles()
  const [styleMap, setStyleMap] = React.useState("mapbox://styles/mapbox/streets-v8");
  const handleChangeStyleMap = event => {
    setStyleMap(event.target.value);
  };
  const [levelMap, setLevelMap] = useState(0);

  const [eventSearchForm, setEventSearchForm] = useState(null)
  const callbackFun = (data) => {
    setEventSearchForm({ eventSearchForm: data })
  }
  const [classification, setClassification] = useState("incidents");
  const handleChangeClassification = event => {
    callbackFunfromEvents(event.target.value)
    setClassification(event.target.value);
  };
  const handleChangeLevelMap = event => {
    dispatchClearBoundaries()
    setLevelMap(event.target.value);
    dispatchGetMapBoundaries(event.target.value, countryId)
    dispatchGetMapChoropleth(event.target.value, countryId, eventSearchForm)
  };

  const [displayScreenShot, setDisplayScreenShot] = useState('')
  const callbackFunfromMap = (data) => {
    setDisplayScreenShot(data)
  }

  const [choroplethLayer, setChoroplethLayer] = useState(false);
  const [colorchoropleth, setColorchoropleth] = useState('YOR');
  const [clusterInChoropleth, setClusterInChoropleth] = useState(false);
  let subDistrict = false
  if (countryId) {
    for (let i = 0; i < countryId.length; i++) {
      if (countryId[i] !== 1 && countryId[i] !== 2 && countryId.length === 1)
        subDistrict = true
    }
  }
  return (
    <div id="mapbox" style={{ height: `calc(100% - 48px)` }}>
      <Search
        submitMapSearchEvents={(countryId, eventSearch) => submitMapSearchEvents(countryId, eventSearch)}
        submitMapSearchChoroplethEvents={(levelMap, countryId, eventSearch) => submitMapSearchChoroplethEvents(levelMap, countryId, eventSearch)}
        levelMap={levelMap}
        countryId={countryId}
        submitting={submitting}
        choroplethLayer={choroplethLayer}
        callbackFun={callbackFun}
        showfilterDate={showfilterDate}
        setShowfilterDate={setShowfilterDate}
        showfilterItems={showfilterItems}
        setShowfilterItems={setShowfilterItems}
        lengthcountryName={lengthcountryName}
      />
      <MapBox
        // eslint-disable-next-line
        style={styleMap}
        containerStyle={{
          height: '100%',
          width: '100%'
        }}
        fitBounds={bounds}
        fitBoundsOptions={{
          padding: 25
        }}
      >
        <MapLayer countryId={countryId} levelMap={levelMap} setChoroplethLayer={setChoroplethLayer} clusterInChoropleth={clusterInChoropleth} setClusterInChoropleth={setClusterInChoropleth} colorchoropleth={colorchoropleth} classification={classification} callbackFunfromMap={callbackFunfromMap} />
        <Card className={classes.card} style={{ display: `${displayScreenShot}` }}>
          <CardContent>
            <FormControl className={classes.formControl}>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label" style={{ fontSize: 18, color: '#16a085' }}>
                Classification
              </InputLabel>
              <Select
                id="demo-simple-select-placeholder-label"
                value={classification}
                onChange={handleChangeClassification}
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value="incidents">
                  <em>Incidents</em>
                </MenuItem>
                <MenuItem value={'kills'}>Kills</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label" style={{ fontSize: 18 }}>
                Style of Map
              </InputLabel>
              <Select
                id="demo-simple-select-placeholder-label"
                value={styleMap}
                onChange={handleChangeStyleMap}
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value="mapbox://styles/mapbox/streets-v8">
                  <em>Streets</em>
                </MenuItem>
                <MenuItem value={'mapbox://styles/mapbox/light-v10'}>Light</MenuItem>
                <MenuItem value={'mapbox://styles/mapbox/navigation-preview-day-v4'}>Navigation</MenuItem>
              </Select>
            </FormControl>
            {choroplethLayer && <FormControl className={classes.formControl}>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label" style={{ fontSize: 18 }}>
                Level of Map
              </InputLabel>
              <Select
                id="demo-simple-select-placeholder-label"
                value={levelMap}
                onChange={handleChangeLevelMap}
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value={0}>
                  <em>Country</em>
                </MenuItem>
                <MenuItem value={1}>Governorate</MenuItem>
                <MenuItem value={2}>District</MenuItem>

                {subDistrict &&
                  <MenuItem value={3}>Sub District</MenuItem>}
              </Select>
            </FormControl>}
            {choroplethLayer && <FormControl className={classes.formControl}>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label" style={{ fontSize: 18 }}>
                Color
              </InputLabel>
              <Select
                id="demo-simple-select-placeholder-label"
                value={colorchoropleth}
                onChange={event => setColorchoropleth(event.target.value)}
                displayEmpty
                disabled={levelMap === 0 ? true : false}
                className={classes.selectEmpty}
              >
                <MenuItem value={'YOR'}>
                  <em>Yellow-Orange-Red</em>
                </MenuItem>
                <MenuItem value={'YGB'}>Yellow-Green-Blue</MenuItem>
                <MenuItem value={'RP'}>Red-Purple</MenuItem>
              </Select>
            </FormControl>}
            {(choroplethLayer) && <FormControl className={classes.formControl}>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label" style={{ fontSize: 18 }}>
                Cluster
              </InputLabel>
              <div className={classes.selectEmpty}>
                <Radio checked={clusterInChoropleth === false}
                  onChange={() => setClusterInChoropleth(false)} />
                <GreenRadio checked={clusterInChoropleth === true}
                  onChange={() => setClusterInChoropleth(true)} />
              </div>
            </FormControl>}
          </CardContent>
        </Card>
        <ZoomControl position='bottom-right' style={{ bottom: 30, display: displayScreenShot ? `${displayScreenShot}` : 'flex' }} />
      </MapBox>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Map)

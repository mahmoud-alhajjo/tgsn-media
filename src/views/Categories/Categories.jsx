import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core'
import {
    getAllCategoriesItems,
    createCategoryItemForCategoryPage,
    updateCategoriesItem,
    deleteCategoryItem
} from '../../api/helper'
import Tabs from './components/TabsCategoriesItem'

const useStyles = makeStyles(theme => ({
    CategoriesContainer: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4)
    }
}))

const mapStateToProps = ({
    categories: { allItems, fetching, error, submitting, showAddModal, showDeleteDialog, showUpdateModal }
}) => {
    return {
        allItems,
        fetching,
        error,
        submitting,
        showAddModal,
        showDeleteDialog,
        showUpdateModal
    }
}

const mapDispatchToProps = dispatch => ({
    dispatchGetAllCategoriesItems: () => {
        dispatch(getAllCategoriesItems())
    },
    submitcreateCategoryItemForCategoryPage: (categoryName, categoryItemValue) => {
        dispatch(createCategoryItemForCategoryPage(categoryName, categoryItemValue))
    },
    toggleAddModal: showAddModal => {
        dispatch({
            type: 'TOGGLE_CATEGORY_ITEM_ADD',
            payload: {
                showAddModal: showAddModal
            }
        })
    },
    submitDeleteCategoryItem: (categoryItemId, categoryName) => {
        dispatch(deleteCategoryItem(categoryItemId, categoryName))
    },
    toggleDeleteDialog: showDeleteDialog => {
        dispatch({
            type: 'TOGGLE_CATEGORY_ITEM_DELETE',
            payload: {
                showDeleteDialog: showDeleteDialog
            }
        })
    },
    submitUpdateCategoriesItem: (categoryItemId, categoryName, categoryItemUpdate) => {
        dispatch(
            updateCategoriesItem(categoryItemId, categoryName, categoryItemUpdate)
        )
    },
    toggleUpdateModal: showUpdateModal => {
        dispatch({
            type: 'TOGGLE_CATEGORY_ITEM_UPDATE',
            payload: {
                showUpdateModal: showUpdateModal
            }
        })
    },
})

const Categories = ({
    allItems,
    fetching,
    error,
    submitting,
    showAddModal,
    dispatchGetAllCategoriesItems,
    submitcreateCategoryItemForCategoryPage,
    toggleAddModal,
    submitDeleteCategoryItem,
    showDeleteDialog,
    toggleDeleteDialog,
    submitUpdateCategoriesItem,
    showUpdateModal,
    toggleUpdateModal
}) => {
    const classes = useStyles()
    let categoriesTabs = Object.keys(allItems)
    useEffect(
        () => {
            dispatchGetAllCategoriesItems()
        },
        [dispatchGetAllCategoriesItems]
    )
    return (
        <Container maxWidth='xl' className={classes.CategoriesContainer}>
            <Container>
                <Tabs
                    tabs={categoriesTabs}
                    data={allItems}
                    fetching={fetching}
                    onRowAdd={() => toggleAddModal(true)}
                    addTooltip="Add Catygories Item"
                    submitcreateCategoryItemForCategoryPage={submitcreateCategoryItemForCategoryPage}
                    submitting={submitting}
                    showAddModal={showAddModal}
                    toggleAddModal={toggleAddModal}
                    showDeleteDialog={showDeleteDialog}
                    submitDeleteCategoryItem={submitDeleteCategoryItem}
                    toggleDeleteDialog={toggleDeleteDialog}
                    submitUpdateCategoriesItem={submitUpdateCategoriesItem}
                    showUpdateModal={showUpdateModal}
                    toggleUpdateModal={toggleUpdateModal}
                />
            </Container >
        </Container >
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories)

import React, { useState } from 'react'
import { TextValidator } from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: 552,
    }
})

const CreateCategoriesItem = ({ open, handleClose, submitCreateCategoriesItem, submitting, categoryName }) => {
    const [formData, setFormData] = useState({
        categoryItem: '',
    })
    const classes = useStyles();

    return (
        <DialogForm
            open={open}
            handleClose={handleClose}
            title={`Create ${categoryName} Item`}
            onSubmit={() => submitCreateCategoriesItem(categoryName, formData.categoryItem)}
            submitting={submitting}
            submitLabel='Create'
        >
            <TextValidator
                className={classes.root}
                label={`${categoryName} Item`}
                name='categoryItem'
                value={formData.categoryItem}
                onChange={event =>
                    setFormData({ ...formData, categoryItem: event.target.value })}
                variant='outlined'
                margin='normal'
                fullWidth
                disabled={submitting}
                validators={['required']}
                errorMessages={['Category Item is required']}
            />
        </DialogForm>
    )
}

export default CreateCategoriesItem

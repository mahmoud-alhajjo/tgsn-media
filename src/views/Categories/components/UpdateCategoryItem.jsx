import React, { useState } from 'react'
import { TextValidator } from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: 552,
    }
})

const UpdateCategoryItem = ({ open, handleClose, submitUpdateCategoriesItem, submitting, categoryName, CategoriesItemToUpdateName, CategoriesItemToUpdateId }) => {
    const [formData, setFormData] = useState({CategoriesItemToUpdate: CategoriesItemToUpdateName})
    const classes = useStyles();
    return (
        <DialogForm
            open={open}
            handleClose={handleClose}
            title={`Update ${categoryName} Item`}
            onSubmit={() => submitUpdateCategoriesItem(CategoriesItemToUpdateId, categoryName, formData.CategoriesItemToUpdate)}
            submitting={submitting}
            submitLabel='Update'
        >
            <TextValidator
                className={classes.root}
                label={`${categoryName} Item`}
                name='categoryItem'
                value={formData.CategoriesItemToUpdate}
                onChange={event =>
                    setFormData({ ...formData, CategoriesItemToUpdate: event.target.value })}
                variant='outlined'
                margin='normal'
                fullWidth
                disabled={submitting}
                validators={['required']}
                errorMessages={['Category Item is required']}
            />
        </DialogForm>
    )
}

export default UpdateCategoryItem

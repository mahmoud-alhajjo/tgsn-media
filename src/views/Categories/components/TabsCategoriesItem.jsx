import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Table from '../../../components/Table'
import CreateCategoriesItem from './CreateCategoriesItem'
import UpdateCategoryItem from './UpdateCategoryItem'
import DialogConfirmation from '../../../components/DialogConfirmation'


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  }
}));

const removeItemFormobj = (obj) => {
  delete obj['country']
  return obj
}

const SimpleTabs = ({
  tabs,
  data,
  fetching,
  onRowAdd,
  addTooltip,
  submitcreateCategoryItemForCategoryPage,
  submitting,
  toggleAddModal,
  showAddModal,
  showDeleteDialog,
  toggleDeleteDialog,
  submitDeleteCategoryItem,
  submitUpdateCategoriesItem,
  showUpdateModal,
  toggleUpdateModal
}) => {

  const tabsWithoutCountry = (tabs) => {
    if (tabs) {
      for (let i = 0; i < tabs.length; i++) {
        if (tabs[i] === 'country') {
          tabs.splice(i, 1);
        }
      }
    }
  }
  if(tabs) { tabs.length > 0 &&  tabsWithoutCountry(tabs) }
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [pageNumber, setPageNumber] = useState(0)
  const columns = [
    { title: `${tabs[value]} Items`, field: 'name' },
  ]
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const [selectedCategoryItemId, setSelectedCategoryItemId] = useState(null)
  const [CategoriesItemToUpdate, setCategoriesItemToUpdate] = useState(null)
  let categoryItems = data ? removeItemFormobj(data) : null

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} variant="fullWidth" aria-label="simple tabs example">
          {tabs ? tabs.map((tab, index) => {
            return <Tab label={tab} {...a11yProps(index)} key={index} />
          }) : null}
        </Tabs>
      </AppBar>
      {
        tabs ? tabs.map((tab, index) => {
          return <TabPanel value={value} index={index} key={index} >
            {categoryItems && categoryItems[`${tabs[index]}`] ?
              <Table
                style={{ maxWidth: '100%' }}
                columns={columns}
                data={categoryItems[`${tabs[index]}`]}
                isLoading={fetching}
                pageNumber={pageNumber}
                onChangePage={newPage => {
                  setPageNumber(newPage)
                }}
                totalData={categoryItems[`${tabs[index]}`].length}
                rowsPerPage={categoryItems[`${tabs[index]}`].length}
                title='Category'
                allowAdd
                allowDelete
                allowEdit
                onRowAdd={onRowAdd}
                onRowUpdate={CategoriesItemToUpdate => {
                  setCategoriesItemToUpdate(CategoriesItemToUpdate)
                  toggleUpdateModal(true)
                }}
                onRowDelete={categoryItem => {
                  setSelectedCategoryItemId(categoryItem)
                  toggleDeleteDialog(true)
                }}
                addTooltip={addTooltip}
              /> : ""}
          </TabPanel>
        }) : null
      }
      <DialogConfirmation
        open={showDeleteDialog}
        handleClose={() => toggleDeleteDialog(false)}
        handleConfirm={() => submitDeleteCategoryItem(selectedCategoryItemId, tabs[value])}
        title='Delete Confirmation'
        message='Are you sure you want to delete this item ?'
      />
      {showAddModal &&
        <CreateCategoriesItem
          open={showAddModal}
          handleClose={() => toggleAddModal(false)}
          submitCreateCategoriesItem={submitcreateCategoryItemForCategoryPage}
          submitting={submitting}
          categoryName={tabs[value]}
        />}
      {showUpdateModal &&
        <UpdateCategoryItem
          open={showUpdateModal}
          handleClose={() => toggleUpdateModal(false)}
          submitUpdateCategoriesItem={submitUpdateCategoriesItem}
          submitting={submitting}
          categoryName={tabs[value]}
          CategoriesItemToUpdateName={CategoriesItemToUpdate.name}
          CategoriesItemToUpdateId={CategoriesItemToUpdate.id}
        />}
    </div>
  );
}

export default SimpleTabs;
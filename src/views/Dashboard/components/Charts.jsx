import React from 'react';
import { Grid, Card, Container } from '@material-ui/core';
import PieChart from '../../../graphs/PieChart';
import MultiBarGraph from '../../../graphs/MultiBarGraph';
import DateBasedRadarChart from '../../../graphs/DateBasedRadarChart';
import StackedPolarGraph from '../../../graphs/StackedPolarGraph';
import MultiLineGraph from '../../../graphs/MultiLineGraph';
import StackedBarChart from '../../../graphs/StackedBarChart'

const Charts = ({ data }) => {
	return data ? (
		<Container maxWidth="xl" style={{ padding: 20 }}>
			<Grid container spacing={3}>

				<Grid item xs={6}>
					<Card>
						<MultiLineGraph
							data={data.num_events_m}
							chartId="num_events_m_multi_line_graph"
							chartTitle="Total incidents reported per month"
							lines={Object.keys(data.num_events_m[0])}
						></MultiLineGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<MultiLineGraph
							data={data.num_kills_m}
							chartId="num_kills_m_multi_line_graph"
							chartTitle="Total kills reported per month"
							lines={Object.keys(data.num_kills_m[0])}
						></MultiLineGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<PieChart
							data={data.number_of_events_pct}
							chartId="number_of_events_pct_pie_chart"
							value="count_pct"
							category="country_name"
							chartTitle="Total share of incidents reported (percentage)"
						></PieChart>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<PieChart
							data={data.number_of_kills_pct}
							chartId="number_of_kills_pct_pie_chart"
							value="kills_pct"
							category="country_name"
							chartTitle="Total share of kills reported"
						></PieChart>
					</Card>
				</Grid>

				<Grid item xs={12}>
					<Card>
						<MultiBarGraph
							data={data.number_of_events_kills}
							chartId="number_of_events_kills_multibar_graph"
							chartTitle="Total number of incidents and kills reported"
							category="country_name"
							bars={[
								{ field: 'kills', name: 'Kills' },
								{ field: 'count', name: 'Incidents' },
							]}
						></MultiBarGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<StackedPolarGraph
							data={data.top_countries_weapons}
							chartId="top_countries_weapons_polar_chart"
							category="country_name"
							polars={Object.keys(data.top_countries_weapons[0])}
							chartTitle="Weapons trends by country"
						></StackedPolarGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<StackedPolarGraph
							data={data.top_countries_targets_types}
							chartId="top_countries_targets_types_polar_chart"
							category="country_name"
							polars={Object.keys(data.top_countries_targets_types[0])}
							chartTitle="Targeting trends by country"
						></StackedPolarGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<StackedPolarGraph
							data={data.top_countries_attack_types}
							chartId="top_countries_attack_types_polar_chart"
							category="country_name"
							polars={Object.keys(data.top_countries_attack_types[0])}
							chartTitle="Attack types by country"
						></StackedPolarGraph>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<DateBasedRadarChart
							data={data.number_of_events}
							chartId="number_of_events_gantt_chart"
							category="country_name"
							chartTitle="Total number of incidents reported"
							chartTooltip="events"
						></DateBasedRadarChart>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<StackedBarChart
							data={data.num_events_y}
							chartId="num_events_y_stacked_bar_chart"
							chartTitle="Total incidents reported per year"
							bars={Object.keys(data.num_events_y[0])}
						></StackedBarChart>
					</Card>
				</Grid>

				<Grid item xs={6}>
					<Card>
						<StackedBarChart
							data={data.num_kills_y}
							chartId="num_kills_y_stacked_bar_chart"
							chartTitle="Total kills reported per year"
							bars={Object.keys(data.num_kills_y[0])}
						></StackedBarChart>
					</Card>
				</Grid>
			</Grid>
		</Container>
	) : null;
};

export default Charts;

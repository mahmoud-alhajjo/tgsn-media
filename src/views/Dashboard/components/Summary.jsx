import React from 'react';
import moment from 'moment';
import { Card, Grid, Typography, Box, LinearProgress, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
	card: {
		padding: 20,
		'& div': {
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
		},
	},
}));

const Summary = ({ data }) => {
	const classes = useStyles();
	return (
		<Container maxWidth="xl" style={{ padding: '0 20px' }}>
			<Card style={{ padding: 20, marginBottom: 20 }}>
				<Grid container spacing={5} alignItems="center">
					<Grid item xs={3}>
						<Typography variant="subtitle1" align="center">
							<b>Incidents</b>
						</Typography>
						<Typography variant="h3" style={{ color: '#aaa', marginBottom: 16 }} align="center">
							<b>{data.number_of_events}</b>
						</Typography>
					</Grid>
					<Grid item xs={3}>
						<Typography variant="subtitle1" align="center">
							<b>Kills </b>
						</Typography>
						<Typography variant="h3" style={{ color: '#aaa' }} align="center">
							<b>{data.number_of_kills}</b>
						</Typography>
					</Grid>
					<Grid item xs={6} style={{ display: 'flex', justifyContent: 'space-around' }}>
						<div>
							<Typography variant="subtitle1" align="center">
								<b>From</b>
							</Typography>
							<Typography
								variant="h4"
								color="textSecondary"
								align="center"
								style={{
									whiteSpace: 'nowrap',
									color: '#aaa',
								}}
							>
								<b>{moment(data.from_date).format('DD MMM YY')}</b>
							</Typography>
						</div>
						<div>
							<Typography variant="subtitle1" align="center">
								<b>To</b>
							</Typography>
							<Typography
								variant="h4"
								color="textSecondary"
								align="center"
								style={{
									whiteSpace: 'nowrap',
									color: '#aaa',
								}}
							>
								<b>{moment(data.to_date).format('DD MMM YY')}</b>
							</Typography>
						</div>
					</Grid>
				</Grid>
			</Card>
			<Grid container spacing={5} alignItems="center">
				<Grid item xs={4}>
					{data.top_country_events && (
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most incidents reported in</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_country_events.country_name}</b>
								</Typography>
								<Typography variant="h4" style={{ color: '#aaa' }}>
									{data.top_country_events.count}
								</Typography>
							</div>
						</Card>
					)}
				</Grid>
				<Grid item xs={4}>
					{data.top_country_kills && (
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most kills reported in</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_country_kills.country_name}</b>
								</Typography>
								<Typography variant="h4" style={{ color: '#aaa' }}>
									{data.top_country_kills.kills}
								</Typography>
							</div>
						</Card>
					)}
				</Grid>
				<Grid item xs={4}>
					{data.top_weapon && (
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently used weapon</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_weapon.weapon_name}</b>
								</Typography>
								<Typography variant="h4" style={{ color: '#aaa' }}>
									{data.top_weapon.weapons_counts}
								</Typography>
							</div>
						</Card>
					)}
				</Grid>
			</Grid>
			<Grid container spacing={5} alignItems="center">
				<Grid item xs={4}>
					{data.top_target_type && (
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_target_type.target_type_name}</b>
								</Typography>
								<Typography variant="h4" style={{ color: '#aaa' }}>
									{data.top_target_type.targets_types_counts}
								</Typography>
							</div>
						</Card>
					)}
				</Grid>
				<Grid item xs={4}>
					{data.top_attack_type && (
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most common attack type</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_attack_type.attack_type_name}</b>
								</Typography>
								<Typography variant="h4" style={{ color: '#aaa' }}>
									{data.top_attack_type.attack_types_counts}
								</Typography>
							</div>
						</Card>
					)}
				</Grid>
			</Grid>
		</Container>
	);
};

export default Summary;

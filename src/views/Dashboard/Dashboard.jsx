import React, { useEffect, Fragment, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { MenuItem, FormControl, Container, Grid, Select, Box, Input, Button, Typography } from '@material-ui/core';
import { connect } from 'react-redux';

import { getCountriesList, getDashboardStatistics } from '../../api/helper';
import Summary from './components/Summary';
import Charts from './components/Charts';
import FullPageLoader from '../../components/FullPageLoader';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
	dashboardContainer: {
		marginTop: theme.spacing(4),
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		'& img': {
			margin: theme.spacing(3),
		},
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120,
		maxWidth: 300,
		marginLeft: 0,
	},
	noLabel: {
		marginTop: theme.spacing(3),
	},
}));

function getStyles(name, personName, theme) {
	return {
		fontWeight:
			personName.indexOf(name) === -1
				? theme.typography.fontWeightRegular
				: theme.typography.fontWeightMedium,
	};
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};

const mapStateToProps = ({
	categories: { countriesList },
	dashboard: { dashboardSummary, dashboardStatistics, fetching },
}) => {
	return {
		countriesList,
		dashboardSummary,
		dashboardStatistics,
		fetchingDashboard: fetching,
	};
};

const mapDispatchToProps = dispatch => ({
	dispatchGetCountriesList: () => {
		dispatch(getCountriesList());
	},
	dispatchGetDashboardStatistics: () => {
		dispatch(getDashboardStatistics());
	},
});

const Dashboard = ({
	dispatchGetCountriesList,
	dispatchGetDashboardStatistics,
	countriesList,
	dashboardSummary,
	dashboardStatistics,
	fetchingDashboard,
	...restProps
}) => {
	const theme = useTheme();
	const classes = useStyles();
	const [countriesMap, setCountriesMap] = useState([]);

	const handleSelectCountryForMap = event => {
		setCountriesMap(event.target.value);
	};
	const handleClose = () => {
		if (countriesMap) {
			if (countriesMap.length > 0) {
				let countries = countriesMap.join('&');
				restProps.history.push('/map/' + countries);
			}
		}
	}
	const handleSelectCountry = event => {
		let selectedCountry = event.target.value;
		if (selectedCountry) {
			restProps.history.push('/events/' + selectedCountry);
		}
	};

	useEffect(
		() => {
			dispatchGetCountriesList();
			dispatchGetDashboardStatistics();
		},
		// eslint-disable-next-line
		[dispatchGetCountriesList, dispatchGetDashboardStatistics]
	);
	return (
		<Container maxWidth="xl" className={classes.dashboardContainer}>
			<Grid
				container
				direction="row"
				justify="space-evenly"
				alignItems="center"
				style={{ marginBottom: 26 }}
			>
				<Box>
					<Typography variant="h6" m={2} style={{ fontSize: 17 }}>Data entry</Typography>
					<Grid
						container
						direction="row"
						justify="space-evenly"
						alignItems="flex-start"
						style={{ marginBottom: 26 }}
					>
						<FormControl className={clsx(classes.formControl, classes.noLabel)}>
							<Select
								displayEmpty
								value={''}
								onChange={handleSelectCountry}
								input={<Input />}
								renderValue={selected => {
									if (selected.length === 0) {
										return <em>Countries</em>;
									}
								}}
								MenuProps={MenuProps}
							>
								<MenuItem disabled value="">
									<em>Countries</em>
								</MenuItem>
								{countriesList && countriesList.map((c, index) => (
									<MenuItem key={index} value={c.id} style={getStyles(c.name, countriesMap, theme)}>
										{c.name}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>
				</Box>
				<Box>
					<Typography variant="h6" m={2} style={{ fontSize: 17 }}>View map and statistics</Typography>
					<Grid
						container
						direction="row"
						justify="space-evenly"
						alignItems="center"
						style={{ marginBottom: 26 }}
					>
						<FormControl className={clsx(classes.formControl, classes.noLabel)}>
							<Select
								multiple
								displayEmpty
								value={countriesMap}
								onChange={handleSelectCountryForMap}
								input={<Input />}
								renderValue={selected => {
									if (selected.length === 0) {
										return <em>Countries</em>;
									}
									let strr = [];
									let countryName = '';
									for (let i = 0; i < selected.length; i++) {
										countriesList.length > 0 && strr.push(countriesList.find((country) => {
											return country.id === selected[i]
										}).name)
									}
									countryName = strr.join(', ')
									return countryName && countryName
								}}
								MenuProps={MenuProps}
							>
								<MenuItem disabled value="">
									<em>Countries</em>
								</MenuItem>
								{countriesList && countriesList.map((c, index) => (
									<MenuItem key={index} value={c.id} style={getStyles(c.name, countriesMap, theme)}>
										{c.name}
									</MenuItem>
								))}
							</Select>
						</FormControl>
						<Button
							variant="contained"
							color="primary"
							onClick={handleClose}
							disabled={(countriesMap && countriesMap.length > 0) ? false : true}
							style={{ marginLeft: 17, marginTop: 18, textTransform: 'capitalize' }}
						>
							View
     					 </Button>
					</Grid>
				</Box>
			</Grid>
			{fetchingDashboard ? (
				<FullPageLoader />
			) : (
					<Fragment>
						<Summary data={dashboardSummary} />
						<Charts data={dashboardStatistics} />
					</Fragment>
				)}
		</Container>
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

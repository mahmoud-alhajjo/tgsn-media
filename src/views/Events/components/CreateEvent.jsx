import React, { useState } from 'react'
import { connect } from 'react-redux'
import {
  TextValidator,
  SelectValidator
} from 'react-material-ui-form-validator'
import DialogForm from '../../../components/DialogForm'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import { MenuItem } from '@material-ui/core'
import AutoComplete from '../../../components/AutoComplete'
import { kills_status } from './constants'
import { createEvent, createCategoryItem, getMapGeocoding } from '../../../api/helper'
import Button from '@material-ui/core/Button'
import LocationsAvailable from './LocationsAvailable'
import Files from "react-butterfiles"
// import languageSolid from "./../../../Assets/languageSolid.svg"

const mapStateToProps = ({
  categories: { allItems },
  events: { submitting },
  map: { mapGeocoding }
}) => {
  return {
    ...allItems,
    submitting,
    mapGeocoding
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchCreateCategoryItem: (
    categoryName,
    categoryItemValue,
    // countryId,
    updateFormDataCategoryItems
  ) => {
    dispatch(
      createCategoryItem(
        categoryName,
        categoryItemValue,
        updateFormDataCategoryItems
      )
    )
  },
  submitCreateEvent: (event, countryId, file) => {
    dispatch(createEvent(event, countryId, file))
  },
  dispatchGetMapGeocoding: (countryName, region, location) => {
    dispatch(getMapGeocoding(countryName, region, location))
  },
  emptyingOfMapGeocoding: () => {
    dispatch({
      type: 'EMPTYING_OF_MAP_GEOCODING',
      payload: {
        mapGeocoding: []
      }
    })
  }
})

const CreateEvent = ({
  open,
  handleClose,
  submitCreateEvent,
  submitting,
  countryId,
  dispatchCreateCategoryItem,
  weapon,
  attack_type,
  target_type,
  target,
  evidence,
  countryName,
  //city: cities
  dispatchGetMapGeocoding,
  emptyingOfMapGeocoding,
  mapGeocoding
}) => {
  const [formData, setFormData] = useState({
    country_id: parseInt(countryId),
    date: null,
    report_heading: '',
    location: '',
    region: '',
    lng: '',
    lat: '',
    kills_status: '',
    kills: 0,
    other: '',
    // city_id: null,
    weapon: [],
    attack_type: [],
    target: [],
    target_type: [],
    evidence: []
  })

  const [specificRegion, setSpecificRegion] = useState('')
  const updateFormDataCategoryItems = ({ categoryName, newItem }) => {
    const newFormData = formData
    newFormData[categoryName].push(newItem.id)
    setFormData({ ...newFormData })
  }
  const CheckRegion = () => {
    dispatchGetMapGeocoding(countryName, formData.region, formData.location)
  }
  const [openMap, setOpenMap] = useState(false);
  const viewMap = () => {
    setOpenMap(true)
  }
  const inputProps = {
    margin: 'normal',
    variant: 'outlined',
    fullWidth: true,
    disabled: submitting
  }
  const menuProps = {
    MenuProps: {
      style: { maxHeight: '40%' },
      getContentAnchorEl: null,
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'left'
      }
    }
  }
  const [files, setFiles] = useState('')
  const [errorsFile, setErrorsFile] = useState('')
  return (
    <div>
      {openMap &&
        < LocationsAvailable
          openMap={openMap}
          handleClose={() => { setOpenMap(false) }}
          mapGeocoding={mapGeocoding}
        />
      }
      <DialogForm
        open={open}
        handleClose={handleClose}
        title='Create Incident'
        onSubmit={() => {
          submitCreateEvent(
            {
              ...formData,
              date: moment(formData.date).local().format('DD/MM/YYYY')
            },
            countryId,
            files,
          )
          setTimeout(() => emptyingOfMapGeocoding(), 1000)
        }
        }
        submitting={submitting}
        submitLabel='Create'
      >
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant='inline'
            margin='normal'
            inputVariant='outlined'
            format='DD/MM/YYYY'
            label='Date'
            fullWidth
            value={formData.date}
            onChange={date => setFormData({ ...formData, date })}
            KeyboardButtonProps={{
              'aria-label': 'change date'
            }}
            required
            maxDate={moment()}
          />
        </MuiPickersUtilsProvider>
        <TextValidator
          label='Report Heading'
          name='report_heading'
          value={formData.report_heading}
          onChange={event =>
            setFormData({
              ...formData,
              report_heading: event.target.value
            })}
          multiline
          rows='2'
          validators={['required']}
          required
          errorMessages={['Report Heading is required']}
          {...inputProps}
        />
      
        {/* <button onClick={()=> showTranslate()} className="translate-icon-button" >
           <img src={languageSolid} alt="translate-icon" className="translate-icon"></img>
        </button> */}
        <TextValidator
          label='Location'
          name='location'
          value={formData.location}
          onChange={event =>
            setFormData({ ...formData, location: event.target.value })}
          validators={['required']}
          required
          errorMessages={['Location address is required']}
          {...inputProps}
        />
        <TextValidator
          label='Region'
          name='region'
          value={formData.region}
          onChange={event =>
            setFormData({ ...formData, region: event.target.value })}
          validators={['required']}
          required
          errorMessages={['Region is required']}
          {...inputProps}
        />

        <Button
          variant="contained"
          color="primary"
          onClick={CheckRegion}
          disabled={formData.location && formData.region ? false : true}
          style={{textTransform: 'none'}}
        >
          Check the location
      </Button>

        <SelectValidator
          label='Select exact Location'
          SelectProps={{
            ...menuProps
          }}
          value={specificRegion}
          onChange={event => {
            setSpecificRegion(event.target.value)
            setFormData({ ...formData, lng: event.target.value.lng, lat: event.target.value.lat })
          }}
          validators={['required']}
          required
          errorMessages={['Specific Region is required']}
          {...inputProps}
          disabled={mapGeocoding.length === 0 ? true : false}
        >
          {mapGeocoding.map((item, index) =>
            <MenuItem key={index} value={item}>
              {item.description}
            </MenuItem>
          )}
        </SelectValidator>

        <Button
          variant="contained"
          color="secondary"
          onClick={viewMap}
          disabled={mapGeocoding.length === 0 ? true : false}
          style={{textTransform: 'none'}}
        >
          View map
      </Button>

        {/* <AutoComplete
        label='City'
        options={cities}
        onChange={options =>
          setFormData({ ...formData, city_id: options ? options.value : null })}
        value={cities.find(e => formData.city_id === e.id)}
        variant='outlined'
        // onCreateOption={(value) => dispatchCreateCategoryItem('city', value)}
      /> */}

        <AutoComplete
          label='weapon'
          options={weapon}
          onChange={options =>
            setFormData({
              ...formData,
              weapon: options ? options.map(opt => opt.value) : []
            })}
          value={weapon.filter(e => formData.weapon.includes(e.id))}
          isMulti
          variant='outlined'
          onCreateOption={value =>
            dispatchCreateCategoryItem(
              'weapon',
              value,
              updateFormDataCategoryItems
            )}
            sizeChip ={'medium'}
            sizeInput={''}
        />

        <AutoComplete
          label='Attack types'
          options={attack_type}
          onChange={options =>
            setFormData({
              ...formData,
              attack_type: options ? options.map(opt => opt.value) : []
            })}
          value={attack_type.filter(e => formData.attack_type.includes(e.id))}
          isMulti
          variant='outlined'
          onCreateOption={value =>
            dispatchCreateCategoryItem(
              'attack_type',
              value,
              updateFormDataCategoryItems
            )}
            sizeChip ={'medium'}
            sizeInput={''}
        />

        <AutoComplete
          label='Targets'
          options={target}
          onChange={options =>
            setFormData({
              ...formData,
              target: options ? options.map(opt => opt.value) : []
            })}
          value={target.filter(e => formData.target.includes(e.id))}
          isMulti
          variant='outlined'
          onCreateOption={value =>
            dispatchCreateCategoryItem(
              'target',
              value,
              updateFormDataCategoryItems
            )}
            sizeChip ={'medium'}
            sizeInput={''}
        />

        <AutoComplete
          label='Targets types'
          options={target_type}
          onChange={options =>
            setFormData({
              ...formData,
              target_type: options ? options.map(opt => opt.value) : []
            })}
          value={target_type.filter(e => formData.target_type.includes(e.id))}
          isMulti
          variant='outlined'
          onCreateOption={value =>
            dispatchCreateCategoryItem(
              'target_type',
              value,
              updateFormDataCategoryItems
            )}
            sizeChip ={'medium'}
            sizeInput={''}
        />

        <SelectValidator
          label='Kills'
          SelectProps={{
            ...menuProps
          }}
          value={formData.kills_status}
          onChange={event =>
            setFormData({
              ...formData,
              kills_status: event.target.value,
              kills: 0
            })}
          validators={['required']}
          required
          errorMessages={['Kills is required']}
          {...inputProps}
        >
          {kills_status.map(item =>
            <MenuItem key={item.id} value={item.id}>
              {item.name}
            </MenuItem>
          )}
        </SelectValidator>

        {(formData.kills_status === 0 || formData.kills_status === 1) &&
          <TextValidator
            label='Kill numbers'
            name='killNumbers'
            value={formData.kills}
            onChange={event =>
              setFormData({ ...formData, kills: parseInt(event.target.value) })}
            type='number'
            InputProps={{ inputProps: { min: formData.kills_status } }}
            validators={['required']}
            errorMessages={['Kills is required']}
            {...inputProps}
            disabled={submitting || formData.kills_status !== 1}
          />}

        <TextValidator
          label='Other'
          name='other'
          value={formData.other}
          onChange={event =>
            setFormData({
              ...formData,
              other: event.target.value
            })}
          multiline
          rows='2'
          {...inputProps}
        />

        <AutoComplete
          label='Evidences'
          options={evidence}
          onChange={options =>
            setFormData({
              ...formData,
              evidence: options ? options.map(opt => opt.value) : []
            })}
          value={evidence.filter(e => formData.evidence.includes(e.id))}
          isMulti
          variant='outlined'
          onCreateOption={value =>
            dispatchCreateCategoryItem(
              'evidence',
              value,
              updateFormDataCategoryItems
            )}
            sizeChip ={'medium'}
            sizeInput={''}
        />

        <Files
          multiple={true}
          maxSize="100mb"
          multipleMaxSize="500mb"
          accept={[
            "application/pdf, .doc, .docx, .xlsx, .csv",
            'application/pdf',
            "text/plain",
            "image/jpg",
            "image/jpeg",
            "image/png",
            "image/tiff",
            "image/tif",
            "image/gif",
            "video/mp4",
            ".17",
            "",
          ]}
          onSuccess={files => setFiles({ files })}
          onError={errors => setErrorsFile({ errors })}
        >
          {({ browseFiles, getDropZoneProps }) => {
            return (
              <div>
                <label style={{ marginTop: 10 }}>Drag and drop files.</label>
                <div
                  {...getDropZoneProps({
                    style: {
                      width: 500,
                      minHeight: 200,
                      border: "2px lightgray dashed",
                      marginTop: 10,
                      marginBottom: 10
                    }
                  })}
                >
                  <ol>
                    {files && files.files.map((file, index) => (
                      <li key={file.id}>{file.name}</li>
                    ))}
                    {errorsFile && errorsFile.errors.map((error, index) => (
                      <li key={error.id}>
                        {error.file ? (
                          <span style={{ color: 'red' }}>
                            {error.file.name} - {error.type}
                          </span>
                        ) : (
                            error.type
                          )}
                      </li>
                    ))}
                  </ol>
                </div>
                <div>
                  Dragging not convenient? Click{" "}
                  <button type="button" onClick={browseFiles}>here</button> to select files.
                </div>
              </div>
            );
          }}
        </Files>

      </DialogForm>
    </div >
  )
  function showTranslate(eventId){ 
        var x =  document.getElementById(eventId);
        if (x.style.display === "none") {
          x.style.display = "block";
        } else {
          x.style.display = "none";
        }
      }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEvent)

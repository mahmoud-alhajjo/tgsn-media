import React, { useState } from 'react';
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import ReactMapboxGl, { Layer, Feature, ZoomControl, Popup } from 'react-mapbox-gl';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoiYWxhYWhhbWlkYWgiLCJhIjoiY2p6bDdrc3MzMHFkMDNtcG03b245ajdyeSJ9.l7-S0Y1taFlWQGaviyFbvg',
  minZoom: 2,
  touchZoomRotate: false
});

const mapStateToProps = ({ map: { bounds } }) => {
  return {
    bounds
  }
}

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const LocationsAvailable = ({ openMap, handleClose, bounds, mapGeocoding }) => {
  const classes = useStyles();
  const [showPopup, setShowPopup] = useState(false)
  const [infoPopup, setInfoPopup] = useState('')
  console.log('bounds', bounds)
  return (
    <div>
      <Dialog fullScreen open={openMap} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              View Locations
            </Typography>
          </Toolbar>
        </AppBar>
        <Map
          // eslint-disable-next-line
          style={'mapbox://styles/mapbox/streets-v8'}
          containerStyle={{
            height: '100%',
            width: '100%'
          }}
          fitBounds={bounds}
          fitBoundsOptions={{
            padding: 25
          }}
        >
          <Layer type="circle" id="marker"
            paint={{
              'circle-color': '#6977C3',
              'circle-radius': {
                property: 'cluster_count',
                type: 'interval',
                stops: [[3, 20], [50, 25], [100, 30]]
              },
              'circle-stroke-width': 10,
              'circle-stroke-color': '#6977C3',
              'circle-stroke-opacity': 0.7
            }}
            onMouseEnter={event => {
              (event.target.getCanvas().style.cursor = 'pointer')
            }}
            onMouseLeave={event => {
              (event.target.getCanvas().style.cursor = '')
              setShowPopup(false)
            }}
          >
            {mapGeocoding.map((coordinate, index) => {
              return <Feature key={index} coordinates={[coordinate.lng, coordinate.lat]}
                onClick={() => {
                  setShowPopup(true)
                  setInfoPopup({ coordinate, index })
                }}
              />
            })
            }
          </Layer>
          {(showPopup && infoPopup) &&
            <Popup
              key={infoPopup.index}
              coordinates={[infoPopup.coordinate.lng, infoPopup.coordinate.lat]}
              offset={{
                'bottom-left': [12, -38], 'bottom': [0, -38], 'bottom-right': [-12, -38]
              }}>
              <h1>{infoPopup.coordinate.description}</h1>
            </Popup>
          }
          <ZoomControl position='bottom-right' style={{ bottom: '30px' }} />
        </Map>
      </Dialog>
    </div>
  );
}

export default connect(mapStateToProps)(LocationsAvailable);
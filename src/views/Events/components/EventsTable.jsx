import React, { useState, useEffect } from 'react'
import Table from '../../../components/Table'
import { makeStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core'
import { connect } from 'react-redux'
import {
  getAllEvents,
  getAllCategoriesItems,
} from '../../../api/helper'
import CreateEvent from './CreateEvent'
import ViewEvent from './ViewEvent'
import { getTrimmedText } from '../../../components/helper'
import Search from '../../Map/components/Search'

const useStyles = makeStyles(theme => ({
  detailsContainer: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    marginLeft: theme.spacing(3)
  },
  eventsContainer: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: theme.spacing(2)
  }
}))

const mapStateToProps = ({
  events: {
    allEvents,
    fetching,
    totalData,
    showAddModal,
    showAddModalFilter,
    submitting
  },
  eventDetails: { eventDetails, isSelected },
  map: { mapGeocoding }
}) => {
  return {
    allEvents,
    fetching,
    totalData,
    showAddModal,
    eventDetails,
    isSelected,
    mapGeocoding,
    showAddModalFilter,
    submitting
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchGetEvents: (pageNumber, countryId) => {
    dispatch(getAllEvents({ pageNum: pageNumber + 1, countryId }))
  },
  submitSearchEvents: (eventSearch) => {
    dispatch(getAllEvents(eventSearch))
  },
  dispatchGetAllCategoriesItems: () => {
    dispatch(getAllCategoriesItems())
  },
  toggleAddModalFilter: showAddModalFilter => {
    dispatch({
      type: 'TOGGLE_SEARCH_EVENTS',
      payload: {
        showAddModalFilter: showAddModalFilter
      }
    })
  },
  toggleAddModal: showAddModal => {
    dispatch({
      type: 'TOGGLE_EVENT_ADD',
      payload: {
        showAddModal: showAddModal
      }
    })
  },
  toggleViewEvent: event => {
    dispatch({
      type: 'TOGGLE_EVENT_VIEW',
      payload: {
        eventDetails: event
      }
    })
  },
  emptyingOfMapGeocoding: () => {
    dispatch({
      type: 'EMPTYING_OF_MAP_GEOCODING',
      payload: {
        mapGeocoding: []
      }
    })
  }
})

const EventsTable = ({
  dispatchGetEvents,
  dispatchGetAllCategoriesItems,
  allEvents,
  fetching,
  totalData,
  toggleAddModal,
  toggleViewEvent,
  showAddModal,
  eventDetails,
  isSelected,
  countryId,
  countryName,
  emptyingOfMapGeocoding,
  submitSearchEvents,
  toggleAddModalFilter,
  showAddModalFilter,
  submitting,
  showfilterDate, setShowfilterDate, showfilterItems, setShowfilterItems, lengthcountryName
}) => {

  useEffect(
    () => {
      dispatchGetEvents(pageNumber, countryId)
      dispatchGetAllCategoriesItems()
    },
    // eslint-disable-next-line
    [dispatchGetEvents, dispatchGetAllCategoriesItems]
  )
  const classes = useStyles()

  const [pageNumber, setPageNumber] = useState(0)

  return (
    <Container maxWidth='xl' className={classes.eventsContainer} style={{ marginBottom: 25 }}>
      <Search
        open={showAddModalFilter}
        handleClose={() => toggleAddModalFilter(false)}
        submitSearchEvents={(eventSearch) => submitSearchEvents(eventSearch)}
        countryId={countryId}
        submitting={submitting}
        // callbackFun={callbackFun}
        showfilterDate={showfilterDate}
        setShowfilterDate={setShowfilterDate}
        showfilterItems={showfilterItems}
        setShowfilterItems={setShowfilterItems}
        styleForPAgeEvent={true}
        lengthcountryName={lengthcountryName}
      />
      {showAddModal &&
        <CreateEvent
          open={showAddModal}
          countryId={countryId}
          countryName={countryName}
          handleClose={() => {
            toggleAddModal(false)
            emptyingOfMapGeocoding()
          }}
        />}
      <Table
        style={{ width: '100%' }}
        columns={[
          { title: 'Date', field: 'date' },
          {
            title: 'Report heading',
            field: 'report_heading',
            render: rowData => getTrimmedText(rowData.report_heading),
            cellStyle: {
              maxWidth: '320px',
              direction: 'rtl'
            }
          },
          { title: 'Attack types', field: 'attack_types[0].name' },
          { title: 'Target types', field: 'targets_types[0].name' }
        ]}
        data={allEvents}
        pageNumber={pageNumber}
        totalData={totalData}
        isLoading={fetching}
        onRowAdd={() => toggleAddModal(true)}
        allowAdd
        rowsPerPage={10}
        onRowClick={(event, rowData) => toggleViewEvent(rowData)}
        onChangePage={newPage => {
          setPageNumber(newPage)
          dispatchGetEvents(newPage, countryId)
        }}
        title={''}
        selectedRow={eventDetails}
        addTooltip='Add incident'
      />

      <Container maxWidth='xs' className={classes.detailsContainer}>
        {eventDetails
          ? <ViewEvent
            key={eventDetails.id}
            event={eventDetails}
            isSelected={isSelected}
            countryId={countryId}
            countryName={countryName}
            pageNum={pageNumber}
          />
          : <div />}
      </Container>
    </Container>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(EventsTable)

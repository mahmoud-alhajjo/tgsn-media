export const kills_status = [
  {id: 0, name : 'No-one was killed'},
  {id: 1, name : 'People were killed and we know how many'},
  {id: 2, name : 'People were killed but we don\'t know how many'},
  {id: 3, name : 'We don\'t know if anyone was killed or not'}
];
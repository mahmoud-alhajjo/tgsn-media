import React, { useState, Fragment, useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { InputLabel, MenuItem, IconButton } from '@material-ui/core'
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Save as SaveIcon,
  Close as CloseIcon,
  Translate
} from '@material-ui/icons'
import {
  ValidatorForm,
  TextValidator,
  SelectValidator
} from 'react-material-ui-form-validator'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import Files from "react-butterfiles"
import DialogConfirmation from '../../../components/DialogConfirmation'
import Tooltip from '../../../components/Tooltip'
import { kills_status } from './constants'
import AutoComplete from '../../../components/AutoComplete'
import { updateEvent, deleteEvent, createCategoryItem, getMapGeocodingUpdate, getFile, deleteFile } from '../../../api/helper'
import Button from '@material-ui/core/Button'
import LocationsAvailable from './LocationsAvailable'
export const googleTranslate = require("google-translate")("AIzaSyDxGiqSYhE_goREwZunphHueYbFK6WAvxQ");

const mapStateToProps = ({
  categories: { allItems },
  events: { showUpdateForm, showDeleteDialog, fileUrl },
  map: { mapGeocodingUpdate, selectedOldRegion }
}) => {
  return {
    ...allItems,
    showUpdateForm,
    showDeleteDialog,
    mapGeocodingUpdate,
    selectedOldRegion,
    fileUrl
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchCreateCategoryItem: (
    categoryName,
    categoryItemValue,
    updateFormDataCategoryItems
  ) => {
    dispatch(
      createCategoryItem(
        categoryName,
        categoryItemValue,
        updateFormDataCategoryItems
      )
    )
  },
  toggleUpdateModal: showUpdateForm => {
    dispatch({
      type: 'TOGGLE_EVENT_UPDATE',
      payload: {
        showUpdateForm: showUpdateForm
      }
    })
  },
  toggleDeleteDialog: showDeleteDialog => {
    dispatch({
      type: 'TOGGLE_EVENT_DELETE',
      payload: {
        showDeleteDialog: showDeleteDialog
      }
    })
  },
  submitUpdateEvent: (event, countryId, pageNum, files) => {
    dispatch(updateEvent(event, countryId, pageNum, files))
  },
  submitDeleteEvent: (event, countryId, pageNum) => {
    dispatch(deleteEvent(event, countryId, pageNum))
  },
  dispatchGetMapGeocodingUpdate: (countryName, region, location, selectedOldlng, selectedOldLat) => {
    dispatch(getMapGeocodingUpdate(countryName, region, location, selectedOldlng, selectedOldLat))
  },
  dispatchGetFile: (fileID) => {
    dispatch(getFile(fileID))
  },
  dispatchDeleteFile: (fileID) => {
    dispatch(deleteFile(fileID))
  },
})

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    backgroundColor: theme.palette.background.paper,
    maxHeight: '130vh',
    overflow: 'auto',
  },
  gridList: {
    width: '100%',
    padding: theme.spacing(1)
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: '1px solid #ddd',
    '& label': {
      fontWeight: 'bold'
    }
  },
  row: {
    padding: theme.spacing(1),
    display: 'flex',
    alignItems: 'center'
  },
  fieldName: {
    flex: 1,
    minHeight: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
    '& label': {
      fontWeight: 'bold',
      fontSize: '14px'
    }
  },
  field: {
    flex: 3,
    paddingLeft: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px',
    width: '66%'
  },
  fieldFile: {
    flex: 3,
    paddingLeft: theme.spacing(2),
    fontSize: '14px',
    width: '66%'
  },
}))

const getArrayMatch = (array1, array2) => {
  return (
    array1 &&
    array1
      .filter(e1 => array2.some(e2 => e1.id === e2))
      .map(e => e.name)
      .join(' ,')
  )
}

const GridRow = ({ fieldName, children }) => {
  const classes = useStyles()
  return (
    <div className={classes.row}>
      <div className={classes.fieldName}>
        <InputLabel>
          {fieldName}
        </InputLabel>
      </div>
      <div className={classes.field}>
        {children}
      </div>
    </div>
  )
}

const GridRowFile = ({ fieldName, children }) => {
  const classes = useStyles()
  return (
    <div className={classes.row}>
      <div className={classes.fieldName}>
        <InputLabel>
          {fieldName}
        </InputLabel>
      </div>
      <div className={classes.fieldFile}>
        {children}
      </div>
    </div>
  )
}

const parseEvent = (event, countryId) => {
  return {
    country_id: parseInt(countryId),
    id: event.id,
    date: event.date,
    report_heading: event.report_heading,
    location: event.location,
    region: event.region,
    lng: event.coordinate.longitude,
    lat: event.coordinate.latitude,
    kills_status: event.kills === 0 ? 0 : event.kills > 0 ? 1 : event.kills === -2 ? 2 : 3,
    kills: event.kills < 0 ? 0 : event.kills,
    other: event.other,
    //city_id: event.city ? event.city.id : event.city,
    weapon: event.weapons.map(e => e.id),
    attack_type: event.attack_types.map(e => e.id),
    target_type: event.targets_types.map(e => e.id),
    target: event.targets.map(e => e.id),
    evidence: event.evidences.map(e => e.id),
    files: event.files
  }
}

const ViewEvent = ({
  event,
  submitting,
  dispatchCreateCategoryItem,
  countryId,
  weapon,
  attack_type,
  target_type,
  target,
  evidence,
  //city: cities,
  submitUpdateEvent,
  showUpdateForm,
  showDeleteDialog,
  toggleUpdateModal,
  toggleDeleteDialog,
  submitDeleteEvent,
  isSelected,
  pageNum,
  dispatchGetMapGeocodingUpdate,
  mapGeocodingUpdate,
  selectedOldRegion,
  dispatchGetFile,
  fileUrl,
  dispatchDeleteFile
}) => {

  const classes = useStyles()
  const oldEventFiles = event.files ? event.files : []
  const [formData, setFormData] = useState(parseEvent(event, countryId))
  const [openUrlFile, setOpenUrlFile] = useState(false)
  const [addfiles, setAddFiles] = useState('')
  const [errorsFile, setErrorsFile] = useState('')
  const [openAddFile, setOpenAddFile] = useState(false)
  const [translateText, setTranslateText] = useState('')
  let colorButtonAttachment = showUpdateForm ? '#27ae60' : ''
  const updateFormDataCategoryItems = ({ categoryName, newItem }) => {
    const newFormData = formData
    newFormData[categoryName].push(newItem.id)
    setFormData({ ...newFormData })
  }

  const openFile = (urlFile) => {
    if (openUrlFile) {
      window.open(urlFile)
    }
    setOpenUrlFile(false)
  }

  const CheckRegion = () => {
    dispatchGetMapGeocodingUpdate(event.country.name, formData.region, formData.location)
    mapGeocodingUpdate && setmapGeocodingCheck(mapGeocodingUpdate)
  }

  useEffect(() => {
    if (fileUrl && openUrlFile) { openFile(fileUrl) }
  },
    [(fileUrl && openUrlFile) && openFile(fileUrl)]
  )

  const menuProps = {
    MenuProps: {
      style: { maxHeight: '40%' },
      getContentAnchorEl: null,
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'left'
      }
    }
  }
  const inputProps = {
    fullWidth: true,
    disabled: submitting
  }
  let selectedoldRegion = selectedOldRegion ? selectedOldRegion.description : ''
  const [specificRegion, setSpecificRegion] = useState('')
  const [mapGeocodingCheck, setmapGeocodingCheck] = useState([])
  const [openMap, setOpenMap] = useState(false);
  const viewMap = () => {
    setOpenMap(true)
  }
  return (
    <div>
      {openMap &&
        < LocationsAvailable
          openMap={openMap}
          handleClose={() => { setOpenMap(false) }}
          mapGeocoding={mapGeocodingCheck}
        />
      }
      <Fragment>
        <DialogConfirmation
          open={showDeleteDialog}
          handleClose={() => toggleDeleteDialog(false)}
          handleConfirm={() => submitDeleteEvent(formData.id, countryId, pageNum)}
          title='Delete Confirmation'
          message='Are you sure you want to delete this incident ?'
        />
        <ValidatorForm
          className={classes.root}
          onSubmit={() => {
            submitUpdateEvent({
              ...formData,
              date: moment(formData.date, 'DD/MM/YYYY')
                .local()
                .format('DD/MM/YYYY')
            }, countryId, pageNum, addfiles)
            setTimeout(() => { setAddFiles('') }, 500)
            setOpenAddFile(false)
          }}
        >
          <div className={classes.gridList}>
            <div className={classes.header}>
              <InputLabel>
                {isSelected ? 'Selected Event' : 'Last Event'}
              </InputLabel>
              {showUpdateForm
                ? <div>
                  <Tooltip title='Translate' placement='top'>
                    <IconButton
                      onClick={() => {
                        googleTranslate.translate(formData.report_heading, 'ar', 'en', function (err, translations) {
                          translations && setTranslateText(translations.translatedText)
                        })
                      }}
                      style={{ marginLeft: 9 }}
                    >
                      <Translate />
                    </IconButton>
                  </Tooltip>
                  <span>
                    <Tooltip title='Save changes' placement='top'>
                      <IconButton type='submit' onClick={() => { }}>
                        <SaveIcon />
                      </IconButton>
                    </Tooltip>
                  </span>
                  <Tooltip title='Cancel editing' placement='top'>
                    <IconButton
                      onClick={() => {
                        setFormData(parseEvent(event, countryId))
                        toggleUpdateModal(!showUpdateForm)
                        setAddFiles('')
                        setOpenAddFile(false)
                      }}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Tooltip>
                </div>
                : <div>
                  <Tooltip
                    title='Translate'
                    placement='top'
                    PopperProps={{
                      popperOptions: {
                        modifiers: {
                          offset: {
                            enabled: true,
                            offset: '0px, 0px'
                          }
                        }
                      }
                    }}
                  >
                    <IconButton
                      onClick={() => {
                        googleTranslate.translate(formData.report_heading, 'ar', 'en', function (err, translations) {
                          setTranslateText(translations.translatedText)
                        })
                      }}
                      style={{ marginLeft: 9 }}
                    >
                      <Translate />
                    </IconButton>
                  </Tooltip>
                  <Tooltip
                    title='Edit incident'
                    placement='top'
                  >
                    <IconButton
                      onClick={() => {
                        toggleUpdateModal(!showUpdateForm)
                      }}
                    >
                      <EditIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title='Delete incident' placement='top'>
                    <IconButton onClick={() => toggleDeleteDialog(true)}>
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                </div>}
            </div>
            <GridRow fieldName='Date'>
              {showUpdateForm
                ? <MuiPickersUtilsProvider utils={MomentUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant='inline'
                    format='DD/MM/YYYY'
                    fullWidth
                    value={moment(formData.date, 'DD/MM/YYYY')}
                    onChange={date =>
                      setFormData({
                        ...formData,
                        date
                      })}
                    KeyboardButtonProps={{
                      'aria-label': 'change date'
                    }}
                    required
                    maxDate={moment()}
                  />
                </MuiPickersUtilsProvider>
                : <InputLabel>
                  {moment(formData.date, 'DD/MM/YYYY').format('DD/MM/YYYY')}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Report heading'>
              {showUpdateForm
                ? <TextValidator
                  name='report_heading'
                  value={formData.report_heading}
                  onChange={event =>
                    setFormData({
                      ...formData,
                      report_heading: event.target.value
                    })}
                  fullWidth
                  multiline
                  rows='2'
                  disabled={submitting}
                  validators={['required']}
                  errorMessages={['Report Heading is required']}
                />
                :
                <InputLabel>
                  {formData.report_heading}
                </InputLabel>
              }
            </GridRow>

            {translateText && <GridRow fieldName='Translate'>
              <InputLabel>{translateText}</InputLabel>
            </GridRow>}

            <GridRow fieldName='Location'>
              {showUpdateForm
                ? <TextValidator
                  name='location'
                  value={formData.location}
                  onChange={event =>
                    setFormData({ ...formData, location: event.target.value })}
                  fullWidth
                  disabled={submitting}
                  validators={['required']}
                  errorMessages={['Location address is required']}
                />
                : <InputLabel>
                  {formData.location}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Region'>
              {showUpdateForm
                ? <TextValidator
                  name='region'
                  value={formData.region}
                  onChange={event =>
                    setFormData({ ...formData, region: event.target.value })}
                  fullWidth
                  disabled={submitting}
                  validators={['required']}
                  errorMessages={['Region is required']}
                />
                : <InputLabel>
                  {formData.region}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Check the location'>
              <Button
                variant="contained"
                color="primary"
                onClick={() => CheckRegion()}
                disabled={showUpdateForm ? false : true}
                style={{ textTransform: 'none' }}
              >
                Check the location
            </Button>
            </GridRow>

            <GridRow fieldName='Check the location'>
              {showUpdateForm
                ? <SelectValidator
                  SelectProps={{
                    ...menuProps
                  }}
                  value={specificRegion === '' ? selectedoldRegion : specificRegion}
                  onChange={event => {
                    setSpecificRegion(event.target.value)
                    setFormData({ ...formData, lng: event.target.value.lng, lat: event.target.value.lat })
                  }}
                  {...inputProps}
                  disabled={mapGeocodingCheck.length === 0 ? true : false}
                >
                  {
                    mapGeocodingCheck.length === 0 ? <MenuItem value={selectedoldRegion}>{selectedoldRegion}</MenuItem> :
                      mapGeocodingUpdate.map((item, index) =>
                        <MenuItem key={index} value={item} >
                          {item.description}
                        </MenuItem>
                      )}
                </SelectValidator>
                : <InputLabel>
                  {mapGeocodingCheck.length === 0 ? selectedoldRegion : specificRegion ? specificRegion.description : ''}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='View map'>
              <Button
                variant="contained"
                color="secondary"
                onClick={viewMap}
                disabled={mapGeocodingCheck.length === 0 || !showUpdateForm ? true : false}
                style={{ textTransform: 'none' }}
              >
                View map
              </Button>
            </GridRow>

            {/* <GridRow fieldName='City'>
            {showUpdateForm
              ? <AutoComplete
                options={cities}
                onChange={options =>
                    setFormData({
                      ...formData,
                      city_id: options ? options.value : null
                    })}
                value={cities.find(e => formData.city_id === e.id)}
                variant='standard'
                />
              : <InputLabel>
                {getArrayMatch(cities, [formData.city_id])}
              </InputLabel>}
          </GridRow> */}

            <GridRow fieldName='Weapons'>
              {showUpdateForm
                ? <AutoComplete
                  options={weapon}
                  onChange={options =>
                    setFormData({
                      ...formData,
                      weapon: options ? options.map(opt => opt.value) : []
                    })}
                  value={weapon.filter(e => formData.weapon.includes(e.id))}
                  isMulti
                  variant='standard'
                  onCreateOption={value =>
                    dispatchCreateCategoryItem(
                      'weapon',
                      value,
                      updateFormDataCategoryItems
                    )}
                  sizeChip={'medium'}
                  sizeInput={''}
                />
                : <InputLabel>
                  {getArrayMatch(weapon, formData.weapon)}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Attack types'>
              {showUpdateForm
                ? <AutoComplete
                  options={attack_type}
                  onChange={options =>
                    setFormData({
                      ...formData,
                      attack_type: options ? options.map(opt => opt.value) : []
                    })}
                  value={attack_type.filter(e =>
                    formData.attack_type.includes(e.id)
                  )}
                  isMulti
                  variant='standard'
                  onCreateOption={value =>
                    dispatchCreateCategoryItem(
                      'attack_type',
                      value,
                      updateFormDataCategoryItems
                    )}
                  sizeChip={'medium'}
                  sizeInput={''}
                />
                : <InputLabel>
                  {getArrayMatch(attack_type, formData.attack_type)}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Targets'>
              {showUpdateForm
                ? <AutoComplete
                  options={target}
                  onChange={options =>
                    setFormData({
                      ...formData,
                      target: options ? options.map(opt => opt.value) : []
                    })}
                  value={target.filter(e => formData.target.includes(e.id))}
                  isMulti
                  variant='standard'
                  onCreateOption={value =>
                    dispatchCreateCategoryItem(
                      'target',
                      value,
                      updateFormDataCategoryItems
                    )}
                  sizeChip={'medium'}
                  sizeInput={''}
                />
                : <InputLabel>
                  {getArrayMatch(target, formData.target)}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Targets types'>
              {showUpdateForm
                ? <AutoComplete
                  options={target_type}
                  onChange={options =>
                    setFormData({
                      ...formData,
                      target_type: options ? options.map(opt => opt.value) : []
                    })}
                  value={target_type.filter(e =>
                    formData.target_type.includes(e.id)
                  )}
                  isMulti
                  variant='standard'
                  onCreateOption={value =>
                    dispatchCreateCategoryItem(
                      'target_type',
                      value,
                      updateFormDataCategoryItems
                    )}
                  sizeChip={'medium'}
                  sizeInput={''}
                />
                : <InputLabel>
                  {getArrayMatch(target_type, formData.target_type)}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Kills'>
              {showUpdateForm
                ? <SelectValidator
                  SelectProps={{
                    ...menuProps
                  }}
                  value={formData.kills_status}
                  onChange={event =>
                    setFormData({
                      ...formData,
                      kills_status: event.target.value,
                      kills: 0
                    })}
                  validators={['required']}
                  errorMessages={['Kills is required']}
                  {...inputProps}
                >
                  {kills_status.map(item =>
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  )}
                </SelectValidator>
                : <InputLabel>
                  {getArrayMatch(kills_status, [formData.kills_status])}
                </InputLabel>}
            </GridRow>

            {(formData.kills_status === 0 || formData.kills_status === 1) &&
              <GridRow fieldName='Kill numbers'>
                {showUpdateForm
                  ? <TextValidator
                    name='kills'
                    value={formData.kills}
                    onChange={event =>
                      setFormData({ ...formData, kills: parseInt(event.target.value) })}
                    fullWidth
                    type='number'
                    InputProps={{ inputProps: { min: formData.kills_status } }}
                    disabled={submitting || formData.kills_status !== 1}
                    validators={['required']}
                    errorMessages={['Kills is required']}
                  />
                  : <InputLabel>
                    {formData.kills}
                  </InputLabel>}
              </GridRow>}

            <GridRow fieldName='Other'>
              {showUpdateForm
                ? <TextValidator
                  name='other'
                  value={formData.other}
                  onChange={event =>
                    setFormData({
                      ...formData,
                      other: event.target.value
                    })}
                  fullWidth
                  multiline
                  rows='2'
                  disabled={submitting}
                />
                : <InputLabel>
                  {formData.other}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Evidences'>
              {showUpdateForm
                ? <AutoComplete
                  options={evidence}
                  onChange={options =>
                    setFormData({
                      ...formData,
                      evidence: options ? options.map(opt => opt.value) : []
                    })}
                  value={evidence.filter(e => formData.evidence.includes(e.id))}
                  isMulti
                  variant='standard'
                  onCreateOption={value =>
                    dispatchCreateCategoryItem(
                      'evidence',
                      value,
                      updateFormDataCategoryItems
                    )}
                  sizeChip={'medium'}
                  sizeInput={''}
                />
                : <InputLabel>
                  {getArrayMatch(evidence, formData.evidence)}
                </InputLabel>}
            </GridRow>

            <GridRow fieldName='Created by'>
              <InputLabel>
                {event.user.username}
              </InputLabel>
            </GridRow>

            <GridRowFile fieldName='Attachments'>
              {oldEventFiles && oldEventFiles.map((file, index) => {
                return <div key={index}>
                  <InputLabel>{file.file_name}</InputLabel><br />
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      setTimeout(() => { setOpenUrlFile(true) }, 1000)
                      dispatchGetFile(file.id)
                    }}
                    disabled={!showUpdateForm ? true : false}
                    style={{
                      marginRight: 15,
                      textTransform: 'capitalize'
                    }}
                  >
                    Show
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      dispatchDeleteFile(file.id)
                      for (let i = 0; i < oldEventFiles.length; i++) {
                        if (oldEventFiles[i].id === file.id) {
                          oldEventFiles.splice(i, 1);
                          i--;
                        }
                      }
                      setFormData({
                        ...formData,
                        files: oldEventFiles ? oldEventFiles : []
                      })
                    }}
                    disabled={!showUpdateForm ? true : false}
                    style={{ textTransform: 'capitalize' }}
                  >
                    Delete
                  </Button>
                  <hr />
                </div>
              })
              }
              <Button
                variant="contained"
                onClick={() => {
                  setOpenAddFile(!openAddFile)
                }}
                style={{
                  color: '#fff',
                  backgroundColor: colorButtonAttachment,
                  textTransform: 'capitalize'
                }}
                disabled={!showUpdateForm ? true : false}
              >
                Attachment
              </Button>
              {openAddFile && <Files
                multiple={true}
                maxSize="100mb"
                multipleMaxSize="500mb"
                accept={[
                  "application/pdf, .doc, .docx, .xlsx, .csv",
                  'application/pdf',
                  "text/plain",
                  "image/jpg",
                  "image/jpeg",
                  "image/png",
                  "image/tiff",
                  "image/tif",
                  "image/gif",
                  "video/mp4",
                  ".17",
                  "",
                ]}
                // accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*,video/mp4,.gif"
                onSuccess={files => setAddFiles({ files })}
                onError={errors => setErrorsFile({ errors })}
                disabled={!showUpdateForm ? true : false}
              >
                {({ browseFiles, getDropZoneProps }) => {
                  return (
                    <div>
                      <label style={{ marginTop: 10 }}>Drag and drop files.</label>
                      <div
                        {...getDropZoneProps({
                          style: {
                            width: 150,
                            minHeight: 150,
                            border: "2px lightgray dashed",
                            marginTop: 10,
                            marginBottom: 10
                          }
                        })}
                      >
                        <ol>
                          {addfiles && addfiles.files.map((file, index) => (
                            <li key={file.id}>{file.name}</li>
                          ))}
                          {errorsFile && errorsFile.errors.map((error, index) => (
                            <li key={error.id}>
                              {error.file ? (
                                <span style={{ color: 'red' }}>
                                  {error.file.name} - {error.type}
                                </span>
                              ) : (
                                  error.type
                                )}
                            </li>
                          ))}
                        </ol>
                      </div>
                      <div>
                        Dragging not convenient? Click{" "}
                        <button type="button" onClick={browseFiles}>here</button> to select files.
                          </div>
                    </div>
                  );
                }}
              </Files>
              }
            </GridRowFile>
          </div>
        </ValidatorForm>
      </Fragment>
    </div >
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(ViewEvent)

import React, { useState, Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, IconButton, Container, FormControl, TextField, MenuItem } from '@material-ui/core';
import { connect } from 'react-redux';
import EventsTable from './components/EventsTable';
import { DateRange, FilterList } from '@material-ui/icons';
import { getMapCoordinates } from '../../api/helper'



const useStyles = makeStyles(theme => ({
	tabsTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		'& h6': {
			margin: `${theme.spacing(1)}px ${theme.spacing(5)}px`,
		},
	},
	filterButton: {
		marginLeft: theme.spacing(1),
	},
	selectEmpty: {
		marginTop: 6,
		marginLeft: 14,
	},
}));

const mapStateToProps = ({
	categories: { countriesList, allItems },
	map: { fetching: fetchingMap },
	statistics: { fetching: fetchingStatistics },
}) => {
	return {
		countriesList,
		allItems,
		fetchingMap,
		fetchingStatistics,
	};
};

const mapDispatchToProps = dispatch => ({
	dispatchGetMapCoordinates: countryId => {
		dispatch(getMapCoordinates(countryId));
	},
});

function a11yProps(index) {
	return {
		id: `scrollable-force-tab-${index}`,
		'aria-controls': `scrollable-force-tabpanel-${index}`,
	};
}
function TabPanel(props) {
	const { children, value, index } = props;
	return value === index && <Fragment>{children}</Fragment>;
}

const Events = ({
	countriesList,
	allItems,
	fetchingMap,
	fetchingStatistics,
	handleSelectCountry,
	worldMap,
	dispatchGetMapCoordinates,
	...restProps
}) => {
	useEffect(
		() => {
			dispatchGetMapCoordinates(countryId)
		},[]);
	const classes = useStyles();
	const countryId = restProps.match.params.country;
	const findcountryName = (countriesList, countryId) => {
		return countriesList.find(country => {
			return country.id === parseInt(countryId);
		});
	};
	let countryName = findcountryName(countriesList, countryId);
	countryName = countryName ? countryName.name : '';

	const [showfilterDate, setShowfilterDate] = useState(false)
	const [showfilterItems, setShowfilterItems] = useState(false)
	const [nameClassification, setNameClassification] = useState('incidents')
	const getCountryName = () => {
		return countriesList.length && countriesList.findIndex(country => country.id.toString() === countryId) >= 0
			? countriesList.find(country => country.id.toString() === countryId).name
			: undefined;
	};


	if (!countryId) {
		return (
			<Container className={classes.countryContainer}>
				<FormControl className={classes.formControl}>
					<TextField
						select
						value={''}
						variant="outlined"
						label="Select country"
						onChange={handleSelectCountry}
						InputLabelProps={{ shrink: false }}
						SelectProps={{
							MenuProps: {
								getContentAnchorEl: null,
								anchorOrigin: {
									vertical: 'bottom',
									horizontal: 'left',
								},
							},
						}}
					>
						{countriesList &&
							countriesList.map(c => {
								return (
									<MenuItem value={c.id} key={c.name}>
										{c.name}
									</MenuItem>
								);
							})}
					</TextField>
				</FormControl>
				<img src={worldMap} alt="logo" />
			</Container>
		);
	} else {
		return (
			<Fragment>
				<Box className={classes.tabsTitle}>
					<Typography variant="h6" m={2} gutterBottom>
						{[getCountryName(), nameClassification].filter(Boolean).join(' ')}
						<span>
							<IconButton aria-label="FilterList" className={classes.filterButton} onClick={() => setShowfilterItems(!showfilterItems)}>
								<FilterList fontSize="inherit" />
							</IconButton>
							<IconButton aria-label="DateRange" className={classes.filterButton} onClick={() => setShowfilterDate(!showfilterDate)}>
								<DateRange fontSize="inherit" />
							</IconButton>
						</span>
					</Typography>
				</Box>
				<EventsTable
					countryId={countryId}
					countryName={countryName}
					showfilterDate={showfilterDate}
					setShowfilterDate={setShowfilterDate}
					showfilterItems={showfilterItems}
					setShowfilterItems={setShowfilterItems}
					lengthcountryName={parseInt(countryName.length)}
				/>
			</Fragment>

		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Events)

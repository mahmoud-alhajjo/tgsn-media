import React, { Fragment, useState } from 'react';
import { Box, Button, ButtonGroup, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import RateChart from './RateChart';

const dateVariantList = [
	{ name: 'day', value: 'd' },
	{ name: 'week', value: 'w' },
	{ name: 'month', value: 'm' },
	{ name: 'year', value: 'y' },
];

const useStyles = makeStyles(theme => ({
	chartHeader: {
		display: 'flex',
		margin: theme.spacing(2),
		alignItems: 'center',
		justifyContent: 'space-between',
	},
}));

const RateChartContainer = ({ data, dataType }) => {
	const [dateVariant, setDateVariant] = useState(dateVariantList[0]);
	const classes = useStyles();

	return (
		<Fragment>
			<Box className={classes.chartHeader}>
				<Typography variant="h6">
					Number of <b>{dataType}</b> per <b>{dateVariant.name}</b>
				</Typography>
				<ButtonGroup size="small">
					{dateVariantList.map(date => {
						return (
							<Button
								key={date.value}
								onClick={() => {
									setDateVariant(date);
								}}
								variant={dateVariant.value === date.value ? 'contained' : ''}
								color={dateVariant.value === date.value ? 'secondary' : ''}
							>
								{date.name}
							</Button>
						);
					})}
				</ButtonGroup>
			</Box>
			<RateChart
				data={data[`${dataType}_rate_${dateVariant.value}`]}
				chartId={`${dataType}_per_${dateVariant.name}`}
				tooltipKey={dataType}
			></RateChart>
		</Fragment>
	);
};

export default RateChartContainer;

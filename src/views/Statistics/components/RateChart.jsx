import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import moment from 'moment';

am4core.useTheme(am4themes_animated);
am4core.addLicense("CH204750632")

const RateChart = ({ data, chartId, tooltipKey }) => {
	useEffect(() => {
		let chart = am4core.create(chartId, am4charts.XYChart);
		if (data) {
            chart.paddingRight = 40;
			let chartData = data.map((stat, index) => {
				return {
					date: moment(stat.name, 'DD/MM/YYYY').toDate(),
					value: stat.value,
					name: stat.name,
				};
			});

            chart.data = chartData;
            chart.xAxes.push(new am4charts.DateAxis());
            chart.yAxes.push(new am4charts.ValueAxis());

			var columnSeries = chart.series.push(new am4charts.ColumnSeries());
			columnSeries.dataFields.dateX = 'date';
			columnSeries.dataFields.valueY = 'value';

			let lineSeries = chart.series.push(new am4charts.LineSeries());
			lineSeries.dataFields.dateX = 'date';
			lineSeries.dataFields.valueY = 'value';
            lineSeries.stroke = am4core.color("#3F50B5");
            lineSeries.strokeWidth = 2;

			lineSeries.tooltipText = `{valueY.value} ${tooltipKey}`;
			chart.cursor = new am4charts.XYCursor();
		}
		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	});

	return <div id={chartId} style={{ width: '100%', height: '500px' }}></div>;
};

export default RateChart;

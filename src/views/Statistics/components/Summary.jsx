import React, { Fragment } from 'react';
import moment from 'moment';
import { Card, Grid, Typography, Box, LinearProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
	card: {
		padding: 20,
		'& div': {
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
		},
	},
}));

const Summary = ({ data }) => {
	const classes = useStyles();
	return (
		<Fragment>
			<Card style={{ padding: 20, marginBottom: 20 }}>
				<Grid container spacing={5} alignItems="center">
					<Grid item xs={3}>
						<Typography variant="subtitle1" align="center">
							<b>Incidents</b>
						</Typography>
						<Typography variant="h3" style={{ color: '#aaa', marginBottom: 16 }} align="center">
							<b>{data.number_of_events}</b>
						</Typography>
					</Grid>
					<Grid item xs={3}>
						<Typography variant="subtitle1" align="center">
							<b>Kills </b>
						</Typography>
						<Typography variant="h3" style={{ color: '#aaa' }} align="center">
							<b>{data.number_of_kills}</b>
						</Typography>
					</Grid>
					<Grid item xs={6} style={{ display: 'flex', justifyContent: 'space-around' }}>
						<div>
							<Typography variant="subtitle1" align="center">
								<b>From</b>
							</Typography>
							<Typography
								variant="h4"
								color="textSecondary"
								align="center"
								style={{
									whiteSpace: 'nowrap',
									color: '#aaa',
								}}
							>
								<b>{moment(data.from_date).format('DD MMM YY')}</b>
							</Typography>
						</div>
						<div>
							<Typography variant="subtitle1" align="center">
								<b>To</b>
							</Typography>
							<Typography
								variant="h4"
								color="textSecondary"
								align="center"
								style={{
									whiteSpace: 'nowrap',
									color: '#aaa',
								}}
							>
								<b>{moment(data.to_date).format('DD MMM YY')}</b>
							</Typography>
						</div>
					</Grid>
				</Grid>
			</Card>
			<Grid container spacing={2} alignItems="center">
				{data.top_cities && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted city</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_cities.city_name}</b>
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Incidents <br />
									{data.top_cities.count}
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Kills <br />
									{data.top_cities.kills}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_regions && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted region</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_regions.region_name}</b>
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Incidents <br />
									{data.top_regions.count}
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Kills <br />
									{data.top_regions.kills}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_subregions && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted subregion</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_subregions.subregion_name}</b>
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Incidents <br />
									{data.top_subregions.count}
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Kills <br />
									{data.top_subregions.kills}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_region_free_text && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted subdistrict</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_region_free_text.region}</b>
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Incidents <br />
									{data.top_region_free_text.count}
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Kills <br />
									{data.top_region_free_text.kills}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_location_free_text && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently targeted town/village</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_location_free_text.location}</b>
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Incidents <br />
									{data.top_location_free_text.count}
								</Typography>
								<Typography variant="h5" align="center" style={{ color: '#aaa' }}>
									Kills <br />
									{data.top_location_free_text.kills}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_weapons && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequently used weapon</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_weapons.name}</b>
								</Typography>
								<Typography variant="h4" align="center" style={{ color: '#aaa' }}>
									{data.top_weapons.count}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_attack_types && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most commonly occurring attack type</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_attack_types.name}</b>
								</Typography>
								<Typography variant="h4" align="center" style={{ color: '#aaa' }}>
									{data.top_attack_types.count}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_targets && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequent target</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_targets.name}</b>
								</Typography>
								<Typography variant="h4" align="center" style={{ color: '#aaa' }}>
									{data.top_targets.count}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{data.top_targets_types && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>Most frequent target type</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_targets_types.name}</b>
								</Typography>
								<Typography variant="h4" align="center" style={{ color: '#aaa' }}>
									{data.top_targets_types.count}
								</Typography>
							</div>
						</Card>
					</Grid>
				)}
				{/* {data.top_evidences && (
					<Grid item xs={4}>
						<Card className={classes.card}>
							<Typography variant="subtitle1">
								<span>The most common evidence</span>
							</Typography>
							<div>
								<Typography variant="h6">
									<b>{data.top_evidences.name}</b>
								</Typography>
								<Typography variant="h4" align="center" style={{ color: '#aaa' }}>
									{data.top_evidences.count}
								</Typography>
							</div>
						</Card>
					</Grid>
				)} */}
			</Grid>
		</Fragment>
	);
};

export default Summary;

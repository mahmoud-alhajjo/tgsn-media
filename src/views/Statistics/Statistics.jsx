import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUnivariableStatistics } from '../../api/helper';
import { Grid, Card, Container } from '@material-ui/core';
import MultiBarGraph from '../../graphs/MultiBarGraph';
import Summary from './components/Summary';
import WordsCloud from '../../graphs/WordsCloud';
import PeriodicMultiLineGraph from '../../graphs/PeriodicMultiLineGraph';
import Search from '../Map/components/Search'

const mapStateToProps = ({ statistics: { univariableStatistics, univariableSummary } }) => {
	return {
		univariableStatistics,
		univariableSummary,
	};
};

const mapDispatchToProps = dispatch => ({
	dispatchGetUnivariableStatistics: countryId => {
		dispatch(getUnivariableStatistics(countryId));
	},
	submitStatisticsSearchEvents: (countryId, eventSearch) => {
		dispatch({
			type: 'UNIVARIABLE_STATISTICS_FETCHING',
		})
		dispatch(getUnivariableStatistics(countryId, eventSearch));
	},
});
const Statistics = ({
	countryId,
	univariableStatistics: data,
	univariableSummary,
	dispatchGetUnivariableStatistics,
	showfilterDate,
	setShowfilterDate,
	showfilterItems,
	setShowfilterItems,
	lengthcountryName,
	submitStatisticsSearchEvents
}) => {
	useEffect(() => {
		dispatchGetUnivariableStatistics(countryId);
	}, []);
	
	return data ? (
		<Container maxWidth="xl" style={{ padding: '20px 30px' }}>
			<Grid container spacing={3}>
				<Grid item xs={3}>
					<Search
						submitStatisticsSearchEvents={(countryId, eventSearch) => submitStatisticsSearchEvents(countryId, eventSearch)}
						countryId={countryId}
						showfilterDate={showfilterDate}
						setShowfilterDate={setShowfilterDate}
						showfilterItems={showfilterItems}
						setShowfilterItems={setShowfilterItems}
						lengthcountryName={lengthcountryName}
						styleForPAgeStatistics={true}
					/>
				</Grid>

				<Grid item xs={showfilterItems ? 9 : 12}>
					<Summary data={univariableSummary} />
				</Grid>
				{/* {data.num_unique_vals_per_category.length && (
					<Grid item xs={6}>
						<Card>
							<MultiBarGraph
								data={data.num_unique_vals_per_category}
								chartId="unique_values_bar_graph"
								chartTitle="Number of unique values"
								category="category"
								bars={[{ field: 'num_unique_vals', name: 'Unique values' }]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)} */}

				{data.top_weapons.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_weapons}
								chartId="top_weapons_words_cloud"
								chartTitle="Most frequently used weapon"
								word="name"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				{data.top_attack_types.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_attack_types}
								chartId="top_attack_types_words_cloud"
								chartTitle="Most commonly occurring attack type"
								word="name"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				{data.top_targets_types.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_targets_types}
								chartId="top_targets_types_words_cloud"
								chartTitle="Most frequent target type"
								word="name"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				{data.top_targets.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_targets}
								chartId="top_targets_words_cloud"
								chartTitle="Most frequent target"
								word="name"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				{/* {data.top_evidences.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_evidences}
								chartId="top_evidences_words_cloud"
								chartTitle="Top 10 evidences"
								word="name"
							></WordsCloud>
						</Card>
					</Grid>
				)} */}
				{/* {data.top_cities.length && (
					<Grid item xs={12}>
						<Card>
							<MultiBarGraph
								data={data.top_cities}
								chartId="top_cities_double_bar_graph"
								chartTitle="Top Cities"
								category="city_name"
								bars={[
									{ field: 'count', name: 'Incidents' },
									{ field: 'kills', name: 'Kills' },
								]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)} */}
				{data.top_regions.length && (
					<Grid item xs={12}>
						<Card>
							<MultiBarGraph
								data={data.top_regions}
								chartId="top_regions_double_bar_graph"
								chartTitle="Incidents by region"
								category="region_name"
								bars={[
									{ field: 'count', name: 'Incidents' },
									{ field: 'kills', name: 'Kills' },
								]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)}
				{data.top_subregions.length && (
					<Grid item xs={12}>
						<Card>
							<MultiBarGraph
								data={data.top_subregions}
								chartId="top_subregions_double_bar_graph"
								chartTitle="Incidents by subregion "
								category="subregion_name"
								bars={[
									{ field: 'count', name: 'Incidents' },
									{ field: 'kills', name: 'Kills' },
								]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)}
				{data.top_region_free_text.length && (
					<Grid item xs={12}>
						<Card>
							<MultiBarGraph
								data={data.top_region_free_text}
								chartId="top_region_free_text_double_bar_graph"
								chartTitle="Incidents by subdistrict"
								category="region"
								bars={[
									{ field: 'count', name: 'Incidents' },
									{ field: 'kills', name: 'Kills' },
								]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)}
				{data.top_location_free_text.length && (
					<Grid item xs={12}>
						<Card>
							<MultiBarGraph
								data={data.top_location_free_text}
								chartId="top_location_free_text_double_bar_graph"
								chartTitle="Incidents by town/village"
								category="location"
								bars={[
									{ field: 'count', name: 'Incidents' },
									{ field: 'kills', name: 'Kills' },
								]}
							></MultiBarGraph>
						</Card>
					</Grid>
				)}

				{data.top_bigrams.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_bigrams}
								chartId="top_bigrams_words_cloud"
								chartTitle="Top 10 bigrams"
								word="ngram"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				{data.top_trigrams.length && (
					<Grid item xs={6}>
						<Card>
							<WordsCloud
								data={data.top_trigrams}
								chartId="top_trigrams_words_cloud"
								chartTitle="Top 10 trigrams"
								word="ngram"
							></WordsCloud>
						</Card>
					</Grid>
				)}
				<Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="num_events_kills"
							chartId="num_events_kills_multi_line_graph"
							chartTitle="Incidents and kills"
							lines={[
								{ field: 'kills', name: 'Kills' },
								{ field: 'count', name: 'Incidents' },
							]}
						></PeriodicMultiLineGraph>
					</Card>
				</Grid>
				<Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="top_weapons"
							chartId="top_weapons_multi_line_graph"
							chartTitle="Weapon usage"
						></PeriodicMultiLineGraph>
					</Card>
				</Grid>

				<Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="top_attack_types"
							chartId="top_attack_types_multi_line_graph"
							chartTitle="Attack type"
						></PeriodicMultiLineGraph>
					</Card>
				</Grid>
				<Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="top_targets_types"
							chartId="top_targets_types_multi_line_graph"
							chartTitle="Target types"
						></PeriodicMultiLineGraph>
					</Card>
				</Grid>
				<Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="top_targets"
							chartId="top_targets_multi_line_graph"
							chartTitle="Targets"
						></PeriodicMultiLineGraph>
					</Card>
				</Grid>
				{/* <Grid item xs={12}>
					<Card>
						<PeriodicMultiLineGraph
							data={data}
							dataKey="top_evidences"
							chartId="top_evidences_multi_line_graph"
							chartTitle="Top 10 evidences"
						></PeriodicMultiLineGraph>
					</Card>
				</Grid> */}
			</Grid>
		</Container>
	) : null;
};

export default connect(mapStateToProps, mapDispatchToProps)(Statistics);

import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import {
  Avatar,
  Button,
  Typography,
  Container,
  CircularProgress,
  Box
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core/styles'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import { login } from '../api/helper'
import cookie from 'react-cookie'

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(10),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: theme.palette.common.white,
    padding: theme.spacing(5)
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

const mapStateToProps = ({
  userProfile: { userProfile, requesting, error }
}) => {
  return {
    userProfile,
    requesting,
    error
  }
}

const mapDispatchToProps = dispatch => ({
  postLogin: (email, password) => {
    dispatch(
      login({
        email,
        password
      })
    )
  }
})

const Login = ({ postLogin, requesting, error }) => {
  const classes = useStyles()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const isAuthenticated = cookie.load('access_token')
  if (isAuthenticated) {
    return <Redirect to='/' />
  }

  return (
    <Container component='main' maxWidth='sm'>
      <div className={classes.container}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        <ValidatorForm
          className={classes.form}
          onSubmit={() => postLogin(email, password)}
        >
          <TextValidator
            label='Email Address'
            name='email'
            value={email}
            onChange={event => setEmail(event.target.value)}
            variant='outlined'
            margin='normal'
            fullWidth
            autoComplete='email'
            autoFocus
            required
            validators={['required', 'isEmail']}
            errorMessages={['Email address is required', 'Email is not valid']}
          />
          <TextValidator
            label='Password'
            type='password'
            name='password'
            value={password}
            onChange={event => setPassword(event.target.value)}
            variant='outlined'
            margin='normal'
            fullWidth
            autoComplete='current-password'
            required
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            disabled={requesting}
          >
            Sign In
          </Button>
        </ValidatorForm>
        {requesting && <CircularProgress className={classes.progress} />}
        <Box color='error.main'>
          {error}
        </Box>
      </div>
    </Container>
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)

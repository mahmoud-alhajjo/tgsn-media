import axios from "axios"
import { config } from '../config'
import cookie from 'react-cookie'
import { ApiServices } from './services'

var APIURL = config.url.API_URL
const instance = axios.create();

//Add a response interceptor

instance.interceptors.response.use(function (response) {

    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data

    const originalRequest = response.config;

    if (response.data.status_code === 200) {
        return response
    }

    if ((response.data.status_code === 434 ||
        response.data.status_code === 433 ||
        response.data.status_code === 432 ||
        response.data.status_code === 436)
        && originalRequest.url === `${APIURL}/authentication/refresh`) {
        console.log('originalRequest', originalRequest)
        cookie.remove('access_token', {
            path: '/',
        });
        cookie.remove('refresh_token')
        localStorage.removeItem('user_profile');
        return response;
    }

    if ((response.data.status_code === 434 ||
        response.data.status_code === 433 ||
        response.data.status_code === 432
        || response.data.status_code === 436)) {
        const refreshToken = cookie.load('refresh_token')
        return ApiServices().refresh(refreshToken).then(res => {
            let responseData = res.data
            if (responseData.status_code === 200) {
                cookie.save('refresh_token', responseData.data.refresh_token);
                cookie.save('access_token', responseData.data.access_token, {
                    path: '/',
                });
                originalRequest.headers['Authorization'] = responseData.data.access_token;
                return axios(originalRequest);
            }
        }).catch((erorr) => console.log('erorr', erorr))
    }

}, function (error) {

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error

    console.log('error : ', error)

    return Promise.reject(error);
});

export default instance
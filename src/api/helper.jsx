import { ApiServices } from './services';
import cookie from 'react-cookie';

export const login = credentials => {
	return dispatch => {
		dispatch({ type: 'USER_PROFILE_REQUEST' });
		ApiServices()
			.login(credentials)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					cookie.save('access_token', responseData.data.access_token, {
						path: '/',
					});
					cookie.save('refresh_token', responseData.data.refresh_token);
					localStorage.setItem('user_profile', JSON.stringify(responseData.data.user));
					dispatch({
						type: 'SET_USER_PROFILE',
						payload: {
							userProfile: responseData.data.user,
							isAdmin: responseData.data.user.is_admin,
							isLoggedIn: true,
						},
					});
				} else {
					dispatch({
						type: 'USER_PROFILE_ERROR',
						payload: {
							error: responseData.message,
						},
					});
				}
			})
			.catch(error => {
				dispatch({
					type: 'USER_PROFILE_ERROR',
					payload: {
						error: error.message,
					},
				});
			});
	};
};

export const logout = () => {
	return dispatch => {
		dispatch({ type: 'USER_PROFILE_REQUEST' });
		ApiServices()
			.logout()
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					cookie.remove('access_token', {
						path: '/',
					});
					localStorage.removeItem('user_profile');
					dispatch({
						type: 'SET_USER_PROFILE',
						payload: {
							userProfile: {},
							isLoggedIn: false,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log('error', error);
			});
	};
};

export const getAllUsers = pageNum => {
	return dispatch => {
		dispatch({ type: 'ALL_USERS_FETCHING' });
		ApiServices()
			.getAllUsers(pageNum)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_ALL_USERS',
						payload: {
							users: responseData.data.users,
							totalData: responseData.data.total_data,
						},
					});
				} else {
					dispatch({
						type: 'ALL_USERS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
				}
			})
			.catch(error => {
				dispatch({
					type: 'ALL_USERS_ERROR',
					payload: {
						error: error,
					},
				});
			});
	};
};

export const getUser = userId => {
	return dispatch => {
		dispatch({ type: 'USER_FETCHING' });
		ApiServices()
			.getUser(userId)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_USER',
						payload: {
							user: responseData.data,
						},
					});
				} else {
					console.log('responseData.message', responseData.message);
				}
			})
			.catch(error => {
				console.log('error', error);
			});
	};
};

export const createUser = user => {
	return dispatch => {
		dispatch({ type: 'USER_SUBMIT_FORM_ACTION' });
		ApiServices()
			.createUser(user)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllUsers(1));
					dispatch({
						type: 'TOGGLE_USER_ADD',
						payload: {
							showAddModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_USERS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_USERS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const updateUser = user => {
	return dispatch => {
		if (user) {
			let userInfo = { username: user.username, email: user.email }
			let activated = { activated: user.activated }
			dispatch({ type: 'USER_SUBMIT_FORM_ACTION' });
			ApiServices().updateUserActivate(user.id, activated).then(response => {
				console.log('status_code', response.data.status_code)
			}).catch(error => {
				console.log('error', error)
			})
			ApiServices()
				.updateUserByID(user.id, userInfo)
				.then(response => {
					let responseData = response.data;
					if (responseData.status_code === 200) {
						dispatch(getAllUsers(1));
						dispatch({
							type: 'TOGGLE_USER_UPDATE',
							payload: {
								showUpdateModal: false,
							},
						});
					} else {
						dispatch({
							type: 'SUBMIT_USERS_ERROR',
							payload: {
								error: responseData.message,
							},
						});
						alert(responseData.message);
					}
				})
				.catch(error => {
					dispatch({
						type: 'SUBMIT_USERS_ERROR',
						payload: {
							error: error,
						},
					});
					alert(error);
				});
		}
	};
};


export const updateUserByID = user => {
	return dispatch => {
		dispatch({ type: 'USER_SUBMIT_FORM_ACTION' });
		ApiServices()
			.updateUserByID(user.id, user)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getUser(user.id));
					dispatch({
						type: 'TOGGLE_USER_UPDATE',
						payload: {
							showUpdateModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_USERS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_USERS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const updatePassword = userPassword => {
	return dispatch => {
		dispatch({ type: 'USER_SUBMIT_FORM_ACTION' });
		ApiServices()
			.updatePassword(userPassword.id, userPassword)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 410) {
					alert('The old password invalid.')
				}
				if (responseData.status_code === 200) {
					dispatch({
						type: 'TOGGLE_USER_UPDATE',
						payload: {
							showUpdateModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_USERS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					console.log('responseData.message', responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_USERS_ERROR',
					payload: {
						error: error,
					},
				});
				alert('The old password invalid.')
				console.log('error', error);
			});
	};
};

export const deleteUser = userId => {
	return dispatch => {
		dispatch({ type: 'EVENT_SUBMIT_FORM_ACTION' });
		ApiServices()
			.deleteUser(userId)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllUsers(1));
					dispatch({
						type: 'TOGGLE_USER_DELETE',
						payload: {
							showDeleteDialog: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_USERS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_USERS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const getCountriesList = () => {
	return dispatch => {
		ApiServices()
			.getCategoryItems('country', 1)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_ALL_COUNTRIES',
						payload: {
							countriesList: responseData.data.items,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getAllCategories = () => {
	return dispatch => {
		ApiServices()
			.getAllCategories()
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_ALL_CATEGORIES',
						payload: {
							categories: responseData.data,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getAllCategoriesItems = () => {
	return dispatch => {
		dispatch({ type: 'ALL_CATEGORIES_ITEMS_FETCHING' });
		ApiServices()
			.getAllCategoriesItems()
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_ALL_CATEGORIES_ITEMS',
						payload: {
							allItems: responseData.data,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const createCategoryItem = (categoryName, categoryItemValue, updateFormDataCategoryItems) => {
	return dispatch => {
		dispatch({
			type: 'CREATE_CATEGORY_ACTION',
			payload: {
				categoryName: categoryName,
			},
		});
		ApiServices()
			.createCategoryItem(categoryName, categoryItemValue)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'CREATE_CATEGORY_ACTION_SUCCESS',
						payload: {
							categoryName: categoryName,
							data: responseData.data,
						},
					});
					updateFormDataCategoryItems({
						categoryName: categoryName,
						newItem: responseData.data,
					});
				} else {
					dispatch({
						type: 'CREATE_CATEGORY_ACTION_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'CREATE_CATEGORY_ACTION_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const createCategoryItemForCategoryPage = (categoryName, categoryItemValue) => {
	return dispatch => {
		ApiServices()
			.createCategoryItem(categoryName, categoryItemValue)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllCategoriesItems());
					dispatch({
						type: 'TOGGLE_CATEGORY_ITEM_ADD',
						payload: {
							showAddModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_CATEGORY_ITEM_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_CATEGORY_ITEM_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const updateCategoriesItem = (categoryItemId, categoryName, categoryItemUpdate) => {
	return dispatch => {
		ApiServices()
			.updateCategoryItem(categoryItemId, categoryName, categoryItemUpdate)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllCategoriesItems());
					dispatch({
						type: 'TOGGLE_CATEGORY_ITEM_UPDATE',
						payload: {
							showUpdateModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_CATEGORY_ITEM_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_CATEGORY_ITEM_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const deleteCategoryItem = (categoryItemId, categoryName) => {
	return dispatch => {
		ApiServices()
			.deleteCategoryItem(categoryItemId, categoryName)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllCategoriesItems());
					dispatch({
						type: 'TOGGLE_CATEGORY_ITEM_DELETE',
						payload: {
							showDeleteDialog: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_CATEGORY_ITEM_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_CATEGORY_ITEM_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const getAllEvents = params => {
	return dispatch => {
		dispatch({ type: 'ALL_EVENTS_FETCHING' });
		ApiServices()
			.getAllEvents(params)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_ALL_EVENTS',
						payload: {
							allEvents: responseData.data.events,
							totalData: responseData.data.total_data,
							eventDetails: responseData.data.events.length > 0 ? responseData.data.events[0] : null,
						},
					});
					dispatch({
						type: 'TOGGLE_SEARCH_EVENTS',
						payload: {
							showAddModalFilter: false
						}
					})
				} else {
					dispatch({ type: 'SUBMIT_EVENTS_ERROR' })
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				dispatch({ type: 'SUBMIT_EVENTS_ERROR' })
				console.log(`error: ${error}`);
			});
	};
};

const parseEvent = event => {
	return {
		id: event.id,
		country_id: event.country_id,
		city_id: event.city_id,
		location: event.location,
		region: event.region,
		lng: event.lng,
		lat: event.lat,
		date: event.date,
		report_heading: event.report_heading,
		other: event.other,
		weapons: event.weapon,
		attack_types: event.attack_type,
		targets_types: event.target_type,
		targets: event.target,
		evidences: event.evidence,
		kills_status: event.kills_status,
		kills: event.kills,
	};
};

export const createEvent = (event, countryId, files) => {
	return dispatch => {
		dispatch({ type: 'EVENT_SUBMIT_FORM_ACTION' });
		ApiServices()
			.createEvent(parseEvent(event))
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					const eventId = responseData.data.id;
					if (files) {
						files.files.forEach((file, index) => {
							const formData = new FormData();
							formData.append('file', file.src.file);
							ApiServices()
								.uploadFiles(formData, eventId)
								.then(response => {
									let responseData = response.data;
									if (responseData.status_code === 200) {
										dispatch(getAllEvents({ pageNum: 1, countryId }));
									} else {
										dispatch({
											type: 'SUBMIT_EVENTS_ERROR',
											payload: {
												error: responseData.message,
											},
										});
										alert(responseData.message);
									}
								})
								.catch(error => {
									dispatch({
										type: 'SUBMIT_EVENTS_ERROR',
										payload: {
											error: error,
										},
									});
									alert(error);
								});
						});
					}
					dispatch(getAllEvents({ pageNum: 1, countryId }));
					dispatch({
						type: 'TOGGLE_EVENT_ADD',
						payload: {
							showAddModal: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_EVENTS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_EVENTS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const updateEvent = (event, countryId, pageNum, files) => {
	return dispatch => {
		dispatch({ type: 'EVENT_SUBMIT_FORM_ACTION' });
		ApiServices()
			.updateEvent(event.id, parseEvent(event))
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					if (files) {
						files.files.forEach((file, index) => {
							const formData = new FormData();
							formData.append('file', file.src.file);
							ApiServices()
								.uploadFiles(formData, event.id)
								.then(response => {
									let responseData = response.data;
									if (responseData.status_code === 200) {
										dispatch(getAllEvents({ pageNum: pageNum + 1, countryId }));
									} else {
										dispatch({
											type: 'SUBMIT_EVENTS_ERROR',
											payload: {
												error: responseData.message,
											},
										});
										alert(responseData.message);
									}
								})
								.catch(error => {
									dispatch({
										type: 'SUBMIT_EVENTS_ERROR',
										payload: {
											error: error,
										},
									});
									alert(error);
								});
						});
					}
					dispatch(getAllEvents({ pageNum: pageNum + 1, countryId }));
					dispatch({
						type: 'TOGGLE_EVENT_UPDATE',
						payload: {
							showUpdateForm: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_EVENTS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_EVENTS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const deleteEvent = (eventId, countryId, pageNum) => {
	return dispatch => {
		dispatch({ type: 'EVENT_SUBMIT_FORM_ACTION' });
		ApiServices()
			.deleteEvent(eventId)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch(getAllEvents({ pageNum: pageNum + 1, countryId }));
					dispatch({
						type: 'TOGGLE_EVENT_DELETE',
						payload: {
							showDeleteDialog: false,
						},
					});
				} else {
					dispatch({
						type: 'SUBMIT_EVENTS_ERROR',
						payload: {
							error: responseData.message,
						},
					});
					alert(responseData.message);
				}
			})
			.catch(error => {
				dispatch({
					type: 'SUBMIT_EVENTS_ERROR',
					payload: {
						error: error,
					},
				});
				alert(error);
			});
	};
};

export const getEvent = eventId => {
	return dispatch => {
		dispatch({ type: 'EVENT_DETAILS_FETCHING' });
		ApiServices()
			.getEvent(eventId)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_EVENT_DETAILS',
						payload: {
							eventDetails: responseData.data,
							eventId: eventId,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getMapCoordinates = (countryId, eventSearch) => {
	return dispatch => {
		dispatch({ type: 'MAP_COORDINATES_FETCHING' })
		dispatch({ type: 'MAP_SEARCH_EVENTS_SUBMIT_FORM_ACTION' })
		if (!countryId)
			return;
		ApiServices()
			.getMapCoordinates(countryId, eventSearch)
			.then(response => {
				let responseData = response.data
				if (responseData.status_code === 731) {
					dispatch({
						type: 'Cleare_MAP_COORDINATES',
					})
					dispatch({
						type: 'Cleare_MAP_BOUNDARIES',
					})
				}
				if (responseData.status_code === 200) {
					const mappedBounds = [
						[
							responseData.data.bounds.south_west.lng,
							responseData.data.bounds.south_west.lat
						],
						[
							responseData.data.bounds.north_east.lng,
							responseData.data.bounds.north_east.lat
						]
					]
					dispatch({
						type: 'GET_MAP_COORDINATES',
						payload: {
							mapCoordinates: responseData.data.coordinates,
							bounds: mappedBounds
						}
					})
					dispatch({
						type: 'TOGGLE_MAP_SEARCH_EVENTS_COORDINATES',
						payload: {
							showAddModal: false
						}
					})
				} else {
					dispatch({ type: 'SUBMIT_MAP_SEARCH_EVENTS_ERROR' })
					console.log(
						`error ${responseData.status_code}: ${responseData.message}`
					)
				}
			})
			.catch(error => {
				dispatch({ type: 'SUBMIT_MAP_SEARCH_EVENTS_ERROR' })
				setTimeout(() => {
					dispatch({
						type: 'Cleare_MAP_BOUNDARIES',
					})
				}, 3000)
				dispatch({
					type: 'Cleare_MAP_COORDINATES',
				})
				console.log(`error: ${error}`)
			})
	}
}

export const getMapChoropleth = (level, countryId, eventSearch) => {
	return dispatch => {
		if (!countryId || level === 0)
			return;
		dispatch({ type: 'MAP_CHOROPLETH_FETCHING' })
		dispatch({ type: 'MAP_SEARCH_EVENTS_SUBMIT_FORM_ACTION' })
		ApiServices()
			.getMapChoropleth(level, countryId, eventSearch)
			.then(response => {
				let responseData = response.data
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_MAP_CHOROPLETH',
						payload: {
							mapChoropleth: responseData.data.coordinates,
							legend: responseData.data.legend
						}
					})
					dispatch({
						type: 'TOGGLE_MAP_SEARCH_EVENTS_COORDINATES',
						payload: {
							showAddModal: false
						}
					})
				} else {
					dispatch({ type: 'SUBMIT_MAP_SEARCH_EVENTS_ERROR' })
					console.log(
						`error ${responseData.status_code}: ${responseData.message}`
					)
				}
			})
			.catch(error => {
				dispatch({ type: 'SUBMIT_MAP_SEARCH_EVENTS_ERROR' })
				console.log(`error: ${error}`)
			})
	}
}

export const getEventsByCoordinate = (countryId, coordsId, pageNum) => {
	return dispatch => {
		dispatch({ type: 'COORD_EVENTS_FETCHING' });
		ApiServices()
			.getAllEvents({ countryId, coordsId, pageNum })
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_COORD_EVENTS',
						payload: {
							coordEvents: responseData.data.events,
							totalEvents: responseData.data.total_data,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getMoreEventsByCoordinate = (countryId, coordsId, pageNum) => {
	return dispatch => {
		dispatch({ type: 'COORD_EVENTS_LOADING_MORE' });
		ApiServices()
			.getAllEvents({ countryId, coordsId, pageNum })
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_COORD_EVENTS',
						payload: {
							coordEvents: responseData.data.events,
							totalEvents: responseData.data.total_data,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getMapGeocoding = (countryName, region, location) => {
	return dispatch => {
		ApiServices()
			.getMapGeocoding(countryName, region, location)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_MAP_Geocoding',
						payload: {
							mapGeocoding: responseData.data.coordinates,
						},
					});
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

export const getMapGeocodingUpdate = (countryName, region, location, selectedOldLng, selectedOldLat) => {
	return dispatch => {
		ApiServices()
			.getMapGeocoding(countryName, region, location)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					let coordinates = responseData.data.coordinates;
					const getArrayMatchLogLat = (array, lng, lat) => {
						if (array.length > 0) {
							return array.find(item => {
								return item.lng === lng && item.lat === lat;
							});
						} else return '';
					};
					let selectedOldRegion = getArrayMatchLogLat(coordinates, selectedOldLng, selectedOldLat);
					dispatch({
						type: 'GET_MAP_Geocoding_Update',
						payload: {
							mapGeocodingUpdate: responseData.data.coordinates,
							selectedOldRegion: selectedOldRegion,
						},
					});
				} else {
					alert(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				alert(`error: ${error}`);
			});
	};
};

export const getMapBoundaries = (level, countryId, index) => {
	return dispatch => {
		if (!countryId) return;
		dispatch({ type: 'MAP_BOUNDARIES_FETCHING' });
		ApiServices()
			.getMapBoundaries(level, countryId[index])
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_MAP_BOUNDARIES',
						payload: {
							mapBoundaries: responseData.data.features,
						},
					});
					if (index < countryId.length - 1) {
						return dispatch(getMapBoundaries(level, countryId, index + 1));
					}
				} else {
					console.log(`error ${responseData.status_code}: ${responseData.message}`);
				}
			})
			.catch(error => {
				console.log(`error: ${error}`);
			});
	};
};

// export const getFile = files => {
//   return dispatch => {
//     let Files = []
//     let typeFile = ''
//     let urlFile = ''
//     let objFile = {}
//     console.log('files', files)
//     if (files) {
//       files.forEach(file => {
//         ApiServices().getFile(file.id).then(response => {
//           console.log('response', response)
//           typeFile = response.headers['content-type']
//           urlFile = response.config['url']
//           objFile = {typeFile: typeFile, urlFile: urlFile}
//           Files.push(objFile)
//           if (response.status === 200) {
//             dispatch({
//               type: 'GIT_FILE',
//               payload: {
//                 file: Files
//               }
//             })
//           } else {
//             alert(response.statusText)
//           }
//         })
//           .catch(error => {
//             alert(error)
//           })
//       })
//     }
//   }
// }

export const getFile = fileId => {
	return dispatch => {
		ApiServices()
			.getFile(fileId)
			.then(response => {
				let urlFile = '';
				urlFile = response.config['url'];
				if (response.status === 200) {
					dispatch({
						type: 'GIT_FILE',
						payload: {
							file: urlFile,
						},
					});
				} else {
					alert(response.statusText);
				}
			})
			.catch(error => {
				alert(error);
			});
	};
};

export const deleteFile = fileId => {
	return dispatch => {
		ApiServices()
			.deleteFile(fileId)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
				} else {
					alert(responseData.message);
				}
			})
			.catch(error => {
				alert(error);
			});
	};
};

export const getUnivariableStatistics = (countryId, eventSearch) => {
	return dispatch => {
		ApiServices()
			.getUnivariableStatistics(countryId, eventSearch)
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 731) {
					dispatch({
						type: 'UNIVARIABLE_STATISTICS_FETCHING',
						payload: {
							fetching: false
						}
					});
				}
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_UNIVARIABLE_STATISTICS',
						payload: {
							univariableStatistics: responseData.data.statistics,
							univariableSummary: responseData.data.summary,
						},
					});
				} else {
					console.log(response.statusText);
				}
			})
			.catch(error => {
				dispatch({
					type: 'UNIVARIABLE_STATISTICS_FETCHING',
					payload: {
						fetching: false
					}
				});
				console.log(error);
			});
	};
};

export const getDashboardStatistics = () => {
	return dispatch => {
		dispatch({ type: 'DASHBOARD_STATISTICS_FETCHING' });
		ApiServices()
			.getDashboardStatistics()
			.then(response => {
				let responseData = response.data;
				if (responseData.status_code === 200) {
					dispatch({
						type: 'GET_DASHBOARD_STATISTICS',
						payload: {
							dashboardStatistics: responseData.data.statistics,
							dashboardSummary: responseData.data.summary,
						},
					});
				} else {
					console.log('response.statusText', response.statusText);
				}
			})
			.catch(error => {
				console.log('error', error);
			});
	};
};

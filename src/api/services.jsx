import instance from './middlewareRequest';
import cookie from 'react-cookie'
import { config } from '../config'

let axios = instance
var APIURL = config.url.API_URL

const serialize = obj => {
  var str = []
  for (var p in obj) {
    if (obj.hasOwnProperty(p) && obj[p]) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
    }
  }
  return str.join('&')
}

const repeatQuery = (nameQuery, array) => {
  let string = ''
  if (array && array.length > 0)
    for (let i = 0; i < array.length; i++) {
      string += `&${nameQuery}=${parseInt(array[i])}`
    }
  return string
}

export const ApiServices = () => {
  const auth = cookie.load('access_token')

  return {
    login: credentials => axios.post(`${APIURL}/authentication/login`, credentials),

    logout: () => {
      return axios.post(`${APIURL}/authentication/logout`, undefined, {
        headers: {
          Authorization: auth
        }
      })
    },

    refresh: (refreshToken) => {
      return axios.post(`${APIURL}/authentication/refresh`, undefined, {
        headers: {
          Authorization: refreshToken
        }
      })
    },

    createUser: user => {
      return axios.post(`${APIURL}/user/`, user, {
        headers: {
          Authorization: auth
        }
      })
    },

    getAllUsers: pageNum => {
      return axios.get(`${APIURL}/user/all?page_num=${pageNum}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    updateUserByID: (userId, user) => {
      return axios.put(`${APIURL}/user/profile/${userId}`, user, {
        headers: {
          Authorization: auth
        }
      })
    },

    updatePassword: (userId, userPassword) => {
      return axios.put(`${APIURL}/user/security/${userId}`, userPassword, {
        headers: {
          Authorization: auth
        }
      })
    },

    updateUserActivate: (userId, activated) => {
      return axios.put(`${APIURL}/user/activate/${userId}`, activated, {
        headers: {
          Authorization: auth
        }
      })
    },

    deleteUser: userId => {
      return axios.delete(`${APIURL}/user/${userId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getUser: userId => {
      return axios.get(`${APIURL}/user/${userId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    createCategoryItem: (categoryName, categoryItem) => {
      return axios.post(
        `${APIURL}/category/?category_name=${categoryName}`,
        {
          name: categoryItem
        },
        {
          headers: {
            Authorization: auth,
            'Content-Type': "application/json"
          }
        }
      )
    },

    getAllCategories: () => {
      return axios.get(`${APIURL}/category/all`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getAllCategoriesItems: () => {
      return axios.get(`${APIURL}/category/items`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getCategoryItems: (categoryName, pageNum, countryId) => {
      const query = serialize({
        category_name: categoryName,
        page_num: pageNum,
        country_id: countryId
      })
      return axios.get(`${APIURL}/category/items?${query}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    updateCategoryItem: (categoryItemId, categoryName, categoryItemUpdate) => {
      return axios.put(
        `${APIURL}/category/${categoryItemId}?category_name=${categoryName}`,
        {
          name: categoryItemUpdate
        },
        {
          headers: {
            Authorization: auth,
            'Content-Type': "application/json"
          }
        }
      )
    },

    getCategory: (categoryId, categoryName) => {
      return axios.get(
        `${APIURL}/category/${categoryId}?category_name=${categoryName}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    deleteCategoryItem: (categoryId, categoryName) => {
      return axios.delete(
        `${APIURL}/category/${categoryId}?category_name=${categoryName}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    getAllEvents: eventSearch => {

      let weapons = ''; let attackTypes = ''; let targetsTypes = ''; let targets = ''; let evidences = ''; let params = ''
      if (eventSearch) {
        weapons = eventSearch.weapon ? repeatQuery('weapons', eventSearch.weapon) : ''
        attackTypes = eventSearch.attack_type ? repeatQuery('attack_types', eventSearch.attack_type) : ''
        targetsTypes = eventSearch.target_type ? repeatQuery('targets_types', eventSearch.target_type) : ''
        targets = eventSearch.target ? repeatQuery('targets', eventSearch.target) : ''
        evidences = eventSearch.evidence ? repeatQuery('evidences', eventSearch.evidence) : ''
        params = {
          page_num: eventSearch.pageNum,
          country_id: eventSearch.countryId,
          since_date: eventSearch.sinceDate === 'Invalid date' ? '' : eventSearch.sinceDate,
          to_date: eventSearch.toDate === 'Invalid date' ? '' : eventSearch.toDate,
          gte_kills: eventSearch.gteKills,
          lte_kills: eventSearch.lteKills,
          other: eventSearch.other,
          region: eventSearch.region,
          report_heading: eventSearch.reportHeading,
          coords_id: eventSearch.coordsId,
          order_by: eventSearch.orderBy,
          asc: eventSearch.asc
        }
      }

      const query = params ? `&${serialize(params)}` : ''

      return axios.get(`${APIURL}/event/all?${query}${weapons}${attackTypes}${targetsTypes}${targets}${evidences}`,
        {
          headers: {
            Authorization: auth
          }
        })
    },

    createEvent: (event) => {
      return axios.post(
        `${APIURL}/event/`,
        event,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    updateEvent: (eventId, event) => {
      return axios.put(`${APIURL}/event/${eventId}`, event, {
        headers: {
          Authorization: auth
        }
      })
    },

    deleteEvent: eventId => {
      return axios.delete(`${APIURL}/event/${eventId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getEvent: eventId => {
      return axios.get(`${APIURL}/event/${eventId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getMapCoordinates: (countryId, eventSearch = null) => {
      let countryiesId = countryId ? repeatQuery('country_id', countryId) : ''
      let weapons = ''; let attackTypes = ''; let targetsTypes = ''; let targets = ''; let evidences = ''; let params = ''
      if (eventSearch) {
        weapons = eventSearch.weapon ? repeatQuery('weapons', eventSearch.weapon) : ''
        attackTypes = eventSearch.attack_type ? repeatQuery('attack_types', eventSearch.attack_type) : ''
        targetsTypes = eventSearch.target_type ? repeatQuery('targets_types', eventSearch.target_type) : ''
        targets = eventSearch.target ? repeatQuery('targets', eventSearch.target) : ''
        evidences = eventSearch.evidence ? repeatQuery('evidences', eventSearch.evidence) : ''
        params = {
          since_date: eventSearch.sinceDate === 'Invalid date' ? '' : eventSearch.sinceDate,
          to_date: eventSearch.toDate === 'Invalid date' ? '' : eventSearch.toDate,
          gte_kills: eventSearch.gteKills,
          lte_kills: eventSearch.lteKills,
          other: eventSearch.other,
          region: eventSearch.region,
          report_heading: eventSearch.reportHeading,
        }
      }

      const query = params ? `&${serialize(params)}` : ''

      return axios.get(
        `${APIURL}/map/points?${countryiesId}${query}${weapons}${attackTypes}${targetsTypes}${targets}${evidences}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    getMapGeocoding: (countryName, region, location) => {
      return axios.get(
        `${APIURL}/map/geocoding?country_name=${countryName}&region=${region}&location=${location}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    getMapChoropleth: (level, countryId, eventSearch = null) => {
      let countryiesId = countryId ? repeatQuery('country_id', countryId) : ''
      let weapons = ''; let attackTypes = ''; let targetsTypes = ''; let targets = ''; let evidences = ''; let params = ''
      if (eventSearch) {
        weapons = eventSearch.weapon ? repeatQuery('weapons', eventSearch.weapon) : ''
        attackTypes = eventSearch.attack_type ? repeatQuery('attack_types', eventSearch.attack_type) : ''
        targetsTypes = eventSearch.target_type ? repeatQuery('targets_types', eventSearch.target_type) : ''
        targets = eventSearch.target ? repeatQuery('targets', eventSearch.target) : ''
        evidences = eventSearch.evidence ? repeatQuery('evidences', eventSearch.evidence) : ''
        params = {
          since_date: eventSearch.sinceDate === 'Invalid date' ? '' : eventSearch.sinceDate,
          to_date: eventSearch.toDate === 'Invalid date' ? '' : eventSearch.toDate,
          gte_kills: eventSearch.gteKills,
          lte_kills: eventSearch.lteKills,
          other: eventSearch.other,
          region: eventSearch.region,
          report_heading: eventSearch.reportHeading,
        }
      }

      const query = params ? `&${serialize(params)}` : ''

      return axios.get(
        `${APIURL}/map/choropleth?level=${level}${countryiesId}${query}${weapons}${attackTypes}${targetsTypes}${targets}${evidences}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    getMapBoundaries: (level, countryId) => {
      return axios.get(
        `${APIURL}/map/boundaries?level=${level}&country_id=${countryId}`,
        {
          headers: {
            Authorization: auth
          }
        }
      )
    },

    uploadFiles: (file, eventId) => {
      return axios.post(`${APIURL}/file/?event_id=${eventId}`, file, {
        headers: {
          Authorization: auth,
          'Content-Type': `multipart/form-data`
        }
      })
    },

    getFile: fileId => {
      return axios.get(`${APIURL}/file/${fileId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    deleteFile: fileId => {
      return axios.delete(`${APIURL}/file/${fileId}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getUnivariableStatistics: (countryId, eventSearch = null )=> {
      let countryiesId = countryId ? repeatQuery('country_id', countryId) : ''
      let weapons = ''; let attackTypes = ''; let targetsTypes = ''; let targets = ''; let evidences = ''; let params = ''
      if (eventSearch) {
        weapons = eventSearch.weapon ? repeatQuery('weapons', eventSearch.weapon) : ''
        attackTypes = eventSearch.attack_type ? repeatQuery('attack_types', eventSearch.attack_type) : ''
        targetsTypes = eventSearch.target_type ? repeatQuery('targets_types', eventSearch.target_type) : ''
        targets = eventSearch.target ? repeatQuery('targets', eventSearch.target) : ''
        evidences = eventSearch.evidence ? repeatQuery('evidences', eventSearch.evidence) : ''
        params = {
          since_date: eventSearch.sinceDate === 'Invalid date' ? '' : eventSearch.sinceDate,
          to_date: eventSearch.toDate === 'Invalid date' ? '' : eventSearch.toDate,
          gte_kills: eventSearch.gteKills,
          lte_kills: eventSearch.lteKills,
          other: eventSearch.other,
          region: eventSearch.region,
          report_heading: eventSearch.reportHeading,
        }
      }

      const query = params ? `&${serialize(params)}` : ''

      return axios.get(`${APIURL}/statistics/statistics_page?top_n=10${countryiesId}${query}${weapons}${attackTypes}${targetsTypes}${targets}${evidences}`, {
        headers: {
          Authorization: auth
        }
      })
    },

    getDashboardStatistics: () => {
      return axios.get(`${APIURL}/statistics/dashboard`, {
        headers: {
          Authorization: auth
        }
      })
    },

  }
}

import React, {useEffect} from 'react'
import { connect } from 'react-redux'
import { NavLink, Redirect } from 'react-router-dom'
import { logout, getUser } from '../api/helper'
import { makeStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, Link, Button, Menu, MenuItem } from '@material-ui/core'
import cookie from 'react-cookie'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  navLinks: {
    flexGrow: 1,
    '& a': {
      marginRight: theme.spacing(2)
    },
    '& a.active': {
      fontWeight: 'bold'
    }
  }
}))

const mapStateToProps = ({ userProfile: { isAdmin, userProfile, user } }) => {
  return {
    isAdmin,
    userProfile,
    user
  }
}

const mapDispatchToProps = dispatch => ({
  postLogout: () => {
    dispatch(logout())
  },
  dispatchGetUser: userId => {
    dispatch(getUser(userId))
  },
})

const Header = ({ postLogout, isAdmin, userProfile, user, dispatchGetUser }) => {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  let username = user ? user.username : ''
  const userStorInfo = JSON.parse(localStorage.getItem('user_profile'))
  useEffect(() => {
    userStorInfo && dispatchGetUser(userStorInfo.id)
    // eslint-disable-next-line
  }, [dispatchGetUser]);

  const isAuthenticated = cookie.load('access_token')
  if (!isAuthenticated) {
    return <Redirect to='/login' />
  }

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <div className={classes.navLinks}>
            <Link
              color='inherit'
              component={NavLink}
              to='/'
              activeClassName={classes.active}
            >
              DaeshBoard
            </Link>
            {/* <Link
              color='inherit'
              component={NavLink}
              to='/categories'
              activeClassName={classes.active}
            >
              Categories
            </Link> */}
          </div>
          <Button aria-controls="simple-menu" aria-haspopup="true" color='inherit' style={{textTransform: 'capitalize'}} onClick={handleClick}>
            {username}
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={() => {
              handleClose()
            }}>
              <Link
                color='inherit'
                component={NavLink}
                to='/profile'
                activeClassName={classes.active}
                underline="none"
              >
                My profile
            </Link>
            </MenuItem>
            {isAdmin && <MenuItem onClick={() => {
              handleClose()
            }}>
              <Link
                color='inherit'
                component={NavLink}
                to='/users'
                exact
                activeClassName={classes.active}
                underline="none"
              >
                Users
              </Link>
            </MenuItem>
            }

            {isAdmin && <MenuItem onClick={() => {
              handleClose()
            }}>
              <Link
                color='inherit'
                component={NavLink}
                to='/categories'
                exact
                activeClassName={classes.active}
                underline="none"
              >
                Categories
              </Link>
            </MenuItem>
            }


            {/* {isAdmin && <MenuItem onClick={() => {
              handleClose()
            }}>
              <Link
                color='inherit'
                component={NavLink}
                to='/categories'
                activeClassName={classes.active}
                underline="none"
              >
                Categories
            </Link>
            </MenuItem>
            } */}


            {/* {isAdmin && <MenuItem onClick={() => {
              handleClose()
            }}>
              <Link
                color='inherit'
                component={NavLink}
                to='/categories'
                activeClassName={classes.active}
                underline="none"
              >
                Categories
            </Link>
            </MenuItem>
            } */}
            <MenuItem onClick={() => {
              handleClose()
              postLogout()
            }}>
              Logout
            </MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)

import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Route } from 'react-router-dom'
import Header from './Header'

const useStyles = makeStyles(theme => ({
  root: {
    margin: 0,
    padding: 0,
    height: `calc(100vh - ${theme.spacing(8)}px)`
  }
}))

const PrivateLayout = ({ component: Component, ...restProps }) => {
  const classes = useStyles()
  return (
    <Fragment>
      <Header />
      <div className={classes.root}>
        <Component {...restProps} />
      </div>
    </Fragment>
  )
}

const PrivateRoute = ({ component, ...restProps }) => {
  return (
    <Route
      {...restProps}
      exact
      render={props => <PrivateLayout component={component} {...props} />}
    />
  )
}

export default PrivateRoute

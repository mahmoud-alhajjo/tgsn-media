
const maxDescLength = 140
export const getTrimmedText = text => {
  if (text && text.length > maxDescLength) {
    let trimmedString = text.substring(0, maxDescLength)
    return trimmedString.substr(
      0,
      Math.min(trimmedString.length, trimmedString.lastIndexOf(' '))
    ) + '...'
  }
  return text
}
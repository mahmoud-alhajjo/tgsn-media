import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Typography,
  CircularProgress
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { ValidatorForm } from 'react-material-ui-form-validator'

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
})

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    outline: 'none'
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}))

const MuiDialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props
  return (
    <DialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant='h6'>
        {children}
      </Typography>
      {onClose
        ? <IconButton
          aria-label='close'
          className={classes.closeButton}
          onClick={onClose}
          >
          <CloseIcon />
        </IconButton>
        : null}
    </DialogTitle>
  )
})

const DialogForm = ({
  open,
  handleClose,
  title,
  onSubmit,
  submitting,
  submitLabel,
  children
}) => {
  const classes = useStyles()
  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='customized-dialog-title'
      open={open}
      disableBackdropClick
    >
      <MuiDialogTitle id='customized-dialog-title' onClose={handleClose}>
        {title}
      </MuiDialogTitle>

      <ValidatorForm className={classes.form} onSubmit={onSubmit}>
        <DialogContent padding='large'>
          {children}
        </DialogContent>
        <DialogActions>
          {submitting && <CircularProgress />}
          <Button
            type='submit'
            color='primary'
            disabled={submitting}
            size='large'
          >
            {submitLabel}
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  )
}

export default DialogForm

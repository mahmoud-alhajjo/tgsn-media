import React from 'react'
import clsx from 'clsx'
import CreatableSelect from 'react-select/creatable'
import { emphasize, makeStyles } from '@material-ui/core/styles'
import NoSsr from '@material-ui/core/NoSsr'
import TextField from '@material-ui/core/TextField'
import Chip from '@material-ui/core/Chip'
import MenuItem from '@material-ui/core/MenuItem'
import CancelIcon from '@material-ui/icons/Cancel'

const useStyles = makeStyles(theme => ({
  'root-outlined': {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(1),
    height: 'auto',
    flexGrow: 1
  },
  'root-standard': {
    height: 'auto',
    flexGrow: 1
  },
  'input-outlined': {
    display: 'flex',
    padding: theme.spacing(1),
    height: 'auto'
  },
  'input-standard': {
    display: 'flex',
    padding: 0,
    height: 'auto'
  },
  'rootSmall-outlined': {
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(0.5),
    height: 'auto',
    flexGrow: 1
  },
  'inputSmall-outlined': {
    display: 'flex',
    paddingLeft: 8,
    paddingRight: 0,
    paddingTop: 3.5,
    paddingBottom: 3.5,
    height: 'auto'
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
    minHeight: theme.spacing(5)
  },
  chip: {
    margin: theme.spacing(0.5, 0.25)
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light'
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
      0.08
    )
  }
}))

const inputComponent = ({ inputRef, ...props }) => {
  return <div ref={inputRef} {...props} />
}

const Control = props => {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps }
  } = props

  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: classes[`input${customSmallStyle}-${TextFieldProps.variant}`],
          ref: innerRef,
          children,
          ...innerProps
        }
      }}
      {...TextFieldProps}
    />
  )
}

const Option = props => {
  return (
    <MenuItem
      ref={props.innerRef}
      selected={props.isFocused}
      component='div'
      style={{
        fontWeight: props.isSelected ? 500 : 400
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  )
}

const ValueContainer = props => {
  return (
    <div className={props.selectProps.classes.valueContainer}>
      {props.children}
    </div>
  )
}

let size = ''
let customSmallStyle = ''

const MultiValue = props => {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={clsx(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
      size={size}
    />
  )
}

const components = {
  Control,
  MultiValue,
  Option,
  ValueContainer
}

export default function AutoComplete({
  options,
  onChange,
  onCreateOption,
  value,
  label,
  variant,
  isMulti,
  isLoading,
  sizeChip,
  sizeInput
}) {
  const classes = useStyles()
  size = sizeChip
  customSmallStyle = sizeInput

  const selectStyles = {
    menu: base => ({ ...base, zIndex: 99 }),
    clearIndicator: base => ({ ...base, cursor: 'pointer' }),
    dropdownIndicator: base => ({ ...base, cursor: 'pointer' }),
    singleValue: base => ({ ...base, fontSize: '16px' })
  }

  const mapOption = option => {
    return option ? { label: option.name, value: option.id } : {}
  }

  const mappedOptions = options.map(option => mapOption(option))

  const getValue = () => {
    if (isMulti) return value.map(v => mapOption(v))
    else return mapOption(value)
  }

  return (
    <div className={classes[`root${sizeInput}-${variant}`]}>
      <NoSsr>
        <CreatableSelect
          isClearable={value}
          classes={classes}
          styles={selectStyles}
          inputId='react-select-multiple'
          TextFieldProps={{
            label,
            variant,
            InputLabelProps: {
              htmlFor: 'react-select-multiple',
              shrink: true
            }
          }}
          options={mappedOptions}
          components={components}
          value={getValue()}
          onChange={onChange}
          onCreateOption={onCreateOption}
          isMulti={isMulti}
          placeholder=''
          menuPlacement='auto'
          isLoading={isLoading}
        />
      </NoSsr>
    </div>
  )
}

import React from 'react'
import MaterialTable, { MTableToolbar } from 'material-table'
import { IconButton, Box, TablePagination } from '@material-ui/core'
import { Add as AddIcon } from '@material-ui/icons'
import Tooltip from '../components/Tooltip'

const Table = ({
  searchable = false,
  sortable = false,
  allowEdit,
  allowDelete,
  allowAdd,
  columns,
  data,
  rowsPerPage,
  pageNumber,
  onChangePage,
  totalData,
  isLoading,
  title,
  onRowUpdate,
  onRowDelete,
  onRowAdd,
  onRowClick,
  selectedRow,
  addTooltip,
  ...restProps
}) => {
  const actions = []
  if (allowEdit) {
    actions.push({
      icon: 'edit',
      tooltip: 'Edit',
      onClick: (event, rowData) => onRowUpdate(rowData)
    })
  }
  if (allowDelete) {
    actions.push({
      icon: 'delete',
      tooltip: 'Delete',
      onClick: (event, rowData) => onRowDelete(rowData.id)
    })
  }
  return (
    <MaterialTable
      options={{
        search: searchable,
        sorting: sortable,
        pageSize: rowsPerPage,
        pageSizeOptions: [],
        headerStyle: {
          backgroundColor: '#3f51b5',
          color: '#fff',
          fontWeight: 'bold',
          fontSize: '14px'
        },
        rowStyle: rowData => ({
          backgroundColor:
            selectedRow && selectedRow.id === rowData.id ? '#EEE' : '#FFF'
        })
      }}
      localization={{
        body: {},
        header: {
          actions: ''
        }
      }}
      columns={columns}
      data={data}
      title={title}
      isLoading={isLoading}
      actions={actions}
      components={{
        Pagination: props =>
          <TablePagination
            {...props}
            count={totalData}
            page={pageNumber}
            onChangePage={(event, newPage) => onChangePage(newPage)}
          />,
        Toolbar: props =>
          <Box display='flex' justifyContent='space-between'>
            <MTableToolbar {...props} />
            {allowAdd &&
              <Box m={1}>
                <Tooltip title={addTooltip} placement="top">
                  <IconButton onClick={onRowAdd}>
                    <AddIcon />
                  </IconButton>
                </Tooltip>
              </Box>}
          </Box>
      }}
      onRowClick={onRowClick}
      {...restProps}
    />
  )
}

export default Table

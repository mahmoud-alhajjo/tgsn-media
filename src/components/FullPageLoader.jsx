import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { CircularProgress, Box } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  fullPage: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  }
}))

const FullPageLoader = () => {
  const classes = useStyles()
  return (
    <Box className={classes.fullPage}>
      <CircularProgress />
    </Box>
  )
}

export default FullPageLoader
import React from 'react'
import PrivateRoute from './components/PrivateRoute'
import Login from './views/Login'
import MainTabs from './views/Map/MainTabs'
import Events from './views/Events/Events'
import Map from './views/Map/Map'
import Users from './views/Users/Users'
import Categories from './views/Categories/Categories'
import './App.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reducers from './store/reducers'
import Authentication from './context/authentication'
import Dashboard from './views/Dashboard/Dashboard'
import Profile from './views/Profile/Profile'


const store = createStore(reducers, applyMiddleware(thunk))

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <Authentication>
            <PrivateRoute path='/' component={Dashboard} />
            <PrivateRoute path='/map' component={Map} />
            <PrivateRoute path='/events/:country' component={Events} />
            <PrivateRoute path='/map/:countries' component={MainTabs} />
            <PrivateRoute path='/users' component={Users} />
            <PrivateRoute path='/categories' component={Categories} />
            <PrivateRoute path='/profile' component={Profile} />
          </Authentication>
          <Route path='/login' component={Login} />
        </div>
      </Router>
    </Provider>
  )
}

export default App

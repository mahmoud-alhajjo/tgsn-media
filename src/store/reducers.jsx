import { combineReducers } from 'redux'
import userProfile from './userProfile'
import users from './users'
import categories from './categories'
import events from './events'
import eventDetails from './eventDetails'
import statistics from './statistics'
import dashboard from './dashboard'
import map from './map'

export default combineReducers({
  userProfile,
  users,
  categories,
  events,
  eventDetails,
  statistics,
  dashboard,
  map
})

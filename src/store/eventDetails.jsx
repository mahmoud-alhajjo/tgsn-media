const initialState = {
  eventDetails: null,
  fetching: false,
  isSelected: false,
  eventId: null
}

export default function eventDetails (state = initialState, action) {
  switch (action.type) {
    case 'EVENT_DETAILS_FETCHING':
      return {
        ...state,
        fetching: true
      }
    case 'GET_EVENT_DETAILS':
      return {
        ...state,
        eventDetails: action.payload.eventDetails,
        eventId: action.payload.eventId,
        fetching: false
      }
    case 'GET_ALL_EVENTS':
      return {
        ...state,
        eventDetails: action.payload.eventDetails,
        isSelected: false
      }
    case 'TOGGLE_EVENT_VIEW':
      return {
        ...state,
        eventDetails: action.payload.eventDetails,
        isSelected: true
      }
    default:
      return state
  }
}

const initialState = {
  users: [],
  totalData: 0,
  fetching: false,
  submitting: false,
  showUpdateModal: false,
  showAddModal: false,
  showDeleteDialog: false,
  error: null
}

export default function users (state = initialState, action) {
  switch (action.type) {
    case 'ALL_USERS_FETCHING':
      return {
        ...state,
        fetching: true,
        error: null
      }
    case 'GET_ALL_USERS':
      return {
        ...state,
        users: action.payload.users,
        totalData: action.payload.totalData,
        fetching: false,
        submitting: false
      }
    case 'ALL_USERS_ERROR':
      return {
        ...state,
        error: action.payload.error,
        fetching: false
      }
    case 'SUBMIT_USERS_ERROR':
      return {
        ...state,
        error: action.payload.error,
        submitting: false
      }
    case 'USER_SUBMIT_FORM_ACTION':
      return {
        ...state,
        submitting: true
      }
    case 'TOGGLE_USER_UPDATE':
      return {
        ...state,
        showUpdateModal: action.payload.showUpdateModal,
        submitting: false
      }
    case 'TOGGLE_USER_ADD':
      return {
        ...state,
        showAddModal: action.payload.showAddModal,
        submitting: false
      }
    case 'TOGGLE_USER_DELETE':
      return {
        ...state,
        showDeleteDialog: action.payload.showDeleteDialog,
        submitting: false
      }
    default:
      return state
  }
}

const initialState = {
    univariableStatistics: null,
    univariableSummary: null,
    fetching: true
  }
  
  export default function statistics (state = initialState, action) {
    switch (action.type) {
      case 'UNIVARIABLE_STATISTICS_FETCHING':
        return {
          ...state,
          fetching: action.payload ?  action.payload.fetching : true
        }
      case 'GET_UNIVARIABLE_STATISTICS':
        return {
          ...state,
          univariableStatistics: action.payload.univariableStatistics,
          univariableSummary: action.payload.univariableSummary,
          fetching: false
        }
      default:
        return state
    }
  }
  
import { getDataBounds } from './helper'

const initialState = {
  mapCoordinates: null,
  coordEvents: [],
  fetching: false,
  fetchingBoundaries: false,
  fetchingEvents: false,
  fetchingChoropleth: false,
  totalEvents: 0,
  mapGeocoding: [],
  mapGeocodingUpdate: [],
  selectedOldRegion: null,
  mapChoropleth: null,
  legend: '',
  submitting: false,
  showAddModal: false,
  mapBoundaries: []
}

export default function map(state = initialState, action) {
  switch (action.type) {
    case 'MAP_COORDINATES_FETCHING':
      return {
        ...state,
        fetching: true
      }
    case 'GET_MAP_COORDINATES':
      return {
        ...state,
        mapCoordinates: action.payload.mapCoordinates,
        bounds: action.payload.bounds,
        fetching: false
      }
      case 'Cleare_MAP_COORDINATES':
        return {
          ...state,
          fetching: false,
          bounds: null,
          mapCoordinates: null,
        }
    case 'MAP_CLUSTER_ZOOM_ACTION':
      return {
        ...state,
        bounds: getDataBounds({ data: action.payload.clusterLeaves })
      }
    case 'COORD_EVENTS_FETCHING':
      return {
        ...state,
        coordEvents: [],
        fetchingEvents: true
      }
    case 'COORD_EVENTS_LOADING_MORE':
      return {
        ...state,
        loadingMoreEvents: true
      }
    case 'GET_COORD_EVENTS':
      return {
        ...state,
        submitting: true,
        coordEvents: state.coordEvents.concat(action.payload.coordEvents),
        totalEvents: action.payload.totalEvents,
        fetchingEvents: false,
        loadingMoreEvents: false
      }
    case 'GET_MAP_Geocoding':
      return {
        ...state,
        mapGeocoding: action.payload.mapGeocoding
      }
    case 'GET_MAP_Geocoding_Update':
      return {
        ...state,
        mapGeocodingUpdate: action.payload.mapGeocodingUpdate,
        selectedOldRegion: action.payload.selectedOldRegion
      }
    case 'EMPTYING_OF_MAP_GEOCODING':
      return {
        ...state,
        mapGeocoding: []
      }
    case 'MAP_CHOROPLETH_FETCHING':
      return {
        ...state,
        fetchingChoropleth: true
      }
    case 'GET_MAP_CHOROPLETH':
      return {
        ...state,
        mapChoropleth: action.payload.mapChoropleth,
        legend: action.payload.legend,
        fetchingChoropleth: false
      }
    case 'MAP_BOUNDARIES_FETCHING':
      return {
        ...state,
        fetchingBoundaries: true
      }
    case 'GET_MAP_BOUNDARIES':
      return {
        ...state,
        mapBoundaries: state.mapBoundaries.concat(action.payload.mapBoundaries),
        fetchingBoundaries: false
      }
    case 'Cleare_MAP_BOUNDARIES':
      return {
        ...state,
        mapBoundaries: [],
      }
    case 'SUBMIT_MAP_SEARCH_EVENTS_ERROR':
      return {
        ...state,
        submitting: false,
        fetching: false,
        fetchingChoropleth: false
      }
    case 'TOGGLE_MAP_SEARCH_EVENTS_COORDINATES':
      return {
        ...state,
        showAddModal: action.payload.showAddModal,
        submitting: false
      }
    default:
      return state
  }
}

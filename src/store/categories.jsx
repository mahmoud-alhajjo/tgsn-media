const initialState = {
  countriesList: [],
  categories: [],
  allItems: [],
  fetching: false,
  error: null,
  submitting: false,
  showAddModal: false,
  showDeleteDialog: false,
  showUpdateModal: false
}

export default function categories(state = initialState, action) {
  switch (action.type) {
    case 'GET_ALL_COUNTRIES':
      return {
        ...state,
        countriesList: action.payload.countriesList
      }
    case 'GET_ALL_CATEGORIES':
      return {
        ...state,
        categories: action.payload.categories
      }
    case 'ALL_CATEGORIES_ITEMS_FETCHING':
      return {
        ...state,
        fetching: true,
        error: null
      }
    case 'GET_ALL_CATEGORIES_ITEMS':
      return {
        ...state,
        allItems: action.payload.allItems,
        fetching: false,
        submitting: false
      }
    case 'CREATE_CATEGORY_ACTION':
      return {
        ...state,
      }
    case 'TOGGLE_CATEGORY_ITEM_ADD':
      return {
        ...state,
        showAddModal: action.payload.showAddModal,
        submitting: false
      }
    case 'SUBMIT_CATEGORY_ITEM_ERROR':
      return {
        ...state,
        error: action.payload.error,
        submitting: false
      }
    case 'TOGGLE_CATEGORY_ITEM_UPDATE':
      return {
        ...state,
        showUpdateModal: action.payload.showUpdateModal,
        submitting: false
      }
    case 'TOGGLE_CATEGORY_ITEM_DELETE':
      return {
        ...state,
        showDeleteDialog: action.payload.showDeleteDialog,
        submitting: false
      }
    case 'CREATE_CATEGORY_ACTION_SUCCESS':
      state.allItems[action.payload.categoryName].push(action.payload.data)
      return {
        ...state,
      }
    case 'CREATE_CATEGORY_ACTION_ERROR':
      return {
        ...state
      }
    default:
      return state
  }
}

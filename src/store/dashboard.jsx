const initialState = {
	dashboardStatistics: null,
	dashboardSummary: null,
	fetching: true,
};

export default function dashboard(state = initialState, action) {
	switch (action.type) {
		case 'DASHBOARD_STATISTICS_FETCHING':
			return {
				...state,
				fetching: true,
			};
		case 'GET_DASHBOARD_STATISTICS':
			return {
				...state,
				dashboardStatistics: action.payload.dashboardStatistics,
				dashboardSummary: action.payload.dashboardSummary,
				fetching: false,
			};
		default:
			return state;
	}
}

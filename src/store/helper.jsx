export const DEFAULT_MAP_BOUNDS = [[-80, -40], [160, 80]]
export const getDataBounds = ({ data }) => {
  let bounds = DEFAULT_MAP_BOUNDS

  if (data.length > 0) {
    const minLng = Math.min(...data.map(o => o.longitude))
    const maxLng = Math.max(...data.map(o => o.longitude))

    const minLat = Math.min(...data.map(o => o.latitude))
    const maxLat = Math.max(...data.map(o => o.latitude))

    bounds = [[minLng, minLat], [maxLng, maxLat]]
  }
  return bounds
}

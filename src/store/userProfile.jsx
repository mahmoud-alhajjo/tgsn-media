const initialState = {
  userProfile: null,
  isLoggedIn: false,
  isAdmin: false,
  requesting: false,
  error: null,
  fetching: false,
  user: ''
}

export default function userProfile (state = initialState, action) {
  switch (action.type) {
    case 'USER_PROFILE_REQUEST':
      return {
        ...state,
        requesting: true,
        error: null
      }
    case 'SET_USER_PROFILE':
      return {
        ...state,
        userProfile: action.payload.userProfile,
        isAdmin: action.payload.isAdmin,
        isLoggedIn: action.payload.isLoggedIn,
        requesting: false
      }
    case 'USER_PROFILE_ERROR':
      return {
        ...state,
        error: action.payload.error,
        requesting: false
      }
      case 'USER_FETCHING':
        return {
          ...state,
          fetching: true,
          error: null
        }
      case 'GET_USER':
        return {
          ...state,
          user: action.payload.user,
          fetching: false,
          submitting: false
        }
    default:
      return state
  }
}

const initialState = {
  allEvents: [],
  totalData: 0,
  showAddModal: false,
  showUpdateForm: false,
  showDeleteDialog: false,
  submitting: false,
  fetching: false,
  fileUrl: '',
  showAddModalFilter: false
}

export default function events(state = initialState, action) {
  switch (action.type) {
    case 'ALL_EVENTS_FETCHING':
      return {
        ...state,
        fetching: true
      }
    case 'GET_ALL_EVENTS':
      return {
        ...state,
        allEvents: action.payload.allEvents,
        totalData: action.payload.totalData,
        fetching: false
      }
    case 'SUBMIT_EVENTS_ERROR':
      return {
        ...state,
        error: action.payload.error,
        submitting: false
      }
    case 'EVENT_SUBMIT_FORM_ACTION':
      return {
        ...state,
        submitting: true
      }
    case 'TOGGLE_EVENT_UPDATE':
      return {
        ...state,
        showUpdateForm: action.payload.showUpdateForm,
        submitting: false
      }
    case 'TOGGLE_EVENT_ADD':
      return {
        ...state,
        showAddModal: action.payload.showAddModal,
        submitting: false
      }
    case 'TOGGLE_EVENT_DELETE':
      return {
        ...state,
        showDeleteDialog: action.payload.showDeleteDialog,
        submitting: false
      }
    case 'GIT_FILE':
      return {
        ...state,
        fileUrl: action.payload.file,
      }
    case 'TOGGLE_SEARCH_EVENTS':
      return {
        ...state,
        showAddModalFilter: action.payload.showAddModalFilter,
        submitting: false
      }
    default:
      return state
  }
}

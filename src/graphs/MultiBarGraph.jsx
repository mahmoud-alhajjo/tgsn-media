import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const MultiBarGraph = ({ data, chartId, category, chartTitle, bars }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		// Create chart instance
		let chart = am4core.create(chartId, am4charts.XYChart);
		chart.colors.step = 2;

		// Add data
		data.forEach(item => {
			for (const key in item) {
				if (item[key] <= 0) {
					item[key] = null;
				}
			}
		});
		chart.data = data;

		// Create axes
		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = category;
		categoryAxis.renderer.inversed = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 30;

		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.logarithmic = true;

		// Create series
		function createSeries(field, name) {
			var series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = category;
			series.name = name;
			series.columns.template.tooltipText = '{name}: [bold]{valueY}[/]';
			series.columns.template.height = am4core.percent(100);
			series.sequencedInterpolation = true;

			var valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = '{valueY}';
			valueLabel.locationY = 0.5;
			valueLabel.label.hideOversized = true;
		}

		bars.forEach(bar => {
			createSeries(bar.field, bar.name);
		});

		if (bars.length > 1) chart.legend = new am4charts.Legend();

		chart.cursor = new am4charts.XYCursor();

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;
		title.marginBottom = 20;

		// exporting
		var options = chart.exporting.getFormatOptions('png');
		options.keepTainted = true;
		chart.exporting.setFormatOptions('png', options);
		chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;

		setChart(chart);
		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default MultiBarGraph;

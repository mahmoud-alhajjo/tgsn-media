import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const GanttChart = ({ data, chartId, chartTitle, chartTooltip, category, fromDate, toDate }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');
	useEffect(() => {
		let chart = am4core.create(chartId, am4charts.XYChart);
		chart.colors.step = 2;

		if (data) {
			chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

			chart.paddingRight = 50;

			var colorSet = new am4core.ColorSet();
			colorSet.saturation = 0.4;

			let chartData = data;
			chart.data = chartData;

			var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
			categoryAxis.dataFields.category = category;
			categoryAxis.renderer.grid.template.location = 0;
			categoryAxis.renderer.inversed = true;

			var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			dateAxis.renderer.minGridDistance = 50;
			dateAxis.baseInterval = { count: 1, timeUnit: 'day' };
			dateAxis.renderer.tooltipLocation = 20;

			var series1 = chart.series.push(new am4charts.ColumnSeries());
			series1.columns.template.height = am4core.percent(70);
			series1.columns.template.tooltipText = `[bold]{count} ${chartTooltip}:[/] {openDateX} to {dateX}`;

			series1.dataFields.openDateX = fromDate;
			series1.dataFields.dateX = toDate;
			series1.dataFields.categoryY = category;
			series1.columns.template.propertyFields.fill = 'color'; // get color from data
			series1.columns.template.propertyFields.stroke = 'color';
			series1.columns.template.strokeOpacity = 1;

			var title = chart.titles.create();
			title.text = chartTitle;
			title.fontSize = 25;
			title.marginBottom = 20;

			// exporting
			var options = chart.exporting.getFormatOptions('png');
			options.keepTainted = true;
			chart.exporting.setFormatOptions('png', options);
			chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;

			setChart(chart);
		}
		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default GanttChart;

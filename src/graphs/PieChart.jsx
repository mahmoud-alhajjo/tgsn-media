import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const PieChart = ({ data, chartId, category, value, chartTitle }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		let chart = am4core.create(chartId, am4charts.PieChart);
		if (data) {
			chart.data = data;
			chart.colors.step = 2;

			// Add and configure Series
			var pieSeries = chart.series.push(new am4charts.PieSeries());
			pieSeries.dataFields.value = value;
			pieSeries.dataFields.category = category;
			pieSeries.slices.template.stroke = am4core.color('#fff');
			pieSeries.slices.template.strokeWidth = 2;
			pieSeries.slices.template.strokeOpacity = 1;

			var title = chart.titles.create();
			title.text = chartTitle;
			title.fontSize = 25;
			title.marginBottom = 20;

			// exporting
			var options = chart.exporting.getFormatOptions('png');
			options.keepTainted = true;
			chart.exporting.setFormatOptions('png', options);
			chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;
			setChart(chart);
		}
		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default PieChart;

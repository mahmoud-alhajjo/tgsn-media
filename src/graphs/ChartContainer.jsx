import React, { useEffect } from 'react';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_dataviz from '@amcharts/amcharts4/themes/dataviz';
import { AMCHARTS_LICENSE } from './constants';
import GetAppIcon from '@material-ui/icons/GetApp';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import { IconButton, Box, Button, ButtonGroup, Popover } from '@material-ui/core';
import Tooltip from '../components/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
	popover: { transform: 'translateY(-30px)' },
	themeSwitcher: {
		margin: 4,
		padding: 0,
		height: 40,
		'& li': {
			listStyle: 'none',
			padding: 4,
			float: 'left',
			cursor: 'pointer',
			'& span': {
				display: 'block',
				width: 24,
				height: 24,
				border: '4px solid #fff',
				borderRadius: '20px',
			},
			'& .active': {
				borderColor: '#ddd',
			},
		},
	},
}));

am4core.addLicense(AMCHARTS_LICENSE);

const periodList = [
	{ name: 'day', value: 'd' },
	{ name: 'week', value: 'w' },
	{ name: 'month', value: 'm' },
	{ name: 'year', value: 'y' },
];

const ChartContainer = ({ chartId, exportPNG, years, setYear, periodSelector, period, setPeriod, theme, setTheme }) => {
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = React.useState(null);

	React.useEffect(() => {
		am4core.unuseAllThemes();
		am4core.useTheme(am4themes_animated);
	}, []);

	const handleClick = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const themePopoverOpened = Boolean(anchorEl);

	const changeTheme = theme => {
		am4core.unuseAllThemes();
		am4core.useTheme(am4themes_animated);
		switch (theme) {
			case 'animated':
				break;
			case 'material':
				am4core.useTheme(am4themes_material);
				break;
			case 'dataviz':
				am4core.useTheme(am4themes_dataviz);
				break;
			case 'kelly':
				am4core.useTheme(am4themes_kelly);
				break;

			default:
				break;
		}
		setTheme(theme);
		setAnchorEl(null);
	};
	return (
		<div style={{ position: 'relative', padding: 8 }}>
			{periodSelector && (
				<ButtonGroup size="small" style={{ marginLeft: 8, marginTop: 8 }}>
					{periodList.map(item => {
						return (
							<Button
								key={item.value}
								onClick={() => {
									setPeriod(item);
								}}
								variant={period.value === item.value ? 'contained' : ''}
								color={period.value === item.value ? 'secondary' : ''}
							>
								{item.name}
							</Button>
						);
					})}
				</ButtonGroup>
			)}

			{years && (
				<div style={{ position: 'absolute', top: '20%', left: '15%', zIndex: 99 }}>
					<select
						onChange={event => setYear(event.target.value)}
						style={{ height: '36px', fontSize: '16px' }}
					>
						{years.map((year, index) => {
							return <option key={index} value={year}>{year}</option>;
						})}
					</select>
				</div>
			)}

			<Tooltip title={'Change theme'} placement="top">
				<IconButton onClick={handleClick} style={{ float: 'right', position: 'relative' }}>
					<FormatColorFillIcon />
				</IconButton>
			</Tooltip>
			<Popover
				id={chartId + '-popover'}
				open={themePopoverOpened}
				anchorEl={anchorEl}
				onClose={handleClose}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'center',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'center',
				}}
				className={classes.popover}
			>
				<ul className={classes.themeSwitcher}>
					<li onClick={() => changeTheme('animated')}>
						<span
							className={theme === 'animated' && 'active'}
							style={{
								backgroundImage:
									'linear-gradient(45deg, #67b7dc 0%,#67b7dc 50%,#c767dc 50%,#c767dc 100%)',
							}}
						></span>
					</li>
					<li onClick={() => changeTheme('dataviz')}>
						<span
							className={theme === 'dataviz' && 'active'}
							style={{
								backgroundImage:
									'linear-gradient(45deg, #283250 0%,#283250 50%,#902c2d 50%,#902c2d 100%)',
							}}
						></span>
					</li>
					<li onClick={() => changeTheme('material')}>
						<span
							className={theme === 'material' && 'active'}
							style={{
								backgroundImage:
									'linear-gradient(45deg, #E91E63 0%,#E91E63 50%,#9C27B0 50%,#9C27B0 100%)',
							}}
						></span>
					</li>
					<li onClick={() => changeTheme('kelly')}>
						<span
							className={theme === 'kelly' && 'active'}
							style={{
								backgroundImage:
									'linear-gradient(45deg, #F3C300 0%,#F3C300 50%,#875692 50%,#875692 100%)',
							}}
						></span>
					</li>
				</ul>
			</Popover>

			<Tooltip title={'Export to PNG'} placement="top">
				<IconButton onClick={() => exportPNG()} style={{ float: 'right' }}>
					<GetAppIcon />
				</IconButton>
			</Tooltip>

			<div id={chartId} style={{ width: '100%', minHeight: '450px', display: 'inline-block' }}></div>
		</div>
	);
};

export default ChartContainer;

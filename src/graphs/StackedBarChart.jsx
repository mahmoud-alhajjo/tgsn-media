import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const StackedBarChart = ({ data, chartId, chartTitle, bars }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		// Create chart instance
		let chart = am4core.create(chartId, am4charts.XYChart);
		chart.paddingRight = 50;
		chart.colors.step = 2;

		// Add data
		data.forEach(item => {
			for (const key in item) {
				if (item[key] <= 0) {
					item[key] = null;
				}
			}
		});
		chart.data = data;
		// Create axes
		var dateAxis = chart.yAxes.push(new am4charts.DateAxis());
		dateAxis.dataFields.category = 'date';
		dateAxis.renderer.grid.template.opacity = 0;

		var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		//valueAxis.logarithmic = true;

		// Create series
		function createSeries(field, name) {
			var series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueX = field;
			series.dataFields.dateY = 'date';
			series.stacked = true;
			series.name = name;
			series.tooltipText = '{name}: [bold]{valueX}[/]';

			var labelBullet = series.bullets.push(new am4charts.LabelBullet());
			labelBullet.locationX = 0.5;
			labelBullet.label.text = '{valueX}';
			labelBullet.label.fill = am4core.color('#fff');
			labelBullet.label.hideOversized = true;
		}

		bars.filter(k => k !== 'date').forEach(bar => {
			createSeries(bar, bar);
		});

		chart.legend = new am4charts.Legend();
		chart.cursor = new am4charts.XYCursor();

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;
		title.marginBottom = 20;

		// exporting
		var options = chart.exporting.getFormatOptions('png');
		options.keepTainted = true;
		chart.exporting.setFormatOptions('png', options);
		chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;

		setChart(chart);

		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default StackedBarChart;

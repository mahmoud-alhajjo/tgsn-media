import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const MultiLineGraph = ({ data, chartId, chartTitle, lines }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		// Create chart instance
		let chart = am4core.create(chartId, am4charts.XYChart);
		chart.colors.step = 2;
		chart.paddingRight = 50;

		// Add data
		chart.data = data;

		// Create axes
		var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		dateAxis.renderer.minGridDistance = 50;

		chart.yAxes.push(new am4charts.ValueAxis());

		// Create series
		function createAxisAndSeries(field, name) {
			var series = chart.series.push(new am4charts.LineSeries());
			series.dataFields.valueY = field;
			series.dataFields.dateX = 'date';
			series.strokeWidth = 2;
			series.name = name;
			series.tooltipText = '{name}: [bold]{valueY}[/]';
		}

		lines
			.filter(k => k !== 'date')
			.forEach(line => {
				createAxisAndSeries(line, line);
			});

		// Add legend
		chart.legend = new am4charts.Legend();

		// Add cursor
		chart.cursor = new am4charts.XYCursor();

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;
		title.marginBottom = 20;

		// exporting
		var options = chart.exporting.getFormatOptions('png');
		options.keepTainted = true;
		chart.exporting.setFormatOptions('png', options);
		chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;

		setChart(chart);

		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default MultiLineGraph;

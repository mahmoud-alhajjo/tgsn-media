import React, { useEffect, Fragment, useState } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const DateBasedRadarChart = ({ data, chartId, category, chartTitle, chartTooltip }) => {
	const yearsList = [...new Set(data.map(item => new Date(item.from_date).getFullYear()))].sort();
	const [year, setYear] = useState(yearsList[0]);
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		/* Create chart instance */
		let chart = am4core.create(chartId, am4charts.RadarChart);
		chart.colors.step = 2;

		data.forEach(item => {
			for (const key in item) {
				if (key === 'from_date' || key === 'to_date') {
					item[key + '_' + item[category]] = item[key];
				}
			}
		});
		chart.data = data;

		chart.padding(20, 20, 20, 20);
		chart.colors.step = 2;
		chart.dateFormatter.inputDateFormat = 'YYYY-MM-dd';
		chart.innerRadius = am4core.percent(40);

		var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = category;
		categoryAxis.renderer.grid.template.disabled = false;
		categoryAxis.renderer.labels.template.disabled = true;
		categoryAxis.renderer.tooltipLocation = 0.5;
		categoryAxis.renderer.minGridDistance = 10;
		categoryAxis.mouseEnabled = false;
		categoryAxis.tooltip.disabled = true;
		categoryAxis.renderer.grid.template.location = 0;

		var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		dateAxis.renderer.labels.template.horizontalCenter = 'left';
		dateAxis.strictMinMax = true;
		dateAxis.renderer.maxLabelPosition = 0.99;
		dateAxis.renderer.grid.template.strokeOpacity = 0.07;
		dateAxis.min = new Date(parseInt(year), 0, 0, 0, 0, 0).getTime();
		dateAxis.max = new Date(parseInt(year) + 1, 0, 0, 0, 0, 0).getTime();
		dateAxis.mouseEnabled = false;
		dateAxis.tooltip.disabled = true;
		dateAxis.baseInterval = { count: 1, timeUnit: 'day' };

		data.forEach(item => {
			var series = chart.series.push(new am4charts.RadarColumnSeries());
			series.dataFields.openDateX = `from_date_${item[category]}`;
			series.dataFields.dateX = `to_date_${item[category]}`;
			series.dataFields.categoryY = category;
			series.clustered = false;
			series.name = item[category];
			series.columns.template.radarColumn.cornerRadius = 30;
			series.columns.template.tooltipText = `[bold]{count} ${chartTooltip} in {categoryY}[/]\n {openDateX} to {dateX}`;
		});

		chart.seriesContainer.zIndex = -1;

		chart.cursor = new am4charts.RadarCursor();
		chart.cursor.innerRadius = am4core.percent(40);
		chart.cursor.lineY.disabled = true;

		var yearLabel = chart.radarContainer.createChild(am4core.Label);
		yearLabel.text = year;
		yearLabel.fontSize = 30;
		yearLabel.horizontalCenter = 'middle';
		yearLabel.verticalCenter = 'middle';

		chart.legend = new am4charts.Legend();

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;
		title.marginBottom = 20;

		// exporting
		var options = chart.exporting.getFormatOptions('png');
		options.keepTainted = true;
		chart.exporting.setFormatOptions('png', options);
		chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;
		setChart(chart);
		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [year, theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return (
		<ChartContainer
			chartId={chartId}
			exportPNG={exportPNG}
			years={yearsList}
			setYear={setYear}
			setTheme={setTheme}
			theme={theme}
		/>
	);
};

export default DateBasedRadarChart;

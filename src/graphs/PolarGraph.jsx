import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import ChartContainer from './ChartContainer';
import moment from 'moment';

const PolarGraph = ({ data, chartId, category, polars, chartTitle }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		/* Create chart instance */
		let chart = am4core.create(chartId, am4charts.RadarChart);
		chart.colors.step = 2;

		chart.data = data;

		/* Create axes */
		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = category;

		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
		valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

		// Create series
		function createSeries(field, name) {
			var series = chart.series.push(new am4charts.RadarSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = category;
			series.name = name;
			series.strokeWidth = 2;
		}

		polars.forEach(polar => {
			createSeries(polar, polar);
		});

		chart.cursor = new am4charts.RadarCursor();

		chart.legend = new am4charts.Legend();

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;
		title.marginBottom = 20;

		// exporting
		var options = chart.exporting.getFormatOptions('png');
		options.keepTainted = true;
		chart.exporting.setFormatOptions('png', options);
		chart.exporting.filePrefix = `${chartTitle} ${moment().format('YYYY-MM-DD')}`;
		setChart(chart);

		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default PolarGraph;

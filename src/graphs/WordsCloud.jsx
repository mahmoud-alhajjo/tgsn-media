import React, { useEffect } from 'react';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4plugins_wordCloud from '@amcharts/amcharts4/plugins/wordCloud';
import ChartContainer from './ChartContainer';

const WordsCloud = ({ data, chartId, chartTitle, word }) => {
	const [chart, setChart] = React.useState();
	const [theme, setTheme] = React.useState('animated');

	useEffect(() => {
		var chart = am4core.create(chartId, am4plugins_wordCloud.WordCloud);
		chart.padding = 20;

		chart.data = data;

		var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());

		series.dataFields.word = word;
		series.dataFields.value = 'count';
		series.colors = new am4core.ColorSet();
		series.colors.step = 1;

		series.rotationThreshold = 0;
		series.labels.template.tooltipText = '{word}: {value}';
		series.fontFamily = 'Courier New';
		series.fontWeight = '600';
		series.maxFontSize = am4core.percent(30);
		series.minFontSize = am4core.percent(4);

		var title = chart.titles.create();
		title.text = chartTitle;
		title.fontSize = 25;

		setChart(chart);

		return () => {
			if (chart) {
				chart.dispose();
			}
		};
	}, [theme]);

	// Custom export function
	const exportPNG = () => {
		chart.exporting.export('png');
	};

	return <ChartContainer chartId={chartId} exportPNG={exportPNG} setTheme={setTheme} theme={theme} />;
};

export default WordsCloud;

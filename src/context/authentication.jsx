import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import cookie from 'react-cookie'

const mapDispatchToProps = dispatch => ({
  setUserProfile: userProfile => {
    dispatch({
      type: 'SET_USER_PROFILE',
      payload: {
        userProfile,
        isAdmin: userProfile.is_admin,
        isLoggedIn: true
      }
    })
  }
})

class Authentication extends Component {
  componentDidMount () {
    const isAuthenticated = cookie.load('access_token')
    if (!isAuthenticated) {
      this.props.history.push('/login')
    } else {
      const userProfile = JSON.parse(localStorage.getItem('user_profile'))
      userProfile && this.props.setUserProfile(userProfile)
    }
  }

  render () {
    return (
      <div>
        {this.props.children}
      </div>
    )
  }
}

export default withRouter(connect(null, mapDispatchToProps)(Authentication))

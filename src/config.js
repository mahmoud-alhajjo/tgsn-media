const production = {
    url: {
        API_URL: "https://api.tgsn.media/v1"
    }
}
const staging = {
    url: {
        API_URL: "https://stagingapi.tgsn.media/v1"
    }
};
export const config = process.env.REACT_APP_ENV === "staging" ? staging : production;


let data = {};

data['data_syr_0'] = require('./gadm36_SYR_0');
data['data_syr_1'] = require('./gadm36_SYR_1');
data['data_syr_2'] = require('./gadm36_SYR_2');
data['data_cmr_0'] = require('./gadm36_CMR_0');
data['data_cmr_1'] = require('./gadm36_CMR_1');
data['data_cmr_2'] = require('./gadm36_CMR_2');
data['data_mli_0'] = require('./gadm36_MLI_0');
data['data_mli_1'] = require('./gadm36_MLI_1');
data['data_mli_2'] = require('./gadm36_MLI_2');
data['data_ner_0'] = require('./gadm36_NER_0');
data['data_ner_1'] = require('./gadm36_NER_1');
data['data_ner_2'] = require('./gadm36_NER_2');
data['data_nga_0'] = require('./gadm36_NGA_0');
data['data_nga_1'] = require('./gadm36_NGA_1');
data['data_nga_2'] = require('./gadm36_NGA_2');

export default data